FROM tomcat:8.5-alpine
FROM openjdk:11-jre
RUN rm -rf /usr/local/tomcat/webapps/*
# change name of .war 
COPY ./target/TSServices-1.1.0.war /usr/local/tomcat/webapps/TSServices-1.1.0.war
COPY ./target/classes/static/mapping /usr/local/tomcat/webapps/classes/static/mapping
COPY ./target/classes/static/platform-data /usr/local/tomcat/webapps/classes/static/platform-data
RUN sh -c 'touch /usr/local/tomcat/webapps/TSServices-1.1.0.war'
CMD ["sh","-c", "java -jar /usr/local/tomcat/webapps/TSServices-1.1.0.war"]