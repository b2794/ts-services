# Terminology Services

- [How to run the project](#RunProject)
    * [Run using docker-compose](#runDocker)  
    * [Run on a local machine](#runLocal)


- [Deployment](#Deployment)  
    * [Build the web application](#webApp)
    * [Create docker image for ts-services and upload it to Gitlab Dockerhub](#dockerDepl)
    
- [Swagger](#Swagger)
    * [Run swagger UI](#swaggerUI)
    
- [Load new data in Virtuoso](#loadData)

- [Import Karma for offline triples generation](#importKarma)


## How to run the project <a name="RunProject"></a>
The easiest way to start the project is to use `docker-compose`.   
However, if you want to develop ts-services, use `the local start`.


### Run using docker-compose <a name="runDocker"></a>
1. Make sure the following ports are available:
   - `1111`  
   - `8080`
2. Run `docker-compose up` and wait until the application starts.  
3. The application is available under `http://localhost:8080/ts-services/core/terminologies/`.

### Run on a local machine <a name="runLocal"></a>
1. Start Virtuoso.  

   There are two options you may use: 
    - stop your local Virtuoso server, copy files from `virtuoso/data` into your virtuoso data storage and start it again, or
    - run from the project root folder `cd virtuoso`, then `docker-compose up`.
  
    Check if Virtuoso has been started under `http://localhost:8890/conductor/`.

2. Run the main class `RESTfulWS` (right-click `Run As -> Java Application`)
3. The application is available under `http://localhost:8080/ts-services/core/terminologies/`. 

## Deployment <a name="Deployment"></a>

### Build the web application <a name="webApp"></a>
To build `war`file:
1. Run in the root project folder: 
    - `mvn clean`, then
    - `mvn package -DskipTests` 
2. Created `TSServices-1.1.0.war` can be found in `/target`.

### Create docker image for ts-services and upload it to Gitlab Dockerhub <a name="dockerDepl"></a>
1. Make sure  virtuoso hosturl in `src/resources/VirtSettings.properties` is `jdbc:virtuoso://virtuoso:1111`.
2. Build the web application as described above. 
3. Log in to your GitLab’s Container Registry using your GitLab username and password:
   `docker login registry.gitlab.com`.
4. Build the docker image for the associated Gitlab Container Registry:
   `docker build -t registry.gitlab.com/b2794/ts-services:tag .`
   Use as `tag` the actual date, i.g. `22012021`.
5. Upload the image: 
   `docker push registry.gitlab.com/b2794/ts-services:tag`.

## Run the Swagger UI  <a name="swaggerUI"></a>
### On the local machine
1. Run the application and open `http://localhost:8083/ts-services/swagger-ui.html` in your browser -> an automatic generated documentation is loaded. 
2. In the Swagger navigation specify the path `/ts-services/openapi.json` and press the button "Explore".  

### Using docker-compose
1. Specify port for swagger ui in the docker-compose.yaml (e.g. port 8082)
2. Specify host and port of the spring boot application in /docs/openapi.json in 
   `"servers": [{"url": "http://localhost:8081/"}],`
3. Access the swagger ui under `host(where the application runs):port`,e.g., `localhost:8083/` 
![Open-Api-Example](src/main/resources/static/docs/openapi.jpg)


## Load new data to VirtuosoGraph <a name="loadData"></a>
* You need to checkout and build the [terminologymetrics](https://gitlab.com/gfbio-terminology-management/gfbiotsterminologymetrics) project.
* Make sure your user can access the terminology storage folder, e.g., `/var/opt/ts/ontologies` and that this folder is mapped into `Virtuoso` - otherwise the upload won't work!
* Ideally the storage folder structure should is the following: `/path/to/ontologies/ACRONYM/YYYY-MM-DD/acronym.(owl|skos)`
* Virtuoso must be running and connectable through ports **1111** and **8890**

### Import new Terminology
1. Collect all required terminology parameters for the new terminology and insert them as triples into the `Parameters` graph
2. Generate metrics for the new terminology using the project `terminologymetrics` (for a detailed instruction read the respective readme)

The terminology should be uploaded automatically to `Virtuoso` into a new graph, e.g., `http://terminologies.gfbio.org/terms/ACRONYM`.
If - for any reason - no access to the terminology directory is possible or forbidden one may execute the SPARQL queries herself. Open the [configuration](https://gitlab.com/gfbio-terminology-management/gfbiotsterminologymetrics/-/blob/master/configuration/metrics.ini) and set `execSPARQL` to `false`. SPARQL queries will be written to files automatically. Then start the metrics/upload process manually.

1. Run the generated SPARQL-queries in the isql console from Virtuoso. 
2. In Virtuoso Conductor: Linked Data -> Graphs -> Quad Store Upload. 
3. Fill the information in the Quad Store Upload Tab and check "Create graph explicitly". 

### Update existing Terminology
1. Place the new terminology version in its respective folder
2. Execute `terminologymetrics` with required parameters. Following steps will be done:
   * Backup the existing graph
   * Upload the new version
   * Calculate the intersection (and write the results to a **MOD** graph)
   * Calculate metrics on the new version
   * Delete the old version after the proceeding steps are done
   * Update entries in the `Metadata` graph and *archive* the metadata of the old version in the same graph
3. If everything went well a new entry should be available in the [archive](https://terminologies.gfbio.org/api/terminologies/ACRONYM/archive) 

To do this manually just follow along the steps mentioned in the section above.

## Import Karma-offline for JSON to RDF Triples Generation <a name="importKarma"></a>
1. Clone the Karma project from `https://github.com/usc-isi-i2/Web-Karma`.
2. Build the project as described `https://github.com/usc-isi-i2/Web-Karma/wiki/Installation%3A-Source-Code`.
3. Copy created `karma-offline-0.0.1-SNAPSHOT-shaded.jar` from your local mvn repository, e.g. `.m2\repository\edu\isi\karma-offline\0.0.1-SNAPSHOT`,
into the repository specified in the project POM.xml
4. Build the application

## Deploy Docker Images to Kubernetes/Kubermatic

_set the namespace to ts-test when issuing a command: -n ts-test_ 

1. Create a deployment `.yaml`
2. Create the deployment on the server via `kubectl create -f <xxx>.yaml -n ts-test`
3. Check for deployments `kubectl get deploy -n ts-test`
4. Create a service `.yaml` which publishes the service
4. Expose the services deployed earlier to make them accessible `kubectl expose deployment ts-services --name=ts-services -n ts-test`
   1. Make sure to use matching deployment names here
5. `kubectl get services -n ts-test`

# Delete a Deployment
1. `kubectl delete -f <xxx>.yaml`
2. `kubectl delete service ts-services -n ts-test`