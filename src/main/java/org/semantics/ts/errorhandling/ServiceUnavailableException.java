package org.semantics.ts.errorhandling;

import org.apache.commons.lang3.StringUtils;

public class ServiceUnavailableException extends RuntimeException {

    public ServiceUnavailableException(String serviceName) {
        super(ServiceUnavailableException.generateMessage(serviceName));
    }

    private static String generateMessage(String serviceName) {
        return String.format("Unable to create the instance of the %s service", StringUtils.capitalize(serviceName));
    }


}
