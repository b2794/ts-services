package org.semantics.ts.errorhandling;

import org.apache.commons.lang3.StringUtils;

public class ArchiveNotFoundException extends RuntimeException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public ArchiveNotFoundException(String entityName) {
    super(ArchiveNotFoundException.generateMessage(entityName));
  }

  private static String generateMessage(String entity) {
    return "Archive for " + StringUtils.capitalize(entity)
        + " could not be found/does not exist yet ";
  }

}
