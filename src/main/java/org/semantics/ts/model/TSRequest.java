package org.semantics.ts.model;

public class TSRequest {
    String query;
    String executionTime;

    public TSRequest() {
    }

    public TSRequest(String query, String executionTime) {
        this.query = query;
        this.executionTime = executionTime;
    }

    public String getQuery() {
        return this.query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getExecutionTime() {
        return this.executionTime;
    }

    public void setExecutionTime(String executionTime) {
        this.executionTime = executionTime;
    }

}
