package org.semantics.ts.model.hierarchy;

import org.semantics.ts.model.core.term.TermDataBasic;

import java.util.List;

public class HierarchyPath extends TermDataBasic {
    List<String> hierarchy;

    public HierarchyPath(List<String> hierarchy) {
        this.hierarchy = hierarchy;
    }

    public HierarchyPath(String uri, String label, List<String> hierarchy) {
        super(uri, label);
        this.hierarchy = hierarchy;
    }

    public List<String> getHierarchy() {
        return hierarchy;
    }

    public void setHierarchy(List<String> hierarchy) {
        this.hierarchy = hierarchy;
    }


}
