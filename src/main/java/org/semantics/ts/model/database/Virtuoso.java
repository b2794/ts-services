package org.semantics.ts.model.database;

import javax.ws.rs.InternalServerErrorException;
import org.apache.jena.query.ResultSet;
import org.apache.jena.shared.JenaException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;

public class Virtuoso {

  private static final Logger LOGGER = LoggerFactory.getLogger(Virtuoso.class);

  private String host;
  private String username;
  private String password;

  private VirtGraph virtGraph;

  public Virtuoso(String host, String username, String password) {
    this.host = host;
    this.username = username;
    this.password = password;

    initVirtConn();
  }

  private void initVirtConn() {
    int count = 0;
    int maxTries = 3;

    if (this.virtGraph == null) {
      while (true) {
        try {
          // sleep 3s to wait for Virtuoso to startup (if using docker-compose)
          LOGGER
              .info("waiting 3s for Virtuoso to boot-up (are we in a docker-compose environment?)");
          Thread.sleep(3000);
          this.virtGraph = new VirtGraph(host, username, password);
          break;
        } catch (JenaException e) {

          if (++count == maxTries) {
            LOGGER.error(e.getLocalizedMessage());
            break;
            // throw e;
          }
        } catch (InterruptedException e) {
          LOGGER.error(e.getLocalizedMessage());
          break;
        }
      }
    }
    LOGGER.info("connection to Virtuoso successful");
  }

  public VirtGraph getVirtGraph() {
    return virtGraph;
  }

  public void setVirtGraph(VirtGraph virtGraph) {
    this.virtGraph = virtGraph;
  }

  /**
   * Logs and executes a SPARQL query
   * 
   * @param vqe Interface for a single execution of a query
   * @param query The query to execute
   * @return ResultSet
   */
  public ResultSet executeSPARQL(VirtuosoQueryExecution vqe, String query) {
    ResultSet rs = null;
    // TODO do we need to log every query?
    LOGGER.info("Executing SPARQL query: " + query);
    try {
      rs = vqe.execSelect();
    } catch (InternalServerErrorException e) {
      this.virtGraph.close();
    }
    return rs;
  }
}
