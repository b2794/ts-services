package org.semantics.ts.model.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TerminologyLinks extends TerminologyJsonBasic {

  @JsonProperty("@context")
  private TerminologyContext context;

  public TerminologyLinks() {}

  public TerminologyContext getContext() {
    return context;
  }

  public void setContext(TerminologyContext context) {
    this.context = context;
  }

}
