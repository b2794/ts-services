package org.semantics.ts.model.json;

public class TerminologyJsonBasic {

  private String self;
  private String metrics;
  private String metadata;
  private String latest;
  private String download;
  private String archive;
  private String allterms;
  private String changelog;

  public TerminologyJsonBasic() {}

  public String getSelf() {
    return self;
  }

  public void setSelf(String self) {
    this.self = self;
  }

  public String getMetrics() {
    return metrics;
  }

  public void setMetrics(String metrics) {
    this.metrics = metrics;
  }

  public String getMetadata() {
    return metadata;
  }

  public void setMetadata(String metadata) {
    this.metadata = metadata;
  }

  public String getLatest() {
    return latest;
  }

  public void setLatest(String latest) {
    this.latest = latest;
  }

  public String getDownload() {
    return download;
  }

  public void setDownload(String download) {
    this.download = download;
  }

  public String getArchive() {
    return archive;
  }

  public void setArchive(String archive) {
    this.archive = archive;
  }

  public String getAllterms() {
    return allterms;
  }

  public void setAllterms(String allterms) {
    this.allterms = allterms;
  }

  public String getChangelog() {
    return changelog;
  }

  public void setChangelog(String changelog) {
    this.changelog = changelog;
  }
}
