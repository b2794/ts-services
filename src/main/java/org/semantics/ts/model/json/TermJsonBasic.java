package org.semantics.ts.model.json;

public class TermJsonBasic {

  private String self;
  private String parents;
  private String children;
  private String ancestors;
  private String descendants;

  public TermJsonBasic() {}

  public String getSelf() {
    return self;
  }

  public void setSelf(String self) {
    this.self = self;
  }

  public String getParents() {
    return parents;
  }

  public void setParents(String parents) {
    this.parents = parents;
  }

  public String getChildren() {
    return children;
  }

  public void setChildren(String children) {
    this.children = children;
  }

  public String getAncestors() {
    return ancestors;
  }

  public void setAncestors(String ancestors) {
    this.ancestors = ancestors;
  }

  public String getDescendants() {
    return descendants;
  }

  public void setDescendants(String descendants) {
    this.descendants = descendants;
  }


}
