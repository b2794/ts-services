package org.semantics.ts.model.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TermLinks extends TermJsonBasic {

  @JsonProperty("@context")
  private TermContext context;

  public TermLinks() {}

  public TermContext getContext() {
    return context;
  }

  public void setContext(TermContext context) {
    this.context = context;
  }

}
