package org.semantics.ts.model.mapping;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Algorithm extends EntityMappingBasic {
    public String acronym;
    @SerializedName(value = "nisq_ready", alternate = "nisqReady")
    public boolean nisqReady;
    @SerializedName(value = "speed_up", alternate = "speedUp")
    public String speedUp;
    public String entityMaturity;
    public List<EntityMappingBasic> publications;
    public String intent;
    public String problem;
    @SerializedName(value = "input_format", alternate = "inputFormat")
    public String inputFormat;
    @SerializedName(value = "output_format", alternate = "outputFormat")
    public String outputFormat;
    public List<EntityMappingBasic> requiredComputeResourceProperties;
    @SerializedName(value = "algo_parameter", alternate = "algoParameters")
    public String algoParameters;
    public List<EntityMappingBasic> sketches;
    public String solution;
    @SerializedName(value = "computation_model", alternate = "computationModel")
    public String computationModel;
    @SerializedName(value = "problem_types", alternate = "problemTypes")
    public List<EntityMappingBasic> problemTypes;
    @SerializedName(value = "application_areas", alternate = "applicationAreas")
    public List<EntityMappingBasic> applicationAreas;
    public List<EntityMappingBasic> tags;
    public List<EntityMappingBasic> implementations;
    @SerializedName(value = "discussion_topics", alternate = "discussionTopics")
    public List<EntityMappingBasic> discussionTopics;
    @SerializedName(value = "learning_methods", alternate = "learningMethods")
    public List<EntityMappingBasic> learningMethods;
    @SerializedName(value = "algorithm_relation", alternate = "algorithmRelations")
    public List<EntityMappingBasic> algorithmRelations;

    public Algorithm(String id, String name, String acronym, boolean nisqReady, String speedUp, String entityMaturity, List<EntityMappingBasic> publications,
                     String intent, String problem, String inputFormat, String outputFormat, List<EntityMappingBasic> requiredComputeResourceProperties, String algoParameters,
                     List<EntityMappingBasic> sketches, String solution, String computationModel, List<EntityMappingBasic> problemTypes, List<EntityMappingBasic> applicationAreas, List<EntityMappingBasic> tags,
                     List<EntityMappingBasic> implementations, List<EntityMappingBasic> discussionTopics, List<EntityMappingBasic> learningMethods, List<EntityMappingBasic> algorithmRelations) {
        super(id, name);
        this.acronym = acronym;
        this.nisqReady = nisqReady;
        this.speedUp = speedUp;
        this.entityMaturity = entityMaturity;
        this.publications = publications;
        this.intent = intent;
        this.problem = problem;
        this.inputFormat = inputFormat;
        this.outputFormat = outputFormat;
        this.requiredComputeResourceProperties = requiredComputeResourceProperties;
        this.algoParameters = algoParameters;
        this.sketches = sketches;
        this.solution = solution;
        this.computationModel = computationModel;
        this.problemTypes = problemTypes;
        this.applicationAreas = applicationAreas;
        this.tags = tags;
        this.implementations = implementations;
        this.discussionTopics = discussionTopics;
        this.learningMethods = learningMethods;
        this.algorithmRelations = algorithmRelations;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isNisqReady() {
        return nisqReady;
    }

    public void setNisqReady(boolean nisqReady) {
        this.nisqReady = nisqReady;
    }


    public List<EntityMappingBasic> getRequiredComputeResourceProperties() {
        return requiredComputeResourceProperties;
    }

    public void setRequiredComputeResourceProperties(List<EntityMappingBasic> requiredComputeResourceProperties) {
        this.requiredComputeResourceProperties = requiredComputeResourceProperties;
    }
    public String getEntityMaturity() {
        return entityMaturity;
    }

    public void setEntityMaturity(String entityMaturity) {
        this.entityMaturity = entityMaturity;
    }

    public List<EntityMappingBasic> getPublications() {
        return publications;
    }

    public void setPublications(List<EntityMappingBasic> publications) {
        this.publications = publications;
    }

    public List<EntityMappingBasic> getSketches() {
        return sketches;
    }

    public void setSketches(List<EntityMappingBasic> sketches) {
        this.sketches = sketches;
    }

    public String getComputationModel() {
        return computationModel;
    }

    public void setComputationModel(String computationModel) {
        this.computationModel = computationModel;
    }

    public List<EntityMappingBasic> getProblemTypes() {
        return problemTypes;
    }

    public void setProblemTypes(List<EntityMappingBasic> problemTypes) {
        this.problemTypes = problemTypes;
    }

    public List<EntityMappingBasic> getApplicationAreas() {
        return applicationAreas;
    }

    public void setApplicationAreas(List<EntityMappingBasic> applicationAreas) {
        this.applicationAreas = applicationAreas;
    }

    public List<EntityMappingBasic> getTags() {
        return tags;
    }

    public void setTags(List<EntityMappingBasic> tags) {
        this.tags = tags;
    }

    public List<EntityMappingBasic> getImplementations() {
        return implementations;
    }

    public void setImplementations(List<EntityMappingBasic> implementations) {
        this.implementations = implementations;
    }

    public List<EntityMappingBasic> getDiscussionTopics() {
        return discussionTopics;
    }

    public void setDiscussionTopics(List<EntityMappingBasic> discussionTopics) {
        this.discussionTopics = discussionTopics;
    }

    public List<EntityMappingBasic> getLearningMethods() {
        return learningMethods;
    }

    public void setLearningMethods(List<EntityMappingBasic> learningMethods) {
        this.learningMethods = learningMethods;
    }

    public List<EntityMappingBasic> getAlgorithmRelations() {
        return algorithmRelations;
    }

    public void setAlgorithmRelations(List<EntityMappingBasic> algorithmRelations) {
        this.algorithmRelations = algorithmRelations;
    }


    public String getSpeedUp() {
        return speedUp;
    }

    public void setSpeedUp(String speedUp) {
        this.speedUp = speedUp;
    }

    public String getIntent() {
        return intent;
    }

    public void setIntent(String intent) {
        this.intent = intent;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public String getInputFormat() {
        return inputFormat;
    }

    public void setInputFormat(String inputFormat) {
        this.inputFormat = inputFormat;
    }

    public String getOutputFormat() {
        return outputFormat;
    }

    public void setOutputFormat(String outputFormat) {
        this.outputFormat = outputFormat;
    }

    public String getAlgoParameters() {
        return algoParameters;
    }

    public void setAlgoParameters(String algoParameters) {
        this.algoParameters = algoParameters;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }
}
