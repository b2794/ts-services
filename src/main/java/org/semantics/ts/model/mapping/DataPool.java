package org.semantics.ts.model.mapping;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataPool extends EntityMappingBasic {
    @SerializedName(value = "short_description", alternate = "shortDescription")
    private String shortDescription;
    private String description;
    @SerializedName(value = "licence_type", alternate = "licenceType")
    private String licenceType;
    private String metadata;
    @SerializedName(value = "discussion_topics", alternate = "discussionTopics")
    private List<EntityMappingBasic> discussionTopics;

    public DataPool(String id, String name, String shortDescription, String description, String licenceType, String metadata, List<EntityMappingBasic> discussionTopics) {
        super(id, name);
        this.shortDescription = shortDescription;
        this.description = description;
        this.licenceType = licenceType;
        this.metadata = metadata;
        this.discussionTopics = discussionTopics;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLicenceType() {
        return licenceType;
    }

    public void setLicenceType(String licenceType) {
        this.licenceType = licenceType;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public List<EntityMappingBasic> getDiscussionTopics() {
        return discussionTopics;
    }

    public void setDiscussionTopics(List<EntityMappingBasic> discussionTopics) {
        this.discussionTopics = discussionTopics;
    }
}
