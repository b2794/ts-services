package org.semantics.ts.model.mapping;

import com.google.gson.annotations.SerializedName;


public class File extends EntityMappingBasic {
    @SerializedName(value = "mime_type", alternate = "mimeType")
    public String mimeType;
    @SerializedName(value = "creation_date", alternate = "creationDate")
    public String creationDate;
    @SerializedName(value = "last_modified_at", alternate = "lastModifiedAt")
    public String lastModifiedAt;
    @SerializedName(value = "file_url", alternate = "fileUrl")
    public String file_url;

    public File(String id, String name, String mimeType, String creationDate, String lastModifiedAt, String file_url) {
        super(id, name);
        this.mimeType = mimeType;
        this.creationDate = creationDate;
        this.lastModifiedAt = lastModifiedAt;
        this.file_url = file_url;
    }

    public File() {
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getLastModifiedAt() {
        return lastModifiedAt;
    }

    public void setLastModifiedAt(String lastModifiedAt) {
        this.lastModifiedAt = lastModifiedAt;
    }

    public String getFile_url() {
        return file_url;
    }

    public void setFile_url(String file_url) {
        this.file_url = file_url;
    }
}
