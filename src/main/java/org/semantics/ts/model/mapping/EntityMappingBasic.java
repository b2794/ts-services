package org.semantics.ts.model.mapping;

public class EntityMappingBasic {
    String id;
    String name;

    public EntityMappingBasic() {
    }

    public EntityMappingBasic(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
