package org.semantics.ts.model.mapping;

public class AlgorithmRelationRaw {
    String id;
    String sourceAlgorithmId;
    String targetAlgorithmId;
    String sourceAlgorithmName;
    String targetAlgorithmName;
    String description;
    AlgorithmRelationType algorithmRelationType;

    public AlgorithmRelationRaw(String id, String sourceAlgorithmId, String targetAlgorithmId, String sourceAlgorithmName,
                                String targetAlgorithmName, String description, AlgorithmRelationType algorithmRelationType) {
        this.id = id;
        this.sourceAlgorithmId = sourceAlgorithmId;
        this.targetAlgorithmId = targetAlgorithmId;
        this.sourceAlgorithmName = sourceAlgorithmName;
        this.targetAlgorithmName = targetAlgorithmName;
        this.description = description;
        this.algorithmRelationType = algorithmRelationType;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSourceAlgorithmId() {
        return sourceAlgorithmId;
    }

    public void setSourceAlgorithmId(String sourceAlgorithmId) {
        this.sourceAlgorithmId = sourceAlgorithmId;
    }

    public String getTargetAlgorithmId() {
        return targetAlgorithmId;
    }

    public void setTargetAlgorithmId(String targetAlgorithmId) {
        this.targetAlgorithmId = targetAlgorithmId;
    }

    public AlgorithmRelationType getAlgorithmRelationType() {
        return algorithmRelationType;
    }

    public void setAlgorithmRelationType(AlgorithmRelationType algorithmRelationType) {
        this.algorithmRelationType = algorithmRelationType;
    }

    public String getSourceAlgorithmName() {
        return sourceAlgorithmName;
    }

    public void setSourceAlgorithmName(String sourceAlgorithmName) {
        this.sourceAlgorithmName = sourceAlgorithmName;
    }

    public String getTargetAlgorithmName() {
        return targetAlgorithmName;
    }

    public void setTargetAlgorithmName(String targetAlgorithmName) {
        this.targetAlgorithmName = targetAlgorithmName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
