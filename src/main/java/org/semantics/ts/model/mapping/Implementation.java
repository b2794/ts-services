package org.semantics.ts.model.mapping;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Implementation extends EntityMappingBasic {
    public String description;
    public String version;
    public String license;
    public String technology;
    public String link;
    @SerializedName(value = "problem_statement", alternate = "problemStatement")
    public String problemStatement;
    @SerializedName(value = "input_format", alternate = "inputFormat")
    public String inputFormat;
    @SerializedName(value = "output_format", alternate = "outputFormat")
    public String outputFormat;
    @SerializedName(value = "implemented_algorithm", alternate = "implementedAlgorithm")
    public EntityMappingBasic implementedAlgorithm;
    @SerializedName(value = "discussion_topics", alternate = "discussionTopics")
    public List<EntityMappingBasic> discussionTopics;
    public List<EntityMappingBasic> tags;
    public List<EntityMappingBasic> contributors;
    public List<String> assumptions;
    public List<String> parameter;
    public List<String> dependencies;
    @SerializedName(value = "required_compute_resource_properties", alternate = "requiredComputeResourceProperties")
    public List<EntityMappingBasic> requiredComputeResourceProperties;
    @SerializedName(value = "software_platforms", alternate = "softwarePlatforms")
    public List<EntityMappingBasic> softwarePlatforms;
    public List<File> files;

    public Implementation(String id, String name, String description, String version, String license, String technology,
                          String link, String problemStatement, String inputFormat, String outputFormat, EntityMappingBasic implementedAlgorithm,
                          List<EntityMappingBasic> discussionTopics, List<EntityMappingBasic> tags, List<EntityMappingBasic> contributors,
                          List<String> assumptions, List<String> parameter, List<String> dependencies,
                          List<EntityMappingBasic> requiredComputeResourceProperties, List<EntityMappingBasic> softwarePlatforms,
                          List<File> files) {
        super(id, name);
        this.description = description;
        this.version = version;
        this.license = license;
        this.technology = technology;
        this.link = link;
        this.problemStatement = problemStatement;
        this.inputFormat = inputFormat;
        this.outputFormat = outputFormat;
        this.implementedAlgorithm = implementedAlgorithm;
        this.discussionTopics = discussionTopics;
        this.tags = tags;
        this.contributors = contributors;
        this.assumptions = assumptions;
        this.parameter = parameter;
        this.dependencies = dependencies;
        this.requiredComputeResourceProperties = requiredComputeResourceProperties;
        this.softwarePlatforms = softwarePlatforms;
        this.files = files;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

    public List<EntityMappingBasic> getDiscussionTopics() {
        return discussionTopics;
    }

    public void setDiscussionTopics(List<EntityMappingBasic> discussionTopics) {
        this.discussionTopics = discussionTopics;
    }

    public List<EntityMappingBasic> getTags() {
        return tags;
    }

    public void setTags(List<EntityMappingBasic> tags) {
        this.tags = tags;
    }

    public List<EntityMappingBasic> getRequiredComputeResourceProperties() {
        return requiredComputeResourceProperties;
    }

    public void setRequiredComputeResourceProperties(List<EntityMappingBasic> requiredComputeResourceProperties) {
        this.requiredComputeResourceProperties = requiredComputeResourceProperties;
    }

    public List<EntityMappingBasic> getSoftwarePlatforms() {
        return softwarePlatforms;
    }

    public void setSoftwarePlatforms(List<EntityMappingBasic> softwarePlatforms) {
        this.softwarePlatforms = softwarePlatforms;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getProblemStatement() {
        return problemStatement;
    }

    public void setProblemStatement(String problemStatement) {
        this.problemStatement = problemStatement;
    }

    public String getInputFormat() {
        return inputFormat;
    }

    public void setInputFormat(String inputFormat) {
        this.inputFormat = inputFormat;
    }

    public String getOutputFormat() {
        return outputFormat;
    }

    public void setOutputFormat(String outputFormat) {
        this.outputFormat = outputFormat;
    }

    public EntityMappingBasic getImplementedAlgorithm() {
        return implementedAlgorithm;
    }

    public void setImplementedAlgorithm(EntityMappingBasic implementedAlgorithm) {
        this.implementedAlgorithm = implementedAlgorithm;
    }

    public List<EntityMappingBasic> getContributors() {
        return contributors;
    }

    public void setContributors(List<EntityMappingBasic> contributors) {
        this.contributors = contributors;
    }

    public List<String> getAssumptions() {
        return assumptions;
    }

    public void setAssumptions(List<String> assumptions) {
        this.assumptions = assumptions;
    }

    public List<String> getParameter() {
        return parameter;
    }

    public void setParameter(List<String> parameter) {
        this.parameter = parameter;
    }

    public List<String> getDependencies() {
        return dependencies;
    }

    public void setDependencies(List<String> dependencies) {
        this.dependencies = dependencies;
    }

    public void setFiles(List<File> files) {
        this.files = files;
    }
}
