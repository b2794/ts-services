package org.semantics.ts.model.mapping;

public class MappingResource {

    private String modelName;

    private String modelFile;

    private String inputJson;

    public MappingResource() {
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getModelFile() {
        return modelFile;
    }

    public void setModelFile(String modelFile) {
        this.modelFile = modelFile;
    }

    public String getInputJson() {
        return inputJson;
    }

    public void setInputJson(String inputJson) {
        this.inputJson = inputJson;
    }

    public Boolean isEmpty() {
        return this.modelFile == null && this.modelName == null && this.inputJson == null;
    }
}
