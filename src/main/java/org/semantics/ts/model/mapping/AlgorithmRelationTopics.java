package org.semantics.ts.model.mapping;
import java.util.List;

public class AlgorithmRelationTopics{
    List<AlgorithmRelationRaw> algorithmRelations;

    public AlgorithmRelationTopics(List<AlgorithmRelationRaw> algorithmRelations) {
        this.algorithmRelations = algorithmRelations;
    }

    public AlgorithmRelationTopics(String id, String name, List<AlgorithmRelationRaw> algorithmRelations) {
        this.algorithmRelations = algorithmRelations;
    }


    public List<AlgorithmRelationRaw> getAlgorithmRelations() {
        return algorithmRelations;
    }

    public void setAlgorithmRelations(List<AlgorithmRelationRaw> algorithmRelations) {
        this.algorithmRelations = algorithmRelations;
    }
}
