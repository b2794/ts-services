package org.semantics.ts.model.mapping;

public class AlgorithmRelationType extends EntityMappingBasic {
    String inverseTypeName;

    public AlgorithmRelationType(String id, String name, String inverseTypeName) {
        super(id, name);
        this.inverseTypeName = inverseTypeName;
    }

    public String getInverseTypeName() {
        return inverseTypeName;
    }

    public void setInverseTypeName(String inverseTypeName) {
        this.inverseTypeName = inverseTypeName;
    }
}
