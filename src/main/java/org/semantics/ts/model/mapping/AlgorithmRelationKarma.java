package org.semantics.ts.model.mapping;

import com.google.gson.annotations.SerializedName;

public class AlgorithmRelationKarma {
    String id;
    String uses;
    @SerializedName(value = "is_parent_of", alternate = "isParentOf")
    String isParentOf;
    @SerializedName(value = "is_specific_type_of", alternate = "isSpecificTypeOf")
    String isSpecificTypeOf;

    public AlgorithmRelationKarma(String id) {
        this.id = id;
    }

    public AlgorithmRelationKarma(String id, String uses, String isParentOf, String isSpecificTypeOf) {
        this.id = id;
        this.uses = uses;
        this.isParentOf = isParentOf;
        this.isSpecificTypeOf = isSpecificTypeOf;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUses() {
        return uses;
    }

    public void setUses(String uses) {
        this.uses = uses;
    }

    public String getIsParentOf() {
        return isParentOf;
    }

    public void setIsParentOf(String isParentOf) {
        this.isParentOf = isParentOf;
    }

    public String getIsSpecificTypeOf() {
        return isSpecificTypeOf;
    }

    public void setIsSpecificTypeOf(String isSpecificTypeOf) {
        this.isSpecificTypeOf = isSpecificTypeOf;
    }
}
