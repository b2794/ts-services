package org.semantics.ts.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRawValue;

@JsonPropertyOrder({"page", "pageCount", "totalCount", "prevPage", "nextPage", "links",
    "collection", "context"})
public class TSResponse<T> {

  @JsonRawValue
  private Integer page;

  @JsonRawValue
  private Integer pageCount;

  @JsonRawValue
  private Integer totalCount;

  @JsonRawValue
  private Integer prevPage;

  @JsonRawValue
  private Integer nextPage;

  @JsonRawValue
  private String links;

  T collection;

  public TSResponse() {}

  public TSResponse(T results) {
    this.collection = results;
  }

  @JsonProperty("collection")
  public T getCollection() {
    return this.collection;
  }

  public void setCollection(T collection) {
    this.collection = collection;
  }

  public void setLinks(String links) {
    this.links = links;
  }

  public void setPage(Integer page) {
    this.page = page;
  }

  public void setPageCount(Integer pageCount) {
    this.pageCount = pageCount;
  }

  public void setTotalCount(Integer totalCount) {
    this.totalCount = totalCount;
  }

  public void setPrevPage(Integer prevPage) {
    this.prevPage = prevPage;
  }

  public void setNextPage(Integer nextPage) {
    this.nextPage = nextPage;
  }

  public Integer getPrevPage() {
    return prevPage;
  }

  public Integer getNextPage() {
    return nextPage;
  }

}
