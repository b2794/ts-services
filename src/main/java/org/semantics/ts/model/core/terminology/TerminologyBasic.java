package org.semantics.ts.model.core.terminology;


import org.semantics.ts.model.json.TerminologyLinks;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TerminologyBasic<T> {

  TerminologyLinks links;

  @JsonProperty("@context")
  private T context;

  public TerminologyBasic() {}

  public TerminologyLinks getLinks() {
    return links;
  }

  public void setLinks(TerminologyLinks links) {
    this.links = links;
  }

  public void setContext(T context) {
    this.context = context;
  }

}
