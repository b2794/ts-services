package org.semantics.ts.model.core.term;

import org.semantics.ts.model.json.TermLinks;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TermDataBasic<T> {
  String uri;
  String label;

  private TermLinks links;

  @JsonProperty("@context")
  private T context;

  public TermDataBasic() {}

  public TermDataBasic(String uri, String label) {
    this.uri = uri;
    this.label = label;
  }

  public String getUri() {
    return uri;
  }

  public void setUri(String uri) {
    this.uri = uri;
  }

  public TermLinks getLinks() {
    return links;
  }

  public void setLinks(TermLinks links) {
    this.links = links;
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public T getContext() {
    return context;
  }

  public void setContext(T context) {
    this.context = context;
  }

}
