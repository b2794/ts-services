package org.semantics.ts.model.core.terminology;

public class TerminologyDataExtended extends TerminologyData {

  String released;
  String hasOntologyLanguage;

  public TerminologyDataExtended(String name, String acronym, String definition, String uri,
      String releaseDate, String hasOntologyLanguage) {
    this.name = name;
    this.acronym = acronym;
    this.definition = definition;
    this.uri = uri;
    this.released = releaseDate;
    this.hasOntologyLanguage = hasOntologyLanguage;
  }

  public String getHasOntologyLanguage() {
    return this.hasOntologyLanguage;
  }

  public void setHasOntologyLanguage(String hasOntologyLanguage) {
    this.hasOntologyLanguage = hasOntologyLanguage;
  }

  public String getReleased() {
    return released;
  }

  public void setReleased(String released) {
    this.released = released;
  }

}
