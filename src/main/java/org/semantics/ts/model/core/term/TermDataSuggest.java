package org.semantics.ts.model.core.term;

public class TermDataSuggest extends TermDataBasic {
    String sourceTerminology;

    public TermDataSuggest(String sourceTerminology) {
        this.sourceTerminology = sourceTerminology;
    }

    public TermDataSuggest(String uri, String label, String sourceTerminology) {
        super(uri, label);
        this.sourceTerminology = sourceTerminology;
    }

    public String getSourceTerminology() {
        return sourceTerminology;
    }

    public void setSourceTerminology(String sourceTerminology) {
        this.sourceTerminology = sourceTerminology;
    }


}
