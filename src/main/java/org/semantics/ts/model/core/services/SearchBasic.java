package org.semantics.ts.model.core.services;

import java.util.List;
import java.util.Optional;

public class SearchBasic {

    private Boolean searchDataOnly;
    private List<String> terminologies;
    private String searchTerm;
    private Integer limit;
    private Integer offset;
    private Optional<String> language;

    public SearchBasic(Boolean searchDataOnly, List<String> terminologies, String searchTerm,
                       Integer limit, Integer offset, Optional<String> language) {
        this.searchDataOnly = searchDataOnly;
        this.terminologies = terminologies;
        this.searchTerm = searchTerm;
        this.limit = limit;
        this.offset = offset;
        this.language = language;
    }

    public List<String> getTerminologies() {
        return terminologies;
    }

    public void setTerminologies(List<String> terminologies) {
        this.terminologies = terminologies;
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    public Optional<String> getLanguage() {
        return language;
    }

    public void setLanguage(Optional<String> language) {
        this.language = language;
    }

    public Boolean getSearchDataOnly() {
        return searchDataOnly;
    }

    public void setSearchDataOnly(Boolean searchDataOnly) {
        this.searchDataOnly = searchDataOnly;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

}
