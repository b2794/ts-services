package org.semantics.ts.model.core.term;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class TermDataFull extends TermDataAdvanced {

  List<String> subClassOf;
  String definition;
  String status;


  public TermDataFull(String sourceTerminology, String uri, String label, List<String> types,
      List<String> subClassOf, List<String> synonyms, List<String> acronyms, String definition,
      String status) {
    super(sourceTerminology, types, synonyms, acronyms);
    this.definition = definition;
    this.subClassOf = subClassOf;
    this.status = status;
  }

  public List<String> getSubClassOf() {
    return this.subClassOf;
  }

  public void setSubClassOf(List<String> subClassOf) {
    this.subClassOf = subClassOf;
  }

  public String getDefinition() {
    return definition;
  }

  public void setDefinition(String definition) {
    this.definition = definition;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

}
