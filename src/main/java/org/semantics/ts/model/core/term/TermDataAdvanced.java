package org.semantics.ts.model.core.term;

import java.util.List;

public class TermDataAdvanced extends TermDataBasic {
  String sourceTerminology;
  List<String> type;
  List<String> synonym;
  List<String> acronym;


  public TermDataAdvanced(String sourceTerminology, List<String> types, List<String> synonyms,
      List<String> acronyms) {
    this.sourceTerminology = sourceTerminology;
    this.type = types;
    this.synonym = synonyms;
    this.acronym = acronyms;
  }


  public TermDataAdvanced(String sourceTerminology, String uri, String label, List<String> types,
      List<String> synonyms, List<String> acronym) {
    super(uri, label);
    this.sourceTerminology = sourceTerminology;
    this.type = types;
    this.synonym = synonyms;
    this.acronym = acronym;

  }

  public String getSourceTerminology() {
    return sourceTerminology;
  }

  public void setSourceTerminology(String sourceTerminology) {
    this.sourceTerminology = sourceTerminology;
  }

  public List<String> getSynonym() {
    return synonym;
  }

  public void setSynonym(List<String> synonym) {
    this.synonym = synonym;
  }

  public List<String> getAcronym() {
    return acronym;
  }

  public void setAcronym(List<String> acronym) {
    this.acronym = acronym;
  }

  public List<String> getType() {
    return type;
  }

  public void setType(List<String> type) {
    this.type = type;
  }
}
