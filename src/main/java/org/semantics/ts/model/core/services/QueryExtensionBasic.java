package org.semantics.ts.model.core.services;

import java.util.List;

public class QueryExtensionBasic {
    String label;

    List<String> synonyms;

    List<String> acronyms;

    public QueryExtensionBasic() {
    }


    public QueryExtensionBasic(String label, List<String> synonyms, List<String> acronyms) {
        this.label = label;
        this.synonyms = synonyms;
        this.acronyms = acronyms;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<String> getSynonyms() {
        return synonyms;
    }

    public void setSynonyms(List<String> synonyms) {
        this.synonyms = synonyms;
    }

    public List<String> getAcronyms() {
        return acronyms;
    }

    public void setAcronyms(List<String> acronyms) {
        this.acronyms = acronyms;
    }


}
