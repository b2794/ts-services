package org.semantics.ts.model.core.terminology;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class TerminologyArchive extends TerminologyMetrics {

  String releaseDate;
  String resourceLocator;
  String archivingDate;
  String archiveID;
  String uri;

  public TerminologyArchive() {}

  public TerminologyArchive(String releaseDate, String resourceLocator, String archivingDate,
      String archiveID, String uri) {
    this.releaseDate = releaseDate;
    this.resourceLocator = resourceLocator;
    this.archivingDate = archivingDate;
    this.archiveID = archiveID;
    this.uri = uri;
  }

  public String getResourceLocator() {
    return resourceLocator;
  }

  public void setResourceLocator(String resourceLocator) {
    this.resourceLocator = resourceLocator;
  }

  public String getArchivingDate() {
    return archivingDate;
  }

  public void setArchivingDate(String archivingDate) {
    this.archivingDate = archivingDate;
  }

  public String getArchiveID() {
    return archiveID;
  }

  public void setArchiveID(String archiveID) {
    this.archiveID = archiveID;
  }

  public String getReleaseDate() {
    return releaseDate;
  }

  public void setReleaseDate(String releaseDate) {
    this.releaseDate = releaseDate;
  }

  public String getUri() {
    return uri;
  }

  public void setUri(String uri) {
    this.uri = uri;
  }

}
