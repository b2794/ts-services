package org.semantics.ts.model.core.terminology;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class TerminologyData extends TerminologyBasic {
  String name;
  String acronym;
  String definition;
  String uri;
  String type;
  String lastArchiveID;

  public TerminologyData() {}

  public TerminologyData(String name, String acronym, String definition, String uri, String type,
      String lastArchiveID) {
    this.name = name;
    this.acronym = acronym;
    this.definition = definition;
    this.uri = uri;
    this.type = type;
    this.lastArchiveID = lastArchiveID;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAcronym() {
    return this.acronym;
  }

  public void setAcronym(String acronym) {
    this.acronym = acronym;
  }

  public String getDefinition() {
    return definition;
  }

  public void setDefinition(String definition) {
    this.definition = definition;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getUri() {
    return uri;
  }

  public void setUri(String uri) {
    this.uri = uri;
  }

  public String getLastArchiveID() {
    return lastArchiveID;
  }

  public void setLastArchiveID(String lastArchiveID) {
    this.lastArchiveID = lastArchiveID;
  }

}
