package org.semantics.ts.model.core.services;

import java.util.List;

public class QueryExtensionAdvanced extends QueryExtensionBasic {
    List<QueryExtensionBasic> allExtendedNarrower;

    public QueryExtensionAdvanced(String label, List<String> synonyms, List<String> acronyms, List<QueryExtensionBasic> allExtendedNarrower) {
        super(label, synonyms, acronyms);
        this.allExtendedNarrower = allExtendedNarrower;
    }

    public QueryExtensionAdvanced() {
    }

    public List<QueryExtensionBasic> getAllExtendedNarrower() {
        return allExtendedNarrower;
    }

    public void setAllExtendedNarrower(List<QueryExtensionBasic> allExtendedNarrower) {
        this.allExtendedNarrower = allExtendedNarrower;
    }
}
