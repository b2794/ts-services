package org.semantics.ts.model.core.services;

import java.util.List;

public class QueryExtensionBasicHelper extends QueryExtensionBasic {
    List<String> uris;

    public QueryExtensionBasicHelper() {
    }

    public QueryExtensionBasicHelper(List<String> uris) {
        this.uris = uris;
    }

    public QueryExtensionBasicHelper(String label, List<String> synonyms, List<String> acronyms, List<String> uris) {
        super(label, synonyms, acronyms);
        this.uris = uris;
    }


    public List<String> getUris() {
        return uris;
    }

    public void setUris(List<String> uris) {
        this.uris = uris;
    }
}
