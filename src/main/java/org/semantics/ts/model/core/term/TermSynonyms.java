package org.semantics.ts.model.core.term;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class TermSynonyms extends TermDataBasic {

  List<String> synonym;

  public TermSynonyms() {}

  public TermSynonyms(List<String> synonyms) {
    this.synonym = synonyms;
  }

  public List<String> getSynonym() {
    return this.synonym;
  }

  public void setSynonym(List<String> synonym) {
    this.synonym = synonym;
  }

}
