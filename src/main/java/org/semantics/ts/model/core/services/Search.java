package org.semantics.ts.model.core.services;


import java.util.List;
import java.util.Optional;

public class Search extends SearchBasic {

    private String matchType;


    public Search(Boolean aboxSearch, List<String> terminologies, String searchTerm,  String matchType,
                  Integer limit, Integer offset, Optional<String> language) {
        super(aboxSearch, terminologies, searchTerm,  limit, offset, language);
        this.matchType = matchType;
    }

    public String getMatchType() {
        return matchType;
    }

    public void setMatchType(String matchType) {
        this.matchType = matchType;
    }


}
