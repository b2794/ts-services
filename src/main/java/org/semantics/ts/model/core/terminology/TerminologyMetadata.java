package org.semantics.ts.model.core.terminology;

import java.util.List;

public class TerminologyMetadata extends TerminologyBasic {

  String title;
  String license;
  String versionIRI;
  String type;
  List<String> creator;
  List<String> contributor;

  public TerminologyMetadata() {}

  public TerminologyMetadata(String creationDate, String title, String status, String license,
      String versionIRI, String preferredNamespacePrefix, String type, List<String> creator,
      List<String> contributor) {
    this.title = title;
    this.license = license;
    this.versionIRI = versionIRI;
    this.type = type;
    this.creator = creator;
    this.contributor = contributor;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getLicense() {
    return license;
  }

  public void setLicense(String license) {
    this.license = license;
  }

  public String getVersionIRI() {
    return versionIRI;
  }

  public void setVersionIRI(String versionIRI) {
    this.versionIRI = versionIRI;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public List<String> getCreator() {
    return creator;
  }

  public void setCreator(List<String> creator) {
    this.creator = creator;
  }

  public List<String> getContributor() {
    return contributor;
  }

  public void setContributor(List<String> contributor) {
    this.contributor = contributor;
  }


}
