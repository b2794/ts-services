package org.semantics.ts.model.core.terminology;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class TerminologyMetrics extends TerminologyBasic {

  String averageNumberOfChildren;
  String classesWithASingleChild;
  String classesWithMoreThan1Parent;
  String classesWithMoreThan25Children;
  String classesWithoutDefinition;
  String classesWithoutLabel;
  String maximumDepth;
  String maximumNumberOfChildren;
  String numberOfLeaves;
  String numClasses;
  String numIndividuals;
  String numProperties;


  public TerminologyMetrics() {}

  public TerminologyMetrics(String averageNumberOfChildren, String classesWithASingleChild,
      String classesWithMoreThan1Parent, String classesWithMoreThan25Children,
      String classesWithoutDefinition, String classesWithoutLabel, String maximumDepth,
      String maximumNumberOfChildren, String numberOfLeaves, String numClasses,
      String numIndividuals, String numProperties) {
    this.averageNumberOfChildren = averageNumberOfChildren;
    this.classesWithASingleChild = classesWithASingleChild;
    this.classesWithMoreThan1Parent = classesWithMoreThan1Parent;
    this.classesWithMoreThan25Children = classesWithMoreThan25Children;
    this.classesWithoutDefinition = classesWithoutDefinition;
    this.classesWithoutLabel = classesWithoutLabel;
    this.maximumDepth = maximumDepth;
    this.maximumNumberOfChildren = maximumNumberOfChildren;
    this.numberOfLeaves = numberOfLeaves;
    this.numClasses = numClasses;
    this.numIndividuals = numIndividuals;
    this.numProperties = numProperties;
  }

  public String getAverageNumberOfChildren() {
    return this.averageNumberOfChildren;
  }

  public void setAverageNumberOfChildren(String averageNumberOfChildren) {
    this.averageNumberOfChildren = averageNumberOfChildren;
  }

  public String getClassesWithASingleChild() {
    return this.classesWithASingleChild;
  }

  public void setClassesWithASingleChild(String classesWithASingleChild) {
    this.classesWithASingleChild = classesWithASingleChild;
  }

  public String getClassesWithMoreThan1Parent() {
    return this.classesWithMoreThan1Parent;
  }

  public void setClassesWithMoreThan1Parent(String classesWithMoreThan1Parent) {
    this.classesWithMoreThan1Parent = classesWithMoreThan1Parent;
  }

  public String getClassesWithMoreThan25Children() {
    return this.classesWithMoreThan25Children;
  }

  public void setClassesWithMoreThan25Children(String classesWithMoreThan25Children) {
    this.classesWithMoreThan25Children = classesWithMoreThan25Children;
  }

  public String getClassesWithoutDefinition() {

    return this.classesWithoutDefinition;

  }

  public void setClassesWithoutDefinition(String classesWithoutDefinition) {
    this.classesWithoutDefinition = classesWithoutDefinition;
  }

  public String getClassesWithoutLabel() {
    return this.classesWithoutLabel;
  }

  public void setClassesWithoutLabel(String classesWithoutLabel) {
    this.classesWithoutLabel = classesWithoutLabel;
  }

  public String getMaximumDepth() {
    return this.maximumDepth;
  }

  public void setMaximumDepth(String maximumDepth) {
    this.maximumDepth = maximumDepth;
  }

  public String getMaximumNumberOfChildren() {
    return this.maximumNumberOfChildren;
  }

  public void setMaximumNumberOfChildren(String maximumNumberOfChildren) {
    this.maximumNumberOfChildren = maximumNumberOfChildren;
  }

  public String getNumberOfLeaves() {
    return this.numberOfLeaves;
  }

  public void setNumberOfLeaves(String numberOfLeaves) {
    this.numberOfLeaves = numberOfLeaves;
  }

  public String getNumClasses() {
    return this.numClasses;
  }

  public void setNumClasses(String numClasses) {
    this.numClasses = numClasses;
  }

  public String getNumIndividuals() {
    return this.numIndividuals;
  }

  public void setNumIndividuals(String numIndividuals) {
    this.numIndividuals = numIndividuals;
  }

  public String getNumProperties() {
    return this.numProperties;
  }

  public void setNumProperties(String numProperties) {
    this.numProperties = numProperties;
  }

}
