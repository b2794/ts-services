package org.semantics.ts.utils;

import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

// public class TSCache<T> {
//
// // defines a Guava cache, which will store a list of objects for a given key String
// private Cache<String, List<T>> cache;
//
// // Constructor to build Cache Store
// public TSCache(int expiryDuration, TimeUnit timeUnit) {
// cache = CacheBuilder.newBuilder().expireAfterWrite(expiryDuration, timeUnit)
// .concurrencyLevel(Runtime.getRuntime().availableProcessors()).build();
// }
//
// // Method to fetch previously stored record using record key
// public List<T> get(String key) {
// return cache.getIfPresent(key);
// }
//
// // Method to put a new record in Cache Store with record key
// public void add(String key, List<T> value) {
// if (key != null && value != null) {
// cache.put(key, value);
// System.out.println(
// "Record stored in " + value.getClass().getSimpleName() + " Cache with Key = " + key);
// }
// }
// }
public class TSCache {

  private static final Logger LOGGER = LoggerFactory.getLogger(TSCache.class);

  // defines a Guava cache, which will store a list of objects for a given key String
  private Cache<String, Integer> cache;

  // Constructor to build Cache Store
  public TSCache(int expiryDuration, TimeUnit timeUnit) {
    cache = CacheBuilder.newBuilder().expireAfterWrite(expiryDuration, timeUnit)
        .concurrencyLevel(Runtime.getRuntime().availableProcessors()).build();
  }

  // Method to fetch previously stored record using record key
  public Integer get(String key) {
    // LOGGER.info("fetching cached query count for key " + key);
    return cache.getIfPresent(key);
  }

  // Method to put a new record in Cache Store with record key
  public void add(String key, Integer value) {
    if (key != null && value != null) {
      cache.put(key, value);

      LOGGER.info(
          "Record stored in " + value.getClass().getSimpleName() + " Cache with Key = " + key);
    }
  }
}
