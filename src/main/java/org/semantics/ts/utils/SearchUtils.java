package org.semantics.ts.utils;

import org.semantics.ts.model.core.services.Search;
import org.semantics.ts.model.core.services.SearchBasic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.InternalServerErrorException;
import java.util.List;

public class SearchUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(SearchUtils.class);

    public static boolean checkIfSearchTermIsLongEnough(String searchTerm) {
        if (searchTerm.length() <= 0) {
            LOGGER.warn("Search term \"" + searchTerm + "\" is too small. " +
                    "The service does not return results for queries shorter than 4 characters.");
            return false;
        } else if (searchTerm.length() - searchTerm.lastIndexOf(" ") - 1 <= 3) {
            LOGGER.warn("Search term \"" + searchTerm + "\" is too small. " +
                    "The service does not return results for wildcard queries with less than 4 leading characters");
            return false;
        } else return true;
    }

    public static void logSearchRequest(Search searchParams) {
        List<String> terminologies = searchParams.getTerminologies();
        if (terminologies == null || terminologies.isEmpty()) {
            LOGGER.info("Search query: " + searchParams.getSearchTerm() + " match type: " + searchParams.getMatchType()
                    + " language " + searchParams.getLanguage()
                    + " over all org.semantics.planqk.ts.RESTfulWS.internal terminologies");
        } else {
            LOGGER.info("Search query: " + searchParams.getSearchTerm() + " match type: " + searchParams.getMatchType()
                    + " language " + searchParams.getLanguage() + " over the following terminologies: " + terminologies);
        }
    }

    public static String checkSearchTerm(String searchTerm) {
        String noiseWordFilter = searchTerm.replaceAll("[^A-Za-z0-9]", "");
        if (!noiseWordFilter.equals("")) {
            searchTerm = searchTerm.replace("\"", "").replace("'", "");
            return searchTerm;
        } else throw new InternalServerErrorException("Search query consists of noise words only: " + searchTerm);
    }

    public static boolean checkParameters(List<String> terminologies) {
        /*
         * RegEx to detect typing errors like: col COL, COL, BEFDATA COL,,BEFDATA "!������,������$",=)(,
         */
        for (String t : terminologies) {
            if (!t.toUpperCase().matches("(([A-Z_]+,)*([A-Z_]+))?")) {
                return false;
            }
        }
        return true;
    }

    public static Search constructSearchParams(SearchBasic searchParams){
        Search params = new Search(searchParams.getSearchDataOnly(), searchParams.getTerminologies(), searchParams.getSearchTerm(),
                "exact", searchParams.getLimit(), searchParams.getOffset(), searchParams.getLanguage());
        return params;
    }



}
