package org.semantics.ts.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.semantics.ts.dao.MappingDataAccessService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.IOException;

public class FileUtils {
    private static File getFile(String inputFile) {
        Resource resource = new ClassPathResource(inputFile.replace("classpath:/", ""));
        try{
            return resource.getFile();
        }catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }


}
