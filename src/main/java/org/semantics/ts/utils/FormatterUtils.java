package org.semantics.ts.utils;

import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.apache.jena.query.QuerySolution;
import org.semantics.ts.configs.StandardVariables;
import org.semantics.ts.model.TSRequest;
import org.semantics.ts.model.TSResponse;
import org.semantics.ts.model.json.TermContext;
import org.semantics.ts.model.json.TermLinks;
import org.semantics.ts.model.json.TerminologyContext;
import org.semantics.ts.model.json.TerminologyLinks;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

public class FormatterUtils {

  @Deprecated
  public static TSRequest formatRequestField(String query) {
    JsonObject tsRequestJson = new JsonObject();
    tsRequestJson.addProperty("query", query);
    tsRequestJson.addProperty("executionTime", new java.util.Date().toString());
    return new Gson().fromJson(tsRequestJson, TSRequest.class);
  }

  @Deprecated
  public static TSRequest formatTSRequestData(HttpServletRequest uriInfo, String path) {
    String query = FormatterUtils.formatQuery(uriInfo, path);
    return FormatterUtils.formatRequestField(query);
  }

  public static String formatQuery(HttpServletRequest uriInfo, String path) {
    final String calling_uri = uriInfo.getRequestURI();
    final int truncIndex = calling_uri.indexOf(path);
    return StandardVariables.getApiUrl() + calling_uri.substring(truncIndex);
  }

  public static int getQueryOffset(Optional<Integer> page, Optional<Integer> limit) {

    int offset = 0;

    if (getValueFromOptionalOrDefault(page, 0) >= 1) {
      offset = getValueFromOptionalOrDefault(limit, StandardVariables.getLimitAllTermsService())
          * (page.get());
    }

    return offset;
  }

  /**
   * 
   * @param <T>
   * @param opt Optional object, from which to extract a value (if present)
   * @param defaultValue a default value to return, if no Optional value is present
   * @return Optional value or default value
   */
  public static <T> T getValueFromOptionalOrDefault(Optional<T> opt, T defaultValue) {

    // could be String or an Integer
    T value = defaultValue;

    if (opt != null && opt.isPresent() == true) {
      value = opt.get();
    }
    // else returns null

    return value;

  }

  /**
   * This produces the pagination information on top of the JSON payload
   * 
   * @param uriInfo
   * @param size Total size of items to show
   * @param page Current page
   * @param limit Query LIMIT
   * @return TSResponse object containing the pagination as JSON string
   */
  public static TSResponse addPagination(HttpServletRequest uriInfo, int size,
      Optional<Integer> page, Optional<Integer> limit) {

    StringBuilder links = new StringBuilder();

    int page_ = getValueFromOptionalOrDefault(page, 0);
    int limit_ = getValueFromOptionalOrDefault(limit, StandardVariables.getLimitAllTermsService());

    TSResponse response = new TSResponse();
    response.setPage(page_);
    response.setPageCount(size / (limit_ == 0 ? 1 : limit_));
    response.setTotalCount(size);
    response.setPrevPage(page_ == 0 ? null : page_ - 1);
    response.setNextPage(size > (limit_ * (page_ + 1)) == true ? page_ + 1 : null);

    if ((response.getPrevPage() == null) && (response.getNextPage() == null)) {
      links.append("{ \"nextPage\":" + null);
      links.append(", \"prevPage\":" + null + "}");

      // response.setLinks("{ \"nextPage\":" + null + ", \"prevPage\":" + null + "}");
    }
    // else if (response.getPrevPage() != null) {
    // links.append("{ \"nextPage\":\"");
    // links.append(FormatterUtils.formatQuery(uriInfo, "/terminologies"));
    // links.append(
    // response.getNextPage() != null ? "?page=" + response.getNextPage() + "&limit=" + limit_
    // : "?limit=" + limit_);
    // links.append("\", \"prevPage\":\"");
    // links.append(null + "\"}");

    // response.setLinks(
    // "{ \"nextPage\":\"" + FormatterUtils.formatQuery(uriInfo, "/terminologies") + "?page="
    // + response.getNextPage() + "&limit=" + limit_ + "\", \"prevPage\":\"" + null + "\"}");
    // }
    else {

      // set nextPage link
      if (response.getNextPage() != null) {
        links.append("{ \"nextPage\":\"");
        links.append(FormatterUtils.formatQuery(uriInfo, "/terminologies"));
        links.append("?page=" + response.getNextPage() + "&limit=" + limit_);
        links.append("\",");
      } else {
        links.append("{ \"nextPage\":" + null);
        links.append(" ,");
      }


      // set prevPage link
      if (response.getPrevPage() != null) {
        links.append(" \"prevPage\":\"");
        links.append(FormatterUtils.formatQuery(uriInfo, "/terminologies"));
        links.append("?page=" + response.getPrevPage() + "&limit=" + limit_);
        links.append("\"}");
      } else {
        links.append(" \"prevPage\":" + null + "}");
      }

      // response.setLinks("{ \"nextPage\":\"" + FormatterUtils.formatQuery(uriInfo,
      // "/terminologies")
      // + "?page=" + response.getNextPage() + "&limit=" + limit_ + "\", \"prevPage\":\""
      // + FormatterUtils.formatQuery(uriInfo, "/terminologies") + "?page="
      // + response.getPrevPage() + "&limit=" + limit_ + "\"}");
    }
    response.setLinks(links.toString());

    return response;
  }

  public static JsonArray processArrayFields(JsonArray data, String value) {
    JsonPrimitive element = new JsonPrimitive(value);
    data.add(element);
    return data;
  }

  public static String formatField(QuerySolution currentResult, String field) {
    String value = null;
    if (currentResult.get(field) != null) {
      value = currentResult.get(field).toString();
    }
    return value;
  }

  public static JsonObject formatTermInfo(QuerySolution current_result) {
    JsonObject builder = new JsonObject();
    builder.addProperty("uri", formatField(current_result, "uri"));
    builder.addProperty("label", formatField(current_result, "label"));
    return builder;
  }

  public static JsonObject formatHierarchyResult(Pair<String, String> entry, String uri,
      JsonArray hierarchyArray) {
    JsonObject builder = new JsonObject();
    builder.addProperty("label", entry.getLeft());
    builder.addProperty("uri", uri);
    builder.add("hierarchy", hierarchyArray);
    return builder;
  }

  public static JsonArray convertListOfStringsToJsonArray(List<String> list) {
    JsonArray arr = new JsonArray();
    for (String jsonString : list) {
      arr.add(jsonString);
    }
    return arr;
  }

  public static String replaceSpaceWithAndString(String string) {
    return string.replace(" ", " and ");
  }

  public static String delQuotesFromString(String string) {
    return string.replaceAll("\"", "");
  }

  public static String delRedundantSpace(String string) {
    return string.replace("  ", " ");
  }

  public static String delSpaceAtTheEnd(String string) {
    return string.replaceAll("\\s+$", "");
  }

  public static String delLogicalOperatorsFromString(String string) {
    string = string.replace(" and ", " ");
    string = string.replace(" and", "");
    string = string.replace(" or ", " ");
    string = string.replace(" or", "");
    return string;
  }

  public static String formatSearchString(String searchString) {
    searchString = delQuotesFromString(searchString);
    searchString = delSpaceAtTheEnd(searchString);
    searchString = delLogicalOperatorsFromString(searchString);
    searchString = delRedundantSpace(searchString);
    searchString = replaceSpaceWithAndString(searchString);
    return searchString;
  }

  /**
   * 
   * @param acronym
   * @return
   */
  public static TerminologyLinks buildTerminologyLinks(String acronym) {

    TerminologyLinks links = new TerminologyLinks();
    TerminologyContext context = new TerminologyContext();

    // TODO use actual subdomain links
    links.setSelf(StandardVariables.getApiUrl() + "/core/terminologies/" + acronym);
    links.setMetrics(StandardVariables.getApiUrl() + "/core/terminologies/" + acronym + "/metrics");
    links.setMetadata(
        StandardVariables.getApiUrl() + "/core/terminologies/" + acronym + "/metadata");
    links.setLatest(StandardVariables.getApiUrl() + "/core/terminologies/" + acronym + "/latest");
    links.setDownload(
        StandardVariables.getApiUrl() + "/core/terminologies/" + acronym + "/download");
    links.setArchive(StandardVariables.getApiUrl() + "/core/terminologies/" + acronym + "/archive");
    links.setAllterms(
        StandardVariables.getApiUrl() + "/core/terminologies/" + acronym + "/allterms");
    links.setChangelog(
        StandardVariables.getApiUrl() + "/core/terminologies/" + acronym + "/changelog");

    // TODO should not be hard coded?
    context.setSelf("http://omv.ontoware.org/2005/05/ontology#Ontology");
    context.setMetrics("http://omv.ontoware.org/2005/05/ontology#Ontology");
    context.setMetadata("http://omv.ontoware.org/2005/05/ontology#Ontology");
    context.setLatest("http://omv.ontoware.org/2005/05/ontology#Ontology");
    context.setDownload("http://omv.ontoware.org/2005/05/ontology#Ontology");
    context.setArchive("http://omv.ontoware.org/2005/05/ontology#Ontology");
    context.setAllterms("http://omv.ontoware.org/2005/05/ontology#Ontology");
    context.setChangelog("http://omv.ontoware.org/2005/05/ontology#Ontology");

    links.setContext(context);

    return links;
  }

  /**
   * 
   * Adds term related hypermedia links to the builder object
   * 
   * @param uri Term URI
   * @param acronym Terminology acronym
   * @return TermLinks object which contains links
   */
  public static TermLinks buildTermLinks(String uri, String acronym) {
    TermLinks links = new TermLinks();
    TermContext context = new TermContext();

    // TODO use actual subdomain links
    links.setSelf(
        StandardVariables.getApiUrl() + "/core/terminologies/" + acronym + "/term?uri=" + uri);
    links.setParents(StandardVariables.getApiUrl() + "/hierarchy/terminologies/" + acronym
        + "/parents?uri=" + uri);
    links.setChildren(StandardVariables.getApiUrl() + "/hierarchy/terminologies/" + acronym
        + "/children?uri=" + uri);
    links.setAncestors(StandardVariables.getApiUrl() + "/hierarchy/terminologies/" + acronym
        + "/ancestors?uri=" + uri);
    links.setDescendants(StandardVariables.getApiUrl() + "/hierarchy/terminologies/" + acronym
        + "/descendants?uri=" + uri);

    // TODO should not be hard coded?
    context.setSelf("http://www.w3.org/2002/07/owl#Class");
    context.setParents("http://www.w3.org/2002/07/owl#Class");
    context.setChildren("http://www.w3.org/2002/07/owl#Class");
    context.setAncestors("http://www.w3.org/2002/07/owl#Class");
    context.setDescendants("http://www.w3.org/2002/07/owl#Class");

    links.setContext(context);

    return links;
  }
}
