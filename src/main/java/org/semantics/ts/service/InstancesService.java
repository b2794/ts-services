package org.semantics.ts.service;

import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.semantics.ts.dao.InstancesDao;
import org.semantics.ts.model.TSRequest;
import org.semantics.ts.model.TSResponse;
import org.semantics.ts.model.core.term.TermDataBasic;
import org.semantics.ts.utils.FormatterUtils;
import org.springframework.stereotype.Service;

@Service
public class InstancesService {
  private final InstancesDao instancesDao;


  public InstancesService(InstancesDao instancesDao) {
    this.instancesDao = instancesDao;
  }

  public TSResponse getDirectInstances(HttpServletRequest uriInfo, String acronym, String uri,
      Optional<String> lang) {
    TSRequest tsRequestData = FormatterUtils.formatTSRequestData(uriInfo, "/terminologies");
    List<TermDataBasic> resultList = instancesDao.getDirectInstances(acronym, uri, lang);
    return new TSResponse(resultList);
  }
}
