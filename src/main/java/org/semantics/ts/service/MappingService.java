package org.semantics.ts.service;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import org.semantics.ts.dao.MappingDao;
import org.semantics.ts.model.mapping.MappingBasic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;


@Service
public class MappingService {
  private static final Logger LOGGER = LoggerFactory.getLogger(MappingService.class);

  private final MappingDao mappingDao;

  @Value("${ts.mapping.config}")
  private Resource mappingFileConfig;

  @Autowired
  public MappingService(MappingDao mappingDao) {
    this.mappingDao = mappingDao;
  }


  /**
   * TO-DO: exclude/include artifact, GET instead of POST
   **/
  // public TSResponse getMapping(HttpServletRequest uriInfo, String artifact) throws Exception {
  // MappingBasic mappingConfig = readMappingConfig();
  // for (MappingResource resource : mappingConfig.getResources()) {
  // LOGGER.info("Perform JSON to RDF mapping for the artifact " + resource.getModelName() + ".");
  // mappingDao.getMapping(mappingConfig.getBaseInstanceUri(), resource.getModelName(),
  // resource.getModelFile(), resource.getInputJson());
  // }
  // TSRequest tsRequestData = FormatterUtils.formatTSRequestData(uriInfo, "/mapping");
  // return new TSResponse("Mapping complete");
  // }

  protected MappingBasic readMappingConfig() {
    LOGGER.info("Read mapping config");
    Gson gson = new Gson();
    MappingBasic mappingConfig = new MappingBasic();

    try {
      Reader reader = new InputStreamReader(mappingFileConfig.getInputStream());
      mappingConfig = gson.fromJson(reader, MappingBasic.class);
      reader.close();
    } catch (IOException e) {
      LOGGER.error(e.getMessage());
    }
    return mappingConfig;
  }


}
