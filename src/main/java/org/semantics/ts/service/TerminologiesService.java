package org.semantics.ts.service;

import java.io.File;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.semantics.ts.dao.CoreSharedServices;
import org.semantics.ts.dao.TermDao;
import org.semantics.ts.dao.TerminologyDao;
import org.semantics.ts.model.TSResponse;
import org.semantics.ts.model.core.term.TermDataFull;
import org.semantics.ts.model.core.term.TermSynonyms;
import org.semantics.ts.model.core.terminology.TerminologyArchive;
import org.semantics.ts.model.core.terminology.TerminologyData;
import org.semantics.ts.model.core.terminology.TerminologyDataExtended;
import org.semantics.ts.model.core.terminology.TerminologyMetadata;
import org.semantics.ts.model.core.terminology.TerminologyMetrics;
import org.semantics.ts.utils.FormatterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@SuppressWarnings("unchecked")
public class TerminologiesService {

  // @Autowired
  // private Environment env;

  private final TerminologyDao terminologyDao;

  private final TermDao termDao;

  @Autowired
  private CoreSharedServices sharedServices;

  @Autowired
  public TerminologiesService(TerminologyDao terminologyDao, TermDao termDao) {
    this.terminologyDao = terminologyDao;
    this.termDao = termDao;
  }

  public TSResponse getTerminologies(HttpServletRequest uriInfo, Optional<Integer> page,
      Optional<Integer> limit) {

    List<TerminologyData> resultList = terminologyDao.getTerminologies(page, limit);

    // retrieve COUNT
    // a cached count value should be already available at this point
    int count = sharedServices.retrieveCachedCount(null, "getTerminologies()");

    TSResponse response = FormatterUtils.addPagination(uriInfo, count, page, limit);
    response.setCollection(resultList);

    return response;
  }

  public TSResponse getTerminologyByAcronym(HttpServletRequest uriInfo, String acronym) {

    List<TerminologyData> resultList = terminologyDao.getTerminologyByAcronym(acronym);

    TSResponse response = FormatterUtils.addPagination(uriInfo, resultList.size(), null, null);
    response.setCollection(resultList);

    return response;
  }

  public TSResponse getAllTerms(HttpServletRequest uriInfo, String acronym,
      Optional<String> language, Optional<Integer> page, Optional<Integer> limit) {

    List<TermDataFull> resultList = termDao.getAllTerms(acronym, language, page, limit);

    // retrieve COUNT
    // a cached count value should be already available at this point
    int count = sharedServices.retrieveCachedCount(null, "getAllTerms()");

    TSResponse response = FormatterUtils.addPagination(uriInfo, count, page, limit);
    response.setCollection(resultList);

    return response;
  }

  public TSResponse getTermByUri(HttpServletRequest uriInfo, String acronym, String uri) {

    List<TermDataFull> resultList = termDao.getTermByUri(acronym, uri);

    TSResponse response = FormatterUtils.addPagination(uriInfo, resultList.size(), null, null);
    response.setCollection(resultList);

    return response;

  }

  public TSResponse getTermSynonymsByUri(HttpServletRequest uriInfo, String acronym, String uri,
      Optional<String> language) {

    List<TermSynonyms> resultList = termDao.getTermSynonymsByUri(acronym, uri, language);

    TSResponse response = FormatterUtils.addPagination(uriInfo, resultList.size(), null, null);
    response.setCollection(resultList);

    return response;
  }

  public TSResponse getTermAbbreviationsByUri(HttpServletRequest uriInfo, String acronym,
      String uri, Optional<String> language) {

    List<TermSynonyms> resultList = termDao.getTermSynonymsByUri(acronym, uri, language);

    TSResponse response = FormatterUtils.addPagination(uriInfo, resultList.size(), null, null);
    response.setCollection(resultList);

    return response;
  }

  public TSResponse getTerminologyMetrics(HttpServletRequest uriInfo, String acronym) {

    List<TerminologyMetrics> resultList = terminologyDao.getTerminologyMetrics(acronym);

    TSResponse response = FormatterUtils.addPagination(uriInfo, resultList.size(), null, null);
    response.setCollection(resultList);

    return response;
  }

  public TSResponse getTerminologyMetadata(HttpServletRequest uriInfo, String acronym) {

    List<TerminologyMetadata> resultList = terminologyDao.getTerminologyMetadata(acronym);

    TSResponse response = FormatterUtils.addPagination(uriInfo, resultList.size(), null, null);
    response.setCollection(resultList);

    return response;
  }

  public File getDownload(String acronym) {
    return terminologyDao.getDownload(acronym);
  }

  /**
   * 
   * @param uriInfo
   * @param acronym
   * @param archiveId
   * @param page
   * @param limit
   * @return
   */
  public TSResponse getTerminologyArchivedVersion(HttpServletRequest uriInfo, String acronym,
      Optional<Integer> archiveId, Optional<Integer> page, Optional<Integer> limit) {

    // TODO add LIMIT OFFSET
    List<TerminologyArchive> resultList =
        terminologyDao.getTerminologyArchivedVersion(acronym, archiveId);

    // retrieve COUNT
    // a cached count value should be already available at this point
    int count = sharedServices.retrieveCachedCount(null, "getTerminologyArchivedVersion()");

    TSResponse response = FormatterUtils.addPagination(uriInfo, count, page, limit);
    response.setCollection(resultList);

    return response;
  }

  public TSResponse getTerminologyLatestVersion(HttpServletRequest uriInfo, String acronym) {

    List<TerminologyDataExtended> resultList = terminologyDao.getTerminologyLatestVersion(acronym);

    TSResponse response = FormatterUtils.addPagination(uriInfo, resultList.size(), null, null);
    response.setCollection(resultList);

    return response;
  }

  public TSResponse getTerminologyChangelog(HttpServletRequest uriInfo, String acronym,
      Optional<Integer> page, Optional<Integer> limit) {

    List<TermDataFull> resultList = termDao.getTerminologyChangelog(acronym, page, limit);

    // retrieve COUNT
    // a cached count value should be already available at this point
    int count = sharedServices.retrieveCachedCount(null, "getTerminologyChangelog()");

    TSResponse response = FormatterUtils.addPagination(uriInfo, count, page, limit);
    response.setCollection(resultList);

    return response;
  }

}
