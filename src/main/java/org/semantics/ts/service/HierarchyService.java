package org.semantics.ts.service;

import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.semantics.ts.dao.CoreSharedServices;
import org.semantics.ts.dao.HierarchyDao;
import org.semantics.ts.model.TSResponse;
import org.semantics.ts.model.core.term.TermDataFull;
import org.semantics.ts.utils.FormatterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HierarchyService {

  private final HierarchyDao hierarchyDao;

  @Autowired
  private CoreSharedServices sharedServices;

  @Autowired
  public HierarchyService(HierarchyDao hierarchyDao) {
    this.hierarchyDao = hierarchyDao;
  }

  public TSResponse getChildren(HttpServletRequest uriInfo, String acronym, String uri,
      Optional<String> lang, Optional<Integer> page, Optional<Integer> limit) {

    List<TermDataFull> resultList = hierarchyDao.getChildren(acronym, uri, lang, page, limit);

    // retrieve COUNT
    // a cached count value should be already available at this point
    int count = sharedServices.retrieveCachedCount(null, "getChildren()");

    TSResponse response = FormatterUtils.addPagination(uriInfo, count, page, limit);
    response.setCollection(resultList);

    return response;
  }

  public TSResponse getDescendants(HttpServletRequest uriInfo, String acronym, String uri,
      Optional<String> lang, Optional<Integer> page, Optional<Integer> limit) {

    List<TermDataFull> resultList = hierarchyDao.getDescendants(acronym, uri, lang, page, limit);

    // retrieve COUNT
    // a cached count value should be already available at this point
    int count = sharedServices.retrieveCachedCount(null, "getDescendants()");

    TSResponse response = FormatterUtils.addPagination(uriInfo, count, page, limit);
    response.setCollection(resultList);

    return response;
  }

  public TSResponse getParents(HttpServletRequest uriInfo, String acronym, String uri,
      Optional<String> lang, Optional<Integer> page, Optional<Integer> limit) {

    List<TermDataFull> resultList = hierarchyDao.getParents(acronym, uri, lang, page, limit);

    // retrieve COUNT
    // a cached count value should be already available at this point
    int count = sharedServices.retrieveCachedCount(null, "getParents()");

    TSResponse response = FormatterUtils.addPagination(uriInfo, count, page, limit);
    response.setCollection(resultList);

    return response;
  }

  public TSResponse getAncestors(HttpServletRequest uriInfo, String acronym, String uri,
      Optional<String> lang, Optional<Integer> page, Optional<Integer> limit) {

    List<TermDataFull> resultList = hierarchyDao.getAncestors(acronym, uri, lang, page, limit);

    // retrieve COUNT
    // a cached count value should be already available at this point
    int count = sharedServices.retrieveCachedCount(null, "getAncestors()");

    TSResponse response = FormatterUtils.addPagination(uriInfo, count, page, limit);
    response.setCollection(resultList);

    return response;
  }

  // public TSResponse getHierarchy(HttpServletRequest uriInfo, String acronym,
  // String uri,
  // Optional<String> lang, Optional<Integer> page, Optional<Integer> limit) {
  //
  // List<HierarchyPath> resultList = hierarchyDao.getHierarchy(acronym, uri,
  // lang, page, limit);
  //
  // TSResponse response = FormatterUtils.formatResponse(uriInfo,
  // resultList.size(), page, limit);
  // response.setCollection(resultList);
  //
  // return response;
  // }

}
