package org.semantics.ts.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.semantics.ts.dao.HierarchyDao;
import org.semantics.ts.dao.SearchDao;
import org.semantics.ts.model.TSRequest;
import org.semantics.ts.model.TSResponse;
import org.semantics.ts.model.core.services.QueryExtensionAdvanced;
import org.semantics.ts.model.core.services.QueryExtensionBasic;
import org.semantics.ts.model.core.services.QueryExtensionBasicHelper;
import org.semantics.ts.model.core.services.Search;
import org.semantics.ts.model.core.services.SearchBasic;
import org.semantics.ts.model.core.term.TermDataAdvanced;
import org.semantics.ts.model.core.term.TermDataFull;
import org.semantics.ts.model.core.term.TermDataSuggest;
import org.semantics.ts.utils.FormatterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SearchService {
  private final SearchDao searchDao;
  private final HierarchyDao hierarchyDao;

  @Autowired
  public SearchService(SearchDao searchDao, HierarchyDao hierarchyDao) {
    this.searchDao = searchDao;
    this.hierarchyDao = hierarchyDao;
  }

  public TSResponse getSearch(HttpServletRequest uriInfo, Search searchParams) {
    TSRequest tsRequestData = FormatterUtils.formatTSRequestData(uriInfo, "/search");
    List<TermDataAdvanced> resultList = searchDao.getSearch(searchParams);
    return new TSResponse(resultList);
  }

  public TSResponse getSuggest(HttpServletRequest uriInfo, SearchBasic suggestParams) {
    TSRequest tsRequestData = FormatterUtils.formatTSRequestData(uriInfo, "/suggest");
    List<TermDataSuggest> resultList = searchDao.getSuggest(suggestParams);
    return new TSResponse(resultList);
  }

  public TSResponse getSearchQueryExpansion(HttpServletRequest uriInfo, SearchBasic searchParams) {
    TSRequest tsRequestData = FormatterUtils.formatTSRequestData(uriInfo, "/expansion");
    QueryExtensionBasicHelper resultHelper = searchDao.getSearchQueryExpansion(searchParams, false);
    QueryExtensionAdvanced resultListAdvanced = setFieldsOfTheMainSearchResult(resultHelper);
    if (!searchParams.getSearchDataOnly()) {
      List<QueryExtensionBasic> narrowerObjectList = new ArrayList<>();
      for (String uri : resultHelper.getUris()) {
        if (!uri.contains("/data/")) {
          List<String> terminologies = searchParams.getTerminologies();
          for (String t : terminologies) {
            narrowerObjectList = getNarrowerSearchResults(narrowerObjectList, t, uri, searchParams);
          }
        }
      }
      resultListAdvanced.setAllExtendedNarrower(narrowerObjectList);
    }
    return new TSResponse(resultListAdvanced);
  }

  private List<QueryExtensionBasic> getNarrowerSearchResults(
      List<QueryExtensionBasic> narrowerObjectList, String terminology, String uri,
      SearchBasic searchParams) {
    Optional<String> lang = searchParams.getLanguage();
    List<TermDataFull> allExtendedNarrower =
        hierarchyDao.getDescendants(terminology, uri, lang, null, null);
    for (TermDataFull narrower : allExtendedNarrower) {
      SearchBasic searchParamsNarrower = searchParams;
      String searchTerm = narrower.getLabel().replace("@en", "").replace("@de", "");
      searchParamsNarrower.setSearchTerm(searchTerm);
      QueryExtensionBasicHelper resultListForNarrower =
          searchDao.getSearchQueryExpansion(searchParamsNarrower, true);
      QueryExtensionBasic resultListNarrower = formatResultListForNarrower(resultListForNarrower);
      narrowerObjectList.add(resultListNarrower);
    }
    return narrowerObjectList;
  }

  private QueryExtensionAdvanced setFieldsOfTheMainSearchResult(
      QueryExtensionBasicHelper resultHelper) {
    QueryExtensionAdvanced resultListAdvanced = new QueryExtensionAdvanced();
    resultListAdvanced.setLabel(resultHelper.getLabel());
    resultListAdvanced.setAcronyms(resultHelper.getAcronyms());
    resultListAdvanced.setSynonyms(resultHelper.getSynonyms());
    return resultListAdvanced;
  }

  private QueryExtensionBasic formatResultListForNarrower(QueryExtensionBasicHelper resultList) {
    QueryExtensionBasic resultListNarrower = new QueryExtensionBasic();
    resultListNarrower.setLabel(resultList.getLabel());
    resultListNarrower.setSynonyms(resultList.getSynonyms());
    resultListNarrower.setAcronyms(resultList.getAcronyms());
    return resultListNarrower;
  }

}
