package org.semantics.ts.queries;

import org.semantics.ts.configs.StandardVariables;

import java.util.Optional;

public class InstancesQueries {
    public static String getDirectInstances(String terminology_id, String conceptUri, Optional<String> lang) {
        StringBuilder sparql = new StringBuilder("");
        if (terminology_id.equals("qco")){
            String planQKGraph =StandardVariables.getPlaqkPrefix();
            String sub_query = "SELECT ?uri ?label FROM <" + planQKGraph  + ">" +
                    "WHERE {?uri rdf:type <" + conceptUri + "> ." +
                    "?uri skos:prefLabel ?label .";
            sparql.append(sub_query);
           if (!lang.isEmpty()){
               sparql.append("FILTER (lang(?label) = \" + lang + \") .");
           }
           sparql.append("}");
        }
        return sparql.toString();
        }


    }
