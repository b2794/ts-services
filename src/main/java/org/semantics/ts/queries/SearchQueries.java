package org.semantics.ts.queries;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import javax.ws.rs.BadRequestException;
import org.semantics.ts.configs.StandardVariables;
import org.semantics.ts.model.core.services.Search;
import org.semantics.ts.model.core.services.SearchBasic;
import org.semantics.ts.utils.FormatterUtils;

public class SearchQueries {

  public static String getSPARQLSuggestSearch(SearchBasic suggestParams, String searchString,
      HashMap<String, String> map) {
    searchString = FormatterUtils.delQuotesFromString(searchString);
    String labelURIs = map.get("labelURIs");
    Optional<String> lang = suggestParams.getLanguage();
    StringBuilder query = new StringBuilder();
    query.append("SELECT DISTINCT ?uri, ?label, ?terminology, ?graph " + "FROM <"
        + StandardVariables.getUriprefix() + StandardVariables.getMetadataGraph() + "> "
        + "WHERE { quad map virtrdf:DefaultQuadMap " + "{ graph ?graph { ?uri ?labeluri ?label .");
    query.append(" FILTER ( (?labeluri = rdfs:label || ?labeluri = skos:prefLabel " + labelURIs
        + ") " + "&& bif:contains(?label, '\"" + searchString + "*\"')). ");
    if (lang.isPresent()) {
      query.append("FILTER (lang(?label)=\"" + lang.get() + "\") .");
    }
    if (suggestParams.getSearchDataOnly()) {
      query = filterInstances(query);
    }
    query = resolveTerminologies(suggestParams.getTerminologies(), query);
    query.append("} .?o " + StandardVariables.getShortPrefix()
        + ":graph ?graph. ?o omv:acronym ?terminology }}");
    query.append("ORDER BY STRLEN(?label)");
    query.append("OFFSET " + suggestParams.getOffset());
    query.append(" LIMIT " + suggestParams.getLimit());
    return query.toString();
  }

  public static String getSPARQLSearch(List<String> terminology_list, String searchString,
      HashMap<String, String> map, Search searchParams) {
    String extensions = map.get("extensions");
    String labelURIs = map.get("labelURIs");
    String matchType = searchParams.getMatchType();
    if (searchParams.getMatchType().equals("included")) {
      searchString = FormatterUtils.formatSearchString(searchString);
    }
    String searchStringMod = FormatterUtils.delQuotesFromString(searchString);
    Boolean abox = searchParams.getSearchDataOnly();
    StringBuilder query = new StringBuilder();
    query.append("SELECT DISTINCT ?uri, ?terminology, ?graph, ?label" + " FROM <"
        + StandardVariables.getUriprefix() + StandardVariables.getMetadataGraph() + "> "
        + "WHERE {{ quad map virtrdf:DefaultQuadMap " + "{ graph ?graph { ?uri ?labeluri ?label .");
    query = resolveMainMatchType(query, matchType, searchString, searchStringMod, labelURIs);
    if (abox) {
      query = filterInstances(query);
    }
    query = resolveTerminologies(terminology_list, query);
    query.append("}?o " + StandardVariables.getShortPrefix()
        + ":graph ?graph. ?o omv:acronym ?terminology.}}");
    query.append(" UNION ");
    query.append("{quad map virtrdf:DefaultQuadMap " + "{ graph ?graph { ?uri ?p ?ext . ");
    query = resolveExtensionMatchType(query, matchType, searchString, searchStringMod, extensions);
    if (abox) {
      query = filterInstances(query);
    }
    query = resolveTerminologies(terminology_list, query);
    query.append(" ?uri ?labeluri ?label ." + " FILTER (?labeluri = rdfs:label " + labelURIs + ")");
    query.append("} ?o " + StandardVariables.getShortPrefix()
        + ":graph ?graph. ?o omv:acronym ?terminology.}}}");
    if (matchType.equals("included")) {
      query.append("ORDER BY DESC (?sc) ");
    }
    query.append("OFFSET " + searchParams.getOffset());
    query.append(" LIMIT " + searchParams.getLimit());
    return query.toString();
  }

  public static StringBuilder resolveMainMatchType(StringBuilder query, String matchType,
      String search_string, String search_string_mod, String labelURIs) {
    switch (matchType) {
      case "exact":
        query.append("?label bif:contains \'\"" + search_string + "\"\' ."
            + " FILTER(STRLEN(?label) = " + search_string_mod.length() + " && "
            + " ( ?labeluri = rdfs:label || ?labeluri = skos:prefLabel" + labelURIs + " ) ). ");
        break;
      case "included":
        query.append("?label bif:contains \'" + search_string + "\' " + "OPTION (score ?sc) ."
            + "FILTER(?labeluri = rdfs:label || ?labeluri = skos:prefLabel " + labelURIs + " ) . ");
        break;
      case "regex":
        query.append("FILTER ((?labeluri = rdfs:label || ?labeluri = skos:prefLabel" + labelURIs
            + " ) " + "&& regex($label, \'" + search_string_mod + "\')) . ");
        break;
      default:
        throw new BadRequestException("Unknown match type: " + matchType + ". "
            + "Match type should have one of following values: \"exact\",\"included\" or \"regex\".");
    }
    return query;
  }

  public static StringBuilder resolveExtensionMatchType(StringBuilder query, String matchType,
      String searchString, String search_string_mod, String extensions) {
    switch (matchType) {
      case "exact":
        query.append("?ext bif:contains \'\"" + searchString + "\"\' . "
            + "FILTER ( STRLEN(?ext) = " + search_string_mod.trim().length() + " && "
            + " ( ?p = skos:altLabel ||  ?p = <http://planqk.de/ontologies/qco/acronym>"
            + extensions + " ) ). ");
        break;
      case "included":
        query.append("?ext bif:contains \'" + searchString + "\'  " + "OPTION (score ?sc) ."
            + "FILTER ( ?p = skos:altLabel ||  ?p = <http://planqk.de/ontologies/qco/acronym> "
            + extensions + " ) . ");
        break;
      case "regex":
        query.append(
            "FILTER ( (?p = skos:altLabel ||  ?p = <http://planqk.de/ontologies/qco/acronym> "
                + extensions + " ) " + "&& regex($ext, \'" + search_string_mod + "\')) . ");
        break;
      default:
        throw new BadRequestException("Unknown match type: " + matchType + ". "
            + "Match type should have one of following values: \"exact\",\"included\" or \"regex\".");
    }
    return query;
  }

  public static StringBuilder filterInstances(StringBuilder query) {
    return query.append("FILTER(regex(str(?uri), <" + StandardVariables.getPlaqkPrefix()
        + StandardVariables.getABoxPrefix() + ">, \"i\" ))");
  }


  public static StringBuilder resolveTerminologies(List<String> terminology_list,
      StringBuilder query) {
    if (!terminology_list.isEmpty()) {
      String terminologies = "";
      for (String term_items : terminology_list) {
        terminologies += " <" + StandardVariables.getUriprefix() + term_items + "/> ";
      }
      query.append("VALUES ?graph {" + terminologies + "}.");
    }
    return query;
  }


}
