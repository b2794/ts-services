package org.semantics.ts.queries;

import java.util.Optional;
import org.apache.jena.rdf.model.RDFNode;

public class HierarchyQueries {

  /**
   * Parents
   * 
   * @param graph
   * @param label
   * @param term_uri
   * @param lang
   * @param limit
   * @param offset
   * @return
   */
  public static String getSPARQLBroader(RDFNode graph, RDFNode label, String term_uri,
      Optional<String> lang, int limit, int offset, boolean countQuery) {

    StringBuilder query = new StringBuilder();

    if (countQuery == true) {
      query.append("SELECT COUNT(*) AS ?count ");
    } else {
      query.append("SELECT DISTINCT ?uri ?label  ");
    }

    query.append("FROM <" + graph.toString() + "> ")
        .append("WHERE {{SELECT DISTINCT ?uri ?label FROM <" + graph.toString() + "> WHERE {"
            + "?graphuri a  <http://www.w3.org/2002/07/owl#Ontology>." + "<" + term_uri
            + ">  ?h ?uri. ?h rdfs:subPropertyOf* rdfs:subClassOf." + "?uri ?p ?label.")
        .append(" FILTER(?p = rdfs:label || ?p = <" + label + ">) ");

    if (lang.isPresent())
      query.append(" FILTER (lang(?label) = \"" + lang + "\") ");

    query.append("}}").append("UNION")
        .append(" {SELECT DISTINCT ?uri ?label FROM <" + graph.toString() + "> " + "WHERE {<"
            + term_uri + "> rdf:type ?type . ?type rdfs:subClassOf ?uri." + "?uri ?p ?label.")
        .append(" FILTER(?p = rdfs:label || ?p = <" + label + ">) ");

    if (lang.isPresent())
      query.append(" FILTER (lang(?label) = \"" + lang + "\") ");

    query.append("}}}");
    query.append(" LIMIT " + limit);
    query.append(" OFFSET " + offset);

    return query.toString();
  }

  /**
   * Child(ren)
   * 
   * @param graph
   * @param label
   * @param term_uri
   * @param lang
   * @param limit
   * @param offset
   * @return
   */
  public static String getSPARQLNarrower(RDFNode graph, RDFNode label, String term_uri,
      Optional<String> lang, int limit, int offset, boolean countQuery) {

    StringBuilder query = new StringBuilder();

    if (countQuery == true) {
      query.append("SELECT COUNT(*) AS ?count ");
    } else {
      query.append("SELECT DISTINCT ?uri ?label ");
    }

    query.append("FROM <" + graph.toString() + "> ")
        .append("WHERE {{SELECT DISTINCT ?uri ?label FROM <" + graph.toString() + "> WHERE {"
            + "?graphuri a  <http://www.w3.org/2002/07/owl#Ontology>." + "?uri ?h <" + term_uri
            + ">. ?h rdfs:subPropertyOf* rdfs:subClassOf." + "?uri ?p ?label.")
        .append(" FILTER(?p = rdfs:label || ?p = <" + label + ">) ");

    if (lang.isPresent())
      query.append(" FILTER (lang(?label) = \"" + lang + "\") ");

    query.append(" }} UNION ").append(" {SELECT DISTINCT ?uri ?label <" + graph.toString() + "> ")
        .append(" WHERE {<" + term_uri
            + "> rdf:type ?type . ?uri rdfs:subClassOf ?type. ?uri ?p ?label.")
        .append(" FILTER(?p = rdfs:label || ?p = <" + label + ">) ");

    if (lang.isPresent())
      query.append(" FILTER (lang(?label) = \"" + lang + "\")");

    query.append(" }}}");
    query.append(" LIMIT " + limit);
    query.append(" OFFSET " + offset);

    return query.toString();
  }

  public static String getSPARQLLabel(RDFNode graph, RDFNode label, String term_uri, String lang) {
    String query = "";
    if (graph != null) {
      query = "SELECT DISTINCT ?label  FROM <" + graph + "> " + "WHERE {\n" + "<" + term_uri
          + "> ?p ?label. " + "FILTER(?p = rdfs:label || ?p = <" + label + ">)"
          + "FILTER (lang(?label) = \"" + lang + "\")}";
    }
    return query;
  }

  public static String getHierarchyQuerySubClassOf(RDFNode graph) {
    StringBuilder query =
        new StringBuilder("SELECT ?subClassOfProperty").append(" FROM <" + graph + "> ")
            .append(" WHERE {?subClassOfProperty rdfs:subPropertyOf rdfs:subClassOf}");
    return query.toString();
  }

  public static String getSPARQLAllBroader(RDFNode graph, RDFNode labelRDF,
      RDFNode subClassOfProperty, String term_uri, Optional<String> lang, int limit, int offset,
      boolean countQuery) {

    StringBuilder query = new StringBuilder();

    if (countQuery == true) {
      query.append("SELECT COUNT(*) AS ?count ");
    } else {
      query.append("SELECT DISTINCT ?uri ?label ");
    }

    query.append(" FROM <" + graph + "> ")
        .append(" WHERE {{SELECT DISTINCT ?uri ?label FROM <" + graph + "> WHERE {"
            + "?graphuri a  <http://www.w3.org/2002/07/owl#Ontology>." + "<" + term_uri + "> (<"
            + subClassOfProperty + "> | rdfs:subClassOf)+ ?uri." + "?uri ?p ?label.")
        .append(" FILTER(?p = rdfs:label || ?p = <" + labelRDF + ">)");

    if (lang.isPresent())
      query.append("FILTER (lang(?label) = \"" + lang + "\")");

    query.append(" }} UNION ")
        .append("{SELECT DISTINCT ?uri ?label FROM <" + graph + "> WHERE {<" + term_uri
            + "> rdf:type ?type . ?type rdfs:subClassOf+ ?uri."
            + "?uri ?p ?label. FILTER(?p = rdfs:label || ?p = <" + labelRDF + ">)");
    if (lang.isPresent())
      query.append("FILTER (lang(?label) = \"" + lang + "\")");

    query.append("}}}");

    query.append(" LIMIT " + limit);
    query.append(" OFFSET " + offset);

    return query.toString();
  }

  public static String getSPARQLAllNarrower(RDFNode graph, RDFNode labelRDF,
      RDFNode subClassOfProperty, String term_uri, Optional<String> lang, int limit, int offset,
      boolean countQuery) {

    StringBuilder query = new StringBuilder();

    if (countQuery == true) {
      query.append("SELECT COUNT(*) AS ?count ");
    } else {
      query.append("SELECT DISTINCT ?uri ?label ");
    }
    query.append(" FROM <" + graph.toString() + "> ")
        .append(" WHERE {{SELECT DISTINCT ?uri ?label FROM <" + graph + "> " + "WHERE {"
            + "?graphuri a  <http://www.w3.org/2002/07/owl#Ontology>. ?uri (<" + subClassOfProperty
            + "> | rdfs:subClassOf) + <" + term_uri + ">."
            + "?uri ?p ?label. FILTER(?p = rdfs:label || ?p = <" + labelRDF + ">)");
    if (lang.isPresent()) {
      query.append(" FILTER (lang(?label) = \"" + lang.get() + "\")");
    }
    query.append(" }} UNION {SELECT DISTINCT ?uri ?label FROM <" + graph + "> WHERE {<" + term_uri
        + "> rdf:type ?type . ?uri rdfs:subClassOf ?type."
        + "?uri ?p ?label. FILTER(?p = rdfs:label || ?p = <" + labelRDF + ">)");
    if (lang.isPresent()) {
      query.append("FILTER (lang(?label) = \"" + lang.get() + "\") .");
    }
    query.append("}}}");
    query.append(" LIMIT " + limit);
    query.append(" OFFSET " + offset);

    return query.toString();
  }

}
