package org.semantics.ts.queries;

import org.semantics.ts.configs.StandardVariables;

public class SharedQueries {

  public static String getMetadataQuery(String terminology_id) {

    int hash = terminology_id.hashCode();
    return new StringBuilder().append("SELECT DISTINCT ?lang ?graph ?label")
        .append(" FROM <" + StandardVariables.getUriprefix() + StandardVariables.getMetadataGraph()
            + ">")
        .append(" WHERE {<" + StandardVariables.getUriprefix() + hash
            + "> omv:hasOntologyLanguage ?lang.")
        .append(" <" + StandardVariables.getUriprefix() + hash + "> ")
        .append(StandardVariables.getShortPrefix() + ":graph ?graph.")
        .append(" <" + StandardVariables.getUriprefix() + hash + "> ")
        .append(StandardVariables.getShortPrefix() + ":label ?label . }").toString();
  }
}
