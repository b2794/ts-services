package org.semantics.ts.queries;

import java.util.Optional;
import org.apache.jena.rdf.model.RDFNode;
import org.semantics.ts.configs.StandardVariables;
import org.springframework.stereotype.Component;

@Component
public class CoreQueries {

  // @Qualifier("virtuosoGraph")
  // @Autowired
  // private VirtGraph set;

  public static String getAttributeQuery() {

    StringBuilder attrQuery =
        new StringBuilder().append("?attribute = omv:" + StandardVariables.getClassesNumber())
            .append("|| ?attribute = omv:" + StandardVariables.getIndividualnumber())
            .append("|| ?attribute = omv:" + StandardVariables.getPropertynumber())
            .append("|| ?attribute = " + StandardVariables.getShortPrefix() + ":"
                + StandardVariables.getMaximumdepth())
            .append("|| ?attribute = " + StandardVariables.getShortPrefix() + ":"
                + StandardVariables.getMaximumnumberofchildren())
            .append("|| ?attribute = " + StandardVariables.getShortPrefix() + ":"
                + StandardVariables.getAveragenumberofchildren())
            .append("|| ?attribute = " + StandardVariables.getShortPrefix() + ":"
                + StandardVariables.getTwentyfivechildren())
            .append("|| ?attribute = " + StandardVariables.getShortPrefix() + ":"
                + StandardVariables.getSinglechild())
            .append("|| ?attribute = " + StandardVariables.getShortPrefix() + ":"
                + StandardVariables.getClasseswithoutdefinition())
            .append("|| ?attribute = " + StandardVariables.getShortPrefix() + ":"
                + StandardVariables.getNumberofleaves())
            .append("|| ?attribute = " + StandardVariables.getShortPrefix() + ":"
                + StandardVariables.getClasseswithmorethan1parent())
            .append("|| ?attribute = " + StandardVariables.getShortPrefix() + ":"
                + StandardVariables.getClasseswithoutlabel());

    return attrQuery.toString();
  }

  /**
   * 
   * @param filterAcronym
   * @param limit
   * @param offset
   * @return
   */
  public static String getSPARQLTerminologies(Optional<String> filterAcronym, int limit, int offset,
      boolean countQuery) {
    StringBuilder query = new StringBuilder();

    if (countQuery == true)
      query.append("SELECT COUNT(*) AS ?count ");
    else
      query.append("SELECT ?acronym ?uri ?name ?description ?type ?lastArchiveID");

    query
        .append(" FROM <" + StandardVariables.getUriprefix() + StandardVariables.getMetadataGraph()
            + "> ")
        .append("WHERE {?s a omv:Ontology . ?s omv:acronym ?acronym . " + "?s "
            + StandardVariables.getShortPrefix()
            + ":graph ?graph . ?s omv:URI ?uri . ?s omv:name ?name . ?s omv:description ?description . ?s rdf:type ?type . ?s gfbio:lastArchiveID ?lastArchiveID . ");

    if (filterAcronym != null)
      query.append("FILTER(?acronym = '" + filterAcronym.get() + "').");

    query.append("} ");
    query.append("LIMIT " + limit + " OFFSET " + offset);

    return query.toString();
  }

  public static String getSPARQLTerminologyInfo(String terminology_id) {
    StringBuilder query = new StringBuilder();
    int hash = terminology_id.hashCode();
    query.append("SELECT ?attribute ?value ?label ?type")
        .append(" FROM <" + StandardVariables.getUriprefix() + StandardVariables.getMetadataGraph()
            + "> FROM <" + StandardVariables.getMetadaschema()
            + "> FROM <http://omv.ontoware.org/2005/05/ontology> ")
        .append(" WHERE {<" + StandardVariables.getUriprefix() + hash + "> ?attribute ?value. "
            + "FILTER (!?attribute = omv:" + StandardVariables.getClassesNumber()
            + " && !?attribute = omv:" + StandardVariables.getIndividualnumber()
            + " && !?attribute = omv:" + StandardVariables.getPropertynumber()
            + " && !?attribute = omv:URI && !?attribute = " + StandardVariables.getShortPrefix()
            + ":" + StandardVariables.getMaximumdepth() + " && !?attribute = "
            + StandardVariables.getShortPrefix() + ":"
            + StandardVariables.getMaximumnumberofchildren() + " && !?attribute = "
            + StandardVariables.getShortPrefix() + ":"
            + StandardVariables.getAveragenumberofchildren() + " && !?attribute = "
            + StandardVariables.getShortPrefix() + ":" + StandardVariables.getTwentyfivechildren()
            + " && !?attribute = " + StandardVariables.getShortPrefix() + ":"
            + StandardVariables.getSinglechild() + " && !?attribute = "
            + StandardVariables.getShortPrefix() + ":"
            + StandardVariables.getClasseswithoutdefinition() + " && !?attribute = "
            + StandardVariables.getShortPrefix() + ":" + StandardVariables.getNumberofleaves()
            + " && !?attribute = " + StandardVariables.getShortPrefix() + ":"
            + StandardVariables.getClasseswithmorethan1parent() + "&& !?attribute = "
            + StandardVariables.getShortPrefix() + ":" + StandardVariables.getClasseswithoutlabel()
            + " && !?attribute = " + StandardVariables.getShortPrefix() + ":graph"
            + " && !?attribute = " + StandardVariables.getShortPrefix() + ":label"
            + " && !?attribute = " + StandardVariables.getShortPrefix() + ":definition"
            + " && !?attribute = " + StandardVariables.getShortPrefix() + ":synonym"
            + " && !?attribute = omv:resourceLocator && !?attribute = rdf:type)" + " . ")
        .append(" OPTIONAL{?attribute rdfs:label ?label} . }");
    return query.toString();
  }

  public static String getSPARQLTerm(String terminology_id, String term_uri) {
    StringBuilder query = new StringBuilder();
    terminology_id = StandardVariables.getUriprefix() + terminology_id;
    query.append("SELECT ?attribute ?value ?label ").append("FROM <" + terminology_id + "> ")
        .append("FROM <" + StandardVariables.getUriprefix() + "> ")
        .append(" FROM <" + StandardVariables.getUriprefix() + StandardVariables.getMetadataGraph()
            + "> ")
        .append(" FROM <" + StandardVariables.getMetadaschema() + "> ")
        .append(" FROM <" + StandardVariables.getTsschema() + "> ")
        .append(" FROM <http://omv.ontoware.org/2005/05/ontology> ")
        .append("WHERE {{<" + term_uri + "> ?attribute ?value}. ")
        .append("FILTER ( ! isBLANK(?value)) . OPTIONAL {?attribute rdfs:label ?label}}");
    return query.toString();
  }

  public static String getSPARQLMetadataSynonymSpecified(String terminology_id) {
    int hash = terminology_id.hashCode();
    String synonymMetadataQuery = "SELECT  ?graph ?synonym FROM <"
        + StandardVariables.getUriprefix() + StandardVariables.getMetadataGraph() + ">" + "WHERE {<"
        + StandardVariables.getUriprefix() + hash + "> " + StandardVariables.getShortPrefix()
        + ":graph ?graph ." + "<" + StandardVariables.getUriprefix() + hash + "> "
        + StandardVariables.getShortPrefix() + ":synonym ?synonym}";
    return synonymMetadataQuery;
  }

  public static String getSPARQLMetadataSynonymUnspecified() {
    return "SELECT DISTINCT ?synonym FROM <" + StandardVariables.getUriprefix()
        + StandardVariables.getMetadataGraph() + ">" + "WHERE { ?graph "
        + StandardVariables.getShortPrefix() + ":synonym ?synonym .}";
  }

  public static String getSPARQLMetadataAbbreviationUnspecified() {
    return "SELECT DISTINCT ?abbreviation FROM <" + StandardVariables.getUriprefix()
        + StandardVariables.getMetadataGraph() + ">" + "WHERE {?graph "
        + StandardVariables.getShortPrefix() + ":abbreviation ?abbreviation}";
  }

  public static String getQueryLabelsUnspecified() {
    return "SELECT DISTINCT ?label FROM <" + StandardVariables.getUriprefix()
        + StandardVariables.getMetadataGraph() + ">" + "WHERE { ?graph "
        + StandardVariables.getShortPrefix() + ":label ?label}";
  }

  public static String getSPARQLSynonym(String term_uri, String graph, String synonym,
      Optional<String> language) {
    StringBuilder query = new StringBuilder("");
    query.append("SELECT ?synonym FROM <" + graph + "> " + "WHERE {<" + term_uri
        + "> ?p ?synonym. FILTER(?p = <" + synonym + ">" + "|| ?p = skos:altLabel) .");
    if (language.isPresent()) {
      query.append("FILTER (lang(?synonym) = \"" + language.get() + "\") .");
    }
    query.append("}");
    return query.toString();
  }

  public static String getSPARQLAbbreviation(String term_uri, String graph, String acronymProperty,
      Optional<String> language) {
    StringBuilder query = new StringBuilder("");
    query.append("SELECT ?acronym FROM <" + graph + "> WHERE {<" + term_uri + "> ?p ?acronym . "
        + "FILTER(?p = <" + acronymProperty + "> ) .");
    if (language.isPresent()) {
      query.append("FILTER (lang(?acronym) = \"" + language.get() + "\") .");
    }
    query.append("}");
    return query.toString();
  }

  public static String getSPARQLMetrics(String terminology_id) {
    StringBuilder query = new StringBuilder();
    int hash = terminology_id.hashCode();
    query.append("SELECT ?attribute ?value ?label")
        .append(" FROM <" + StandardVariables.getUriprefix() + StandardVariables.getMetadataGraph()
            + "> FROM <" + StandardVariables.getMetadaschema()
            + "> FROM <http://omv.ontoware.org/2005/05/ontology> ")
        .append(" WHERE {<" + StandardVariables.getUriprefix() + hash + "> ?attribute ?value. ")
        .append("FILTER (").append(getAttributeQuery()).append(").")
        .append(" OPTIONAL{?attribute rdfs:label ?label}}");
    return query.toString();
  }

  public static String getSPARQLMetadata(RDFNode graph) {
    StringBuilder query = new StringBuilder("SELECT ?attribute ?value ?label ")
        .append("FROM <" + graph.toString() + "> ")
        .append(
            "WHERE {{ ?uri a <http://www.w3.org/2002/07/owl#Ontology> } . ?uri ?attribute ?value. ")
        .append("FILTER ( ! isBLANK(?value)) . ").append("OPTIONAL{?attribute rdfs:label ?label}}");
    return query.toString();
  }

  public static String getCheckingTerminologyExistQuery(String terminology_id) {
    String sparql =
        "SELECT * FROM <" + StandardVariables.getUriprefix() + StandardVariables.getMetadataGraph()
            + "> {?s omv:acronym ?o . ?o bif:contains '" + terminology_id + "'}";
    return sparql;
  }

  public static String getSPARQLAllTerms(RDFNode graph, String label, Optional<String> lang,
      int limit, int offset, boolean countQuery) {
    StringBuilder query = new StringBuilder();

    if (graph != null) {
      if (countQuery == true)
        query.append("SELECT COUNT(*) AS ?count {");
      else
        query.append("SELECT DISTINCT ?uri ?label {");

      if (label.equals("")) {
        label = "rdfs:label";
      }
      query.append("{SELECT ?uri ?label FROM <" + graph + "> ")
          .append("WHERE { ?uri a owl:Class . ?uri " + label + " ?label .");

      if (lang.isPresent())
        query.append("FILTER (lang(?label) = \"" + lang + "\")");

      query.append("}}");
      query.append(" UNION ").append("{SELECT ?uri ?label FROM <" + graph + "> "
          + "WHERE { ?uri a owl:DatatypeProperty . ?uri " + label + " ?label . ");

      if (lang.isPresent())
        query.append(" FILTER (lang(?label) = \"" + lang + "\")");

      query.append("}}");
      query.append(" UNION ").append("{SELECT ?uri ?label FROM <" + graph + "> "
          + "WHERE { ?uri a owl:ObjectProperty . ?uri " + label + " ?label .");

      if (lang.isPresent())
        query.append(" FILTER (lang(?label) = \"" + lang + "\")");

      query.append("}}");
      query.append(" UNION ").append("{SELECT ?ind as ?uri ?label FROM <" + graph + "> "
          + "WHERE { ?uri a owl:Class . ?ind rdf:type ?uri . ?ind " + label + " ?label . ");

      if (lang.isPresent())
        query.append(" FILTER (lang(?label) = \"" + lang + "\")");

      query.append("}}}");
      // TODO remove ORDER BY to increase performance?!
      // query.append(" ORDER BY LCASE (?label)");
      query.append(" LIMIT " + limit + " OFFSET " + offset);
    }

    return query.toString();
  }

  public static String getSPARQLClassesNumber(String terminology_id) {
    int hash = terminology_id.hashCode();
    StringBuilder sb = new StringBuilder();
    sb.append("SELECT ?number FROM <" + StandardVariables.getUriprefix()
        + StandardVariables.getMetadataGraph() + "> WHERE {");
    sb.append("<" + StandardVariables.getUriprefix() + hash + "> omv:"
        + StandardVariables.getClassesNumber() + " ?number}");
    return sb.toString();
  }

  public static String getSPARQLInsertGraphIntoMetadataMap(String graph) {
    return "SELECT ?synonym, ?acronym, ?label FROM <" + StandardVariables.getUriprefix()
        + StandardVariables.getMetadataGraph() + "> " + "WHERE { ?o "
        + StandardVariables.getShortPrefix() + ":graph <" + graph + "> . " + "OPTIONAL{?o "
        + StandardVariables.getShortPrefix() + ":synonym ?synonym} . OPTIONAL{?o "
        + StandardVariables.getShortPrefix() + ":abbreviation ?acronym}}";
  }

  public static String getSPARQLTypes(String uri, String graph) {
    return "SELECT ?type FROM <" + graph + "> WHERE { <" + uri + "> a ?type  ."
        + "FILTER (?type != owl:NamedIndividual " + "&& ?type != owl:Class "
        + "&& ?type != owl:ObjectProperty" + "&& ?type != owl:DatatypeProperty"
        + "&& ?type != owl:AnnotationProperty)}";
  }

  /**
   * Returns the id of the last archived terminology version, if available
   * 
   * @param acronym
   * @return
   */
  public static String getSPARQLTerminologyArchive(String acronym) {

    StringBuilder query = new StringBuilder();

    query.append("SELECT ?label ?value")
        .append(" FROM <" + StandardVariables.getUriprefix() + "Metadata>").append(" WHERE {<"
            + StandardVariables.getUriprefix() + acronym.hashCode() + "> ?label ?value.")
        .append(" FILTER(?label = gfbio:lastArchiveID)}");

    return query.toString();
  }

  /**
   * 
   * @param acronym
   * @param limit
   * @param offset
   * @return
   */
  public static String getSPARQLChangedTerms(String acronym, int limit, int offset,
      boolean countQuery) {

    StringBuilder query = new StringBuilder();

    if (countQuery == true)
      query.append("SELECT COUNT(*) AS ?count ");
    else
      query.append("SELECT ?o ?change ");

    query.append(" FROM <" + StandardVariables.getUriprefix() + acronym + "MOD>");

    query.append(
        " WHERE { ?s ?p ?o. ?o <http://terminologies.gfbio.org/terms/ontology#change> ?change . }")
        .append(" ORDER BY ASC(?change) ").append("LIMIT " + limit + " OFFSET " + offset);

    return query.toString();
  }

  public static String getSPARQLOriginalAttributeLabel(String attributeUri) {
    StringBuilder query = new StringBuilder("SELECT *")
        .append(" WHERE {graph ?graph{<" + attributeUri + "> ?p ?o} . ")
        .append(" FILTER (?p = <http://www.w3.org/2000/01/rdf-schema#label>)}");

    return query.toString();
  }

  public static String getSPARQLTSSchemaLabels() {
    StringBuilder query = new StringBuilder();

    query.append("SELECT * ").append("FROM <" + StandardVariables.getTsschema() + "> ")
        .append("FROM <" + StandardVariables.getMetadaschema() + "> ")
        .append("FROM <http://omv.ontoware.org/2005/05/ontology> ")
        .append("WHERE { ?tsAttr rdfs:label ?label }");

    return query.toString();
  }

  public static String getSPARQLAttributeLabel(String acronym, String attributeUri) {
    StringBuilder query = new StringBuilder("SELECT DISTINCT ?tsapiAttribute ?label")
        .append(" FROM <" + StandardVariables.getUriprefix() + StandardVariables.getMetadataGraph()
            + "> ")
        .append(" FROM <" + StandardVariables.getMetadaschema() + "> ")
        .append(" FROM <" + StandardVariables.getTsschema() + "> ")
        .append(" WHERE { ?graph gfbio:graph <" + StandardVariables.getUriprefix() + acronym
            + "> . ?graph ?attribute <" + attributeUri
            + "> . ?attribute rdfs:subPropertyOf ?tsapiAttribute . ?tsapiAttribute rdfs:label ?label }");

    return query.toString();
  }

  public static String getSPARQLArchivedVersion(String acronym, int archiveId) {

    StringBuilder baseQuery = new StringBuilder().append("SELECT ?attribute ?value ?label")
        .append(" FROM <" + StandardVariables.getUriprefix() + "Metadata>")
        .append(" FROM <" + StandardVariables.getMetadaschema()
            + "> FROM <http://omv.ontoware.org/2005/05/ontology> ")
        .append(" WHERE {")
        .append("<" + StandardVariables.getUriprefix() + acronym.hashCode() + "/" + archiveId
            + "> ?attribute ?value.")
        .append(" FILTER(").append(getAttributeQuery())
        .append(" || ?attribute = omv:resourceLocator")
        .append(" || ?attribute = " + StandardVariables.getShortPrefix() + ":releaseDate")
        .append(" || ?attribute = " + StandardVariables.getShortPrefix() + ":archivingDate")
        .append(" || ?attribute = " + StandardVariables.getShortPrefix() + ":archiveID")
        .append(").").append(" OPTIONAL{?attribute rdfs:label ?label}.}");

    return baseQuery.toString();
  }
}
