package org.semantics.ts.queries;

import org.semantics.ts.configs.StandardVariables;

public class TopicQueries {

    public static String countTriplesQuery(String graph) {
        return "SELECT COUNT(*) as ?triples " +
                "FROM <" + graph + "> " +
                "WHERE { ?s ?p ?o } ";
    }

    public static String getDeleteTriplesFromSubject(String uri, String graph) {
        return "WITH <" + graph + "> " +
                "DELETE {<" + uri + "> ?p ?o . " +
                "?s ?p <" + uri + "> . " +
                "?s <" + uri + "> ?o }" +
                " WHERE { {<" + uri + "> ?p ?o} UNION" +
                "{?s ?p <" + uri + "> } UNION" +
                "{?s <" + uri + "> ?o }}";
    }

    /*TO-DO refactoring with array of relations uri and String builder */
    public static String getDeleteAlgorithmRelations(String uri, String graph) {
        return "WITH <" + graph + "> " +
                "DELETE " +
                "{<" + uri + ">" + "<http://planqk.de/ontologies/qco/usesAlgorithm> ?o ." +
                " ?s <http://planqk.de/ontologies/qco/usesAlgorithm> <" + uri + "> ." +
                "<" + uri + ">  <http://planqk.de/ontologies/qco/isParentOf> ?o ." +
                "?s <http://planqk.de/ontologies/qco/isParentOf> <" + uri + "> ." +
                "<" + uri + "> <http://planqk.de/ontologies/qco/isSpecificTypeOf> ?o ." +
                "?s <http://planqk.de/ontologies/qco/isSpecificTypeOf> <" + uri + "> ." +
                " } " +
                "WHERE { {<" + uri + "> <http://planqk.de/ontologies/qco/usesAlgorithm> ?o}" +
                "UNION{?s <http://planqk.de/ontologies/qco/usesAlgorithm> <" + uri + "> }" +
                "UNION {<" + uri + "> <http://planqk.de/ontologies/qco/isParentOf> ?o} " +
                "UNION{?s <http://planqk.de/ontologies/qco/isParentOf> <" + uri + "> }" +
                "UNION {<" + uri + "> <http://planqk.de/ontologies/qco/isSpecificTypeOf> ?o}" +
                "UNION{?s <http://planqk.de/ontologies/qco/isSpecificTypeOf> <" + uri + "> }}";
    }


    public static String insertTriples(String rdf, String graph) {
        return "INSERT DATA" +
                "  { GRAPH <" + graph + ">" +
                "  { " + rdf + "}}";
    }
}
