package org.semantics.ts.dao;

import java.util.concurrent.TimeUnit;
import org.semantics.ts.configs.StandardVariables;
import org.semantics.ts.utils.TSCache;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TSCacheBean {

  @Bean
  public TSCache termCache() {
    return new TSCache(StandardVariables.getTscacheduration(), TimeUnit.SECONDS);
  }

  @Bean
  public TSCache terminologyCache() {
    return new TSCache(StandardVariables.getTscacheduration(), TimeUnit.SECONDS);
  }
}
