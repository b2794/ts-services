package org.semantics.ts.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.InternalServerErrorException;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.semantics.ts.configs.StandardVariables;
import org.semantics.ts.model.core.services.QueryExtensionBasicHelper;
import org.semantics.ts.model.core.services.Search;
import org.semantics.ts.model.core.services.SearchBasic;
import org.semantics.ts.model.core.term.TermDataAdvanced;
import org.semantics.ts.model.core.term.TermDataSuggest;
import org.semantics.ts.model.database.Virtuoso;
import org.semantics.ts.queries.CoreQueries;
import org.semantics.ts.queries.SearchQueries;
import org.semantics.ts.utils.FormatterUtils;
import org.semantics.ts.utils.SearchUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

@Repository("search")
public class SearchDataAccessService implements SearchDao {
  private static final Logger LOGGER = LoggerFactory.getLogger(SearchDataAccessService.class);
  private static final GsonBuilder builder = new GsonBuilder().serializeNulls();
  private static final Gson gson = builder.setPrettyPrinting().create();
  @Qualifier("virtuosoGraph")
  @Autowired
  VirtGraph set;

  @Qualifier("virtuosoInstance")
  @Autowired
  Virtuoso virtuoso;

  private ConcurrentHashMap<String, HashMap<String, String>> terminologiesMetadataMap =
      new ConcurrentHashMap<>();

  @Override
  public List<TermDataAdvanced> getSearch(Search searchParams) {
    List<TermDataAdvanced> searchResults = new ArrayList<>();
    String searchTerm = SearchUtils.checkSearchTerm(searchParams.getSearchTerm());
    if (!searchTerm.equals("")) {
      List<String> terminologies = searchParams.getTerminologies();
      if (!SearchUtils.checkParameters(terminologies)) {
        throw new BadRequestException("List of terminologies in parameters is not valid!");
      }
      SearchUtils.logSearchRequest(searchParams);
      try {
        ResultSet rs = executeSearchQuery(terminologies, searchTerm, searchParams);
        List<JsonObject> results =
            getSearchResults(rs, searchParams.getTerminologies(), searchParams.getLanguage(), true);
        for (JsonObject builder : results) {
          TermDataAdvanced resultMap = gson.fromJson(builder, TermDataAdvanced.class);
          searchResults.add(resultMap);
        }
        LOGGER.info("Search execution finished, results added = " + searchResults.size());
      } catch (InternalServerErrorException e) {
        set.close();
      }
    }
    return searchResults;
  }

  @Override
  public QueryExtensionBasicHelper getSearchQueryExpansion(SearchBasic searchParams,
      Boolean forNarrowers) {
    JsonArray synonyms = new JsonArray();
    JsonArray acronyms = new JsonArray();
    JsonObject builder = new JsonObject();
    QueryExtensionBasicHelper searchResults = new QueryExtensionBasicHelper();
    Search searchParams_ = SearchUtils.constructSearchParams(searchParams);
    String searchTerm = SearchUtils.checkSearchTerm(searchParams.getSearchTerm());
    if (!searchTerm.equals("")) {
      List<String> terminologies = searchParams.getTerminologies();
      if (!SearchUtils.checkParameters(terminologies)) {
        throw new BadRequestException("List of terminologies in parameters is not valid!");
      }
      SearchUtils.logSearchRequest(searchParams_);
      try {
        ResultSet rs = executeSearchQuery(terminologies, searchTerm, searchParams_);
        List<String> uris = new ArrayList<>();
        while (rs.hasNext()) {
          QuerySolution result = rs.next();
          String graph = result.get("graph").toString();
          checkMetadata(graph);
          String uri = FormatterUtils.formatField(result, "uri");
          if (!uris.contains(uri)) {
            for (JsonElement synonym : getSemanticTypeValues("synonym", graph, uri,
                searchParams.getLanguage())) {
              synonyms.add(synonym);
            }
            for (JsonElement acronym : getSemanticTypeValues("acronym", graph, uri,
                searchParams.getLanguage())) {
              acronyms.add(acronym);
            }
          }
          uris.add(uri);
        }
        builder.addProperty("prefLabel", searchParams.getSearchTerm());
        builder.add("synonyms", synonyms);
        builder.add("acronyms", acronyms);
        if (!forNarrowers) {
          builder.add("uris", FormatterUtils.convertListOfStringsToJsonArray(uris));
        }
        searchResults = gson.fromJson(builder, QueryExtensionBasicHelper.class);
        LOGGER
            .info("Search query extension execution finished " + searchResults.getSynonyms().size()
                + " synonyms and " + searchResults.getAcronyms().size() + " acronyms are added.");
      } catch (InternalServerErrorException e) {
        set.close();
      }
    }
    return searchResults;
  }

  @Override
  public List<TermDataSuggest> getSuggest(SearchBasic suggestParams) {
    List<TermDataSuggest> resultList = new ArrayList<>();
    String searchTerm = suggestParams.getSearchTerm().trim();
    if (SearchUtils.checkIfSearchTermIsLongEnough(searchTerm)) {
      searchTerm = "\"" + searchTerm + "\"";
      try {
        HashMap<String, String> map = getSearchMetadata();
        String sparql = SearchQueries.getSPARQLSuggestSearch(suggestParams, searchTerm, map);
        VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
        ResultSet rs = executeSPARQL(vqe, sparql);
        for (JsonObject builder : getSearchResults(rs, suggestParams.getTerminologies(),
            suggestParams.getLanguage(), false)) {
          TermDataSuggest resultMap = gson.fromJson(builder, TermDataSuggest.class);
          resultList.add(resultMap);
        }
        LOGGER.info("Suggestion execution finished, results added = " + resultList.size());
        vqe.close();
      } catch (InternalServerErrorException e) {
        set.close();
      }
      return resultList;
    }
    return null;
  }

  public void checkMetadata(String graph) {
    if (!terminologiesMetadataMap.containsKey(graph)) {
      insertGraphIntoMetadataMap(graph);
    }
  }

  private List<JsonObject> getSearchResults(ResultSet rs, List<String> terminologies,
      Optional<String> language, Boolean search) {
    List<JsonObject> builders = new ArrayList<>();
    List<String> uris = new ArrayList<>();
    while (rs.hasNext()) {
      QuerySolution result = rs.next();
      String uri = FormatterUtils.formatField(result, "uri");
      if (!uris.contains(uri)) {
        uris.add(uri);
        String sourceTerminology = FormatterUtils.formatField(result, "terminology");
        JsonObject builder = new JsonObject();
        if (search) {
          builder = getSemanticTermInfos(builder, result.get("graph").toString(), uri, language);
        }
        builder.addProperty("uri", uri.replace(StandardVariables.getPlaqkInstancesPrefix(), ""));
        builder.addProperty("prefLabel", FormatterUtils.formatField(result, "label"));
        String query = CoreQueries.getSPARQLTypes(uri,
            StandardVariables.getUriprefix() + sourceTerminology + "/");
        builder.add("types", formatJsonArrayField(query, "type"));
        builder.addProperty("sourceTerminology", sourceTerminology);
        builders.add(builder);
      }
    }
    return builders;
  }

  private JsonArray formatJsonArrayField(String query, String field) {
    VirtuosoQueryExecution vqe_ = VirtuosoQueryExecutionFactory.create(query, set);
    ResultSet result = executeSPARQL(vqe_, query);
    JsonArray values = new JsonArray();
    while (result.hasNext()) {
      QuerySolution value = result.next();
      values = FormatterUtils.processArrayFields(values, FormatterUtils.formatField(value, field));
    }
    vqe_.close();
    return values;
  }

  private ResultSet executeSearchQuery(List<String> terminologies, String searchTerm,
      Search searchParams) {
    HashMap<String, String> map = getSearchMetadata();
    String sparql = SearchQueries.getSPARQLSearch(terminologies, searchTerm, map, searchParams);
    VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
    return executeSPARQL(vqe, sparql);
  }

  private JsonObject getSemanticTermInfos(JsonObject builder, String graph, String uri,
      Optional<String> language) {
    checkMetadata(graph);
    HashMap<String, String> terminology_metadata = terminologiesMetadataMap.get(graph);
    if (!terminology_metadata.isEmpty()) {
      builder = getSemanticTermInfo(terminology_metadata, "synonym", builder, graph, uri, language);
      builder = getSemanticTermInfo(terminology_metadata, "acronym", builder, graph, uri, language);
    }
    return builder;
  }

  private JsonObject getSemanticTermInfo(HashMap<String, String> terminology_metadata,
      String semanticType, JsonObject builder, String graph, String uri,
      Optional<String> language) {
    if (terminology_metadata.get(semanticType) != null) {
      JsonArray semanticTypeValues = getSemanticTypeValues(semanticType, graph, uri, language);
      builder.add(semanticType, semanticTypeValues);
    }
    return builder;
  }


  private JsonArray getSemanticTypeValues(String semanticType, String graph, String uri,
      Optional<String> language) {
    String query;
    String semanticTypeProperty = terminologiesMetadataMap.get(graph).get(semanticType).trim();
    if (semanticType == "synonym") {
      query = CoreQueries.getSPARQLSynonym(uri, graph, semanticTypeProperty, language);
    } else {
      query = CoreQueries.getSPARQLAbbreviation(uri, graph, semanticTypeProperty, language);
    }
    JsonArray semanticTypeValues = formatJsonArrayField(query, semanticType);
    return semanticTypeValues;
  }

  private HashMap<String, String> getSearchMetadata() {
    HashMap<String, String> map = new HashMap<>();
    StringBuilder extensions = new StringBuilder();
    StringBuilder labelURIs = new StringBuilder();
    try {
      extensions = processSynonyms(extensions);
      extensions = processAcronyms(extensions);
      labelURIs = processLabelsUri(labelURIs);

    } catch (InternalServerErrorException e) {
      set.close();
    }
    map.put("extensions", extensions.toString());
    map.put("labelURIs", labelURIs.toString());
    return map;
  }

  private StringBuilder processSynonyms(StringBuilder extensions) {
    String metadata_query_synonyms = CoreQueries.getSPARQLMetadataSynonymUnspecified();
    VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(metadata_query_synonyms, set);
    ResultSet res = executeSPARQL(vqe, metadata_query_synonyms);
    while (res.hasNext()) {
      QuerySolution result = res.nextSolution();
      if (result.get("synonym") != null && !result.get("synonym").toString()
          .equals("http://www.w3.org/2004/02/skos/core#altLabel")) {
        extensions.append(" || ");
        extensions.append("?p = ");
        extensions.append("<" + result.get("synonym") + ">");
      }
    }
    vqe.close();
    return extensions;
  }

  private StringBuilder processAcronyms(StringBuilder extensions) {
    String metadata_query_abbreviations = CoreQueries.getSPARQLMetadataAbbreviationUnspecified();
    VirtuosoQueryExecution vqe =
        VirtuosoQueryExecutionFactory.create(metadata_query_abbreviations, set);
    ResultSet res = executeSPARQL(vqe, metadata_query_abbreviations);
    while (res.hasNext()) {
      QuerySolution result = res.nextSolution();
      if (result.get("abbreviation") != null) {
        extensions.append(" || ");
        extensions.append("?p = ");
        extensions.append("<" + result.get("abbreviation") + ">");

      }
    }
    vqe.close();
    return extensions;
  }

  private StringBuilder processLabelsUri(StringBuilder labelURIs) {
    String metadata_query_labels = CoreQueries.getQueryLabelsUnspecified();
    VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(metadata_query_labels, set);
    ResultSet res = executeSPARQL(vqe, metadata_query_labels);
    while (res.hasNext()) {
      QuerySolution result = res.nextSolution();
      if (result.get("label") != null
          && !result.get("label").toString().equals("http://www.w3.org/2000/01/rdf-schema#label")) {
        labelURIs.append(" || ");
        labelURIs.append("?labeluri = ");
        labelURIs.append("<" + result.get("label") + ">");
      }
    }
    vqe.close();
    return labelURIs;
  }


  synchronized private void insertGraphIntoMetadataMap(String graph) {
    LOGGER.info("Inserting " + graph);
    HashMap<String, String> metadata_map = new HashMap<>();
    String query = CoreQueries.getSPARQLInsertGraphIntoMetadataMap(graph);
    VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(query, set);
    ResultSet res = executeSPARQL(vqe, query);
    if (res.hasNext()) {
      QuerySolution metadata = res.next();
      if (metadata.get("synonym") != null) {
        metadata_map.put("synonym", metadata.get("synonym").toString());
      }
      if (metadata.get("acronym") != null) {
        metadata_map.put("acronym", metadata.get("acronym").toString());
      }
    }
    vqe.close();
    terminologiesMetadataMap.put(graph, metadata_map);
  }

  private ResultSet executeSPARQL(VirtuosoQueryExecution vqe, String query) {
    checkIfVirtuosoAvailable();
    ResultSet rs = null;
    LOGGER.info("Executing SPARQL query: " + query);
    try {
      rs = vqe.execSelect();
    } catch (InternalServerErrorException e) {
      set.close();
    }
    return rs;
  }

  private void checkIfVirtuosoAvailable() {
    try {
      if (set == null || !set.getConnection().isValid(10)) {
        set = virtuoso.getVirtGraph();
      }
    } catch (SQLException e) {
      set.close();
      throw new RuntimeException(e.getMessage());

    }
  }
}
