package org.semantics.ts.dao;

import java.io.File;
import java.util.List;
import java.util.Optional;
import org.semantics.ts.model.core.terminology.TerminologyArchive;
import org.semantics.ts.model.core.terminology.TerminologyData;
import org.semantics.ts.model.core.terminology.TerminologyDataExtended;
import org.semantics.ts.model.core.terminology.TerminologyMetadata;
import org.semantics.ts.model.core.terminology.TerminologyMetrics;

public interface TerminologyDao {

  List<TerminologyData> getTerminologies(Optional<Integer> page, Optional<Integer> limit);

  List<TerminologyData> getTerminologyByAcronym(String acronym);


  List<TerminologyMetrics> getTerminologyMetrics(String acronym);

  List<TerminologyMetadata> getTerminologyMetadata(String acronym);

  File getDownload(String acronym);

  List<TerminologyArchive> getTerminologyArchivedVersion(String acronym,
      Optional<Integer> archiveId);

  List<TerminologyDataExtended> getTerminologyLatestVersion(String acronym);

}
