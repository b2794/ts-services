package org.semantics.ts.dao;

import java.util.List;
import java.util.Optional;
import org.semantics.ts.model.core.term.TermDataFull;
import org.semantics.ts.model.core.term.TermSynonyms;

public interface TermDao {

  List<TermDataFull> getAllTerms(String acronym, Optional<String> language, Optional<Integer> page,
      Optional<Integer> limit);

  List<TermDataFull> getTermByUri(String acronym, String uri);

  List<TermSynonyms> getTermSynonymsByUri(String acronym, String uri, Optional<String> language);

  List<TermDataFull> getTerminologyChangelog(String acronym, Optional<Integer> page,
      Optional<Integer> limit);

}
