package org.semantics.ts.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.ws.rs.InternalServerErrorException;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.semantics.ts.errorhandling.EntityNotFoundException;
import org.semantics.ts.model.core.term.TermDataBasic;
import org.semantics.ts.model.database.Virtuoso;
import org.semantics.ts.queries.InstancesQueries;
import org.semantics.ts.utils.FormatterUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

@Repository("instances")
public class InstancesDataAccessService implements InstancesDao {

  private static final Logger LOGGER = LoggerFactory.getLogger(HierarchyDataAccessService.class);

  @Qualifier("virtuosoGraph")
  @Autowired
  VirtGraph set;

  @Qualifier("virtuosoInstance")
  @Autowired
  Virtuoso virtuoso;

  @Autowired
  private CoreSharedServices sharedServices;

  @Override
  public List<TermDataBasic> getDirectInstances(String acronym, String uri, Optional<String> lang) {
    List<TermDataBasic> instances = new ArrayList<>();
    if (sharedServices.isAvailable(acronym)) {
      try {
        String sparql = null;
        try {
          sparql = InstancesQueries.getDirectInstances(acronym, uri, lang);
        } catch (NullPointerException e) {
          LOGGER.error("GET direct instances is available only for PlanQK ontology. " + e);
          set.close();
        }
        if (!sparql.isEmpty()) {
          VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
          ResultSet rs = virtuoso.executeSPARQL(vqe, sparql);
          while (rs.hasNext()) {
            QuerySolution current_result = rs.next();
            JsonObject builder = FormatterUtils.formatTermInfo(current_result);
            TermDataBasic resultMap = new Gson().fromJson(builder, TermDataBasic.class);
            instances.add(resultMap);
          }
          vqe.close();
        }
      } catch (InternalServerErrorException e) {
        set.close();
      }
    } else
      throw new EntityNotFoundException("Terminology", "acronym", acronym);
    return instances;
  }

}
