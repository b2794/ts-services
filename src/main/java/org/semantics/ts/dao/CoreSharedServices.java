package org.semantics.ts.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.annotation.PostConstruct;
import javax.ws.rs.InternalServerErrorException;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Literal;
import org.semantics.ts.configs.StandardVariables;
import org.semantics.ts.model.core.term.TermDataFull;
import org.semantics.ts.model.database.Virtuoso;
import org.semantics.ts.model.hierarchy.BroaderTermBasic;
import org.semantics.ts.model.hierarchy.NarrowerTermBasic;
import org.semantics.ts.queries.CoreQueries;
import org.semantics.ts.utils.FormatterUtils;
import org.semantics.ts.utils.TSCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

/**
 * Utility service class, which implements shared methods
 *
 */
@Service
public class CoreSharedServices {

  private static final Logger LOGGER = LoggerFactory.getLogger(CoreSharedServices.class);

  private ConcurrentHashMap<String, String> mapLabelToAttribute =
      new ConcurrentHashMap<String, String>();

  @Qualifier("virtuosoInstance")
  @Autowired
  private Virtuoso virtuoso;

  @Qualifier("virtuosoGraph")
  @Autowired
  private VirtGraph set;

  @Autowired
  private TSCache terminologyCache;

  /**
   * 
   * @param forLabel
   * @return
   */
  public String getAttribute(String forLabel) {
    return mapLabelToAttribute.get(forLabel);
  }

  /**
   * 
   * @param query
   * @return
   */
  public void cacheCount(String query) {
    Integer cachedCount = terminologyCache.get(query);
    if (cachedCount == null) {

      VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(query, set);
      ResultSet rs = virtuoso.executeSPARQL(vqe, query);

      int count = 0;

      while (rs.hasNext()) {
        QuerySolution currentResult = rs.next();
        count = Integer.valueOf(FormatterUtils.formatField(currentResult, "count"));
      }
      terminologyCache.add(query, count);

      vqe.close();

    }
  }

  public void cacheCount(String query, int count) {
    Integer cachedCount = terminologyCache.get(query);
    if (cachedCount == null)
      terminologyCache.add(query, count);
  }

  /**
   * 
   * @param query
   * @return
   */
  public int retrieveCachedCount(String query, String key) {

    Integer cachedCount = terminologyCache.get(key);
    if (cachedCount != null) {
      return cachedCount;
    }

    VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(query, set);
    ResultSet rs = virtuoso.executeSPARQL(vqe, query);

    int count = 0;

    while (rs.hasNext()) {
      QuerySolution currentResult = rs.next();
      count = currentResult.get("count").asLiteral().getInt();
    }
    terminologyCache.add(key, count);

    vqe.close();

    return count;
  }

  /**
   * 
   * @param acronym
   * @param termUri
   * @param termResultSet
   * @return TermDataFull object + @context
   */
  public TermDataFull resolveTermData(String acronym, String termUri, ResultSet termResultSet) {

    Map<String, String> context = new HashMap<String, String>();
    JsonArray synonym = new JsonArray();
    JsonArray subClassOf = new JsonArray();
    JsonArray acronyms = new JsonArray();
    JsonArray types = new JsonArray();

    JsonObject builder = new JsonObject();

    // this builds one term entry
    if (termResultSet.hasNext()) {
      while (termResultSet.hasNext()) {
        QuerySolution currentResult = termResultSet.next();
        String label = FormatterUtils.formatField(currentResult, "label");
        String value = FormatterUtils.formatField(currentResult, "value");
        String attribute = FormatterUtils.formatField(currentResult, "attribute");

        if (label == null) {

          if (mapLabelToAttribute
              .contains(currentResult.get("attribute").asResource().getLocalName()) == false)
            resolveContext(acronym, attribute);

          // try to resolve label from cached attribute labels
          label = currentResult.get("attribute").asResource().getLocalName();
        }

        if (label != null) {
          if (label.contains("synonym")) {
            synonym = FormatterUtils.processArrayFields(synonym, value);
            builder.add("synonym", synonym);
            // FIXME synonym or synonyms?
            label = "synonym";

          } else if (label.equals("subClassOf")) {
            subClassOf = FormatterUtils.processArrayFields(subClassOf, value);
            builder.add("subClassOf", subClassOf);

          } else if (label.equals("acronym")) {
            acronyms = FormatterUtils.processArrayFields(acronyms, value);
            builder.add("acronym", acronyms);

          } else if (label.equals("type")) {
            types = FormatterUtils.processArrayFields(types, value);
            builder.add("type", types);

          } else if (label.contains("definition")) {
            builder.addProperty("definition", value);

          } else if (label.equalsIgnoreCase("label")) {
            builder.addProperty("label", value);
          }

          // build @context
          if (builder.keySet().contains(label) == true)
            context.put(label, mapLabelToAttribute.get(label));
        }
      }
      builder.addProperty("uri", termUri);
      builder.addProperty("sourceTerminology", acronym);
      // add both schema uris to context
      context.put("uri", mapLabelToAttribute.get("uri"));
      context.put("sourceTerminology", mapLabelToAttribute.get("sourceTerminology"));

      builder.add("context", new Gson().toJsonTree(context));
    }

    TermDataFull resultMap = new Gson().fromJson(builder, TermDataFull.class);
    resultMap.setLinks(FormatterUtils.buildTermLinks(termUri, acronym));

    return resultMap;
  }


  /**
   * 
   * @param acronym Terminology acronym, e.g, ENVO
   * @param hierarchyResultSet ResultSet containing at least URIs of terms to resolve
   * @param resultList Pointer to List holding the results
   * @return
   */
  public List<TermDataFull> resolveTermHierarchyData(String acronym, ResultSet hierarchyResultSet,
      List<TermDataFull> resultList) {

    String termQuery = null;

    // this iterates the found hierarchy, e.g., narrow terms
    while (hierarchyResultSet.hasNext()) {
      try {

        // this extracts results from first query (find all parents, children, etc.)
        QuerySolution current_result = hierarchyResultSet.next();

        // use the term uri of the current result to extract full term info
        termQuery = CoreQueries.getSPARQLTerm(acronym, current_result.get("uri").toString());
        VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(termQuery, set);

        ResultSet termRes = vqe.execSelect();
        resultList.add(resolveTermData(acronym, current_result.get("uri").toString(), termRes));

        vqe.close();
      } catch (Exception e) {
        LOGGER.info(
            "Exception while resolving term hierarchy - following query was issued: " + termQuery);
      }
    }
    return resultList;

  }

  /**
   * 
   */
  @PostConstruct
  public void init() {

    VirtuosoQueryExecution vqe =
        VirtuosoQueryExecutionFactory.create(CoreQueries.getSPARQLTSSchemaLabels(), set);
    ResultSet labelResult = vqe.execSelect();

    String label = "", labelUri = "";

    while (labelResult.hasNext()) {
      QuerySolution labelSol = labelResult.next();
      label = labelSol.get("label").as(Literal.class).getLexicalForm().toString();
      labelUri = labelSol.get("tsAttr").toString();
      mapLabelToAttribute.put(label, labelUri);
    }

    vqe.close();
  }

  /**
   * 
   * @param acronym
   * @param attributeUri
   */
  public void resolveContext(String acronym, String attributeUri) {

    VirtuosoQueryExecution vqe = null;
    ResultSet labelResult = null;
    String label = "", labelUri = "";

    vqe = VirtuosoQueryExecutionFactory
        .create(CoreQueries.getSPARQLOriginalAttributeLabel(attributeUri), set);

    labelResult = vqe.execSelect();
    label = attributeUri;

    while (labelResult.hasNext()) {
      label = labelResult.nextSolution().get("o").as(Literal.class).getLexicalForm().toString();
      if (mapLabelToAttribute.get(label) == null)
        mapLabelToAttribute.put(label, attributeUri);
    }


    vqe.close();

    ///
    ///

    vqe = VirtuosoQueryExecutionFactory
        .create(CoreQueries.getSPARQLAttributeLabel(acronym, attributeUri), set);

    labelResult = vqe.execSelect();

    while (labelResult.hasNext()) {
      QuerySolution labelSol = labelResult.next();
      label = labelSol.get("label").as(Literal.class).getLexicalForm().toString();
      labelUri = labelSol.get("tsapiAttribute").toString();
      if (mapLabelToAttribute.get(label) == null)
        mapLabelToAttribute.put(label, labelUri);
    }

    vqe.close();
  }

  /**
   * 
   * @param rs
   * @param allnarrower
   * @return
   */
  @Deprecated
  public List<NarrowerTermBasic> resolveTermDataNarrower(ResultSet rs,
      List<NarrowerTermBasic> allnarrower) {
    while (rs.hasNext()) {
      QuerySolution current_result = rs.next();
      JsonObject builder = FormatterUtils.formatTermInfo(current_result);
      NarrowerTermBasic resultMap = new Gson().fromJson(builder, NarrowerTermBasic.class);
      allnarrower.add(resultMap);
    }
    return allnarrower;
  }

  /**
   * 
   * @param rs
   * @param allBroader
   * @return
   */
  @Deprecated
  public List<BroaderTermBasic> resolveTermDataBroader(ResultSet rs,
      List<BroaderTermBasic> allBroader) {
    while (rs.hasNext()) {
      QuerySolution current_result = rs.next();
      JsonObject builder = FormatterUtils.formatTermInfo(current_result);
      BroaderTermBasic resultMap = new Gson().fromJson(builder, BroaderTermBasic.class);
      allBroader.add(resultMap);
    }
    return allBroader;
  }

  /**
   * Check the connection with Virtuoso and close it an SQLException is raised
   */
  public void checkIfVirtuosoAvailable() {
    try {
      if (set == null || !set.getConnection().isValid(10)) {
        set = virtuoso.getVirtGraph();
      }
    } catch (SQLException e) {
      set.close();
      throw new RuntimeException(e.getMessage());

    }
  }

  /**
   * @param acronym - acronym of the ontology that should be checked on the availability
   * @return true if the terminology is available
   */
  public boolean isAvailable(String acronym) {

    checkIfVirtuosoAvailable();

    try {
      VirtuosoQueryExecution vqe =
          new VirtuosoQueryExecution(CoreQueries.getCheckingTerminologyExistQuery(acronym), set);
      // ResultSet rs =
      // virtuoso.executeSPARQL(vqe, CoreQueries.getCheckingTerminologyExistQuery(acronym));
      ResultSet rs = vqe.execSelect();
      if (rs.hasNext()) {
        return true;
      }
    } catch (InternalServerErrorException e) {
      set.close();
    }
    return false;
  }

  /**
   * Check the number of terms a terminology has
   *
   * @param acronym - acronym of the ontology that si queried
   * @return true if the number of terms is smaller than specified limit
   *         (StandardVariables.limitAllTermsService)
   */
  public boolean isSmallerThanLimit(String acronym) {
    String sparql = CoreQueries.getSPARQLClassesNumber(acronym);
    try {
      VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
      ResultSet res = virtuoso.executeSPARQL(vqe, sparql);
      int classesCount = Integer.parseInt(res.next().get("number").toString());
      vqe.close();
      if (classesCount > StandardVariables.getLimitAllTermsService()) {
        return false;
      }
    } catch (InternalServerErrorException e) {
      set.close();
    }
    return true;
  }

}
