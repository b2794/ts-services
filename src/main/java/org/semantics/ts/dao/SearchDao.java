package org.semantics.ts.dao;

import org.semantics.ts.model.core.services.QueryExtensionBasic;
import org.semantics.ts.model.core.services.QueryExtensionBasicHelper;
import org.semantics.ts.model.core.services.Search;
import org.semantics.ts.model.core.services.SearchBasic;
import org.semantics.ts.model.core.term.TermDataAdvanced;
import org.semantics.ts.model.core.term.TermDataSuggest;

import java.util.List;

public interface SearchDao {
    List<TermDataAdvanced> getSearch(Search searchParams);

    QueryExtensionBasicHelper getSearchQueryExpansion(SearchBasic searchParams, Boolean forNarrowers);

    List<TermDataSuggest> getSuggest(SearchBasic suggestParams);
}
