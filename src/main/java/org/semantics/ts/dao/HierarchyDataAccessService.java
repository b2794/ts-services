package org.semantics.ts.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.ws.rs.InternalServerErrorException;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.RDFNode;
import org.semantics.ts.configs.StandardVariables;
import org.semantics.ts.errorhandling.EntityNotFoundException;
import org.semantics.ts.model.core.term.TermDataFull;
import org.semantics.ts.model.database.Virtuoso;
import org.semantics.ts.queries.HierarchyQueries;
import org.semantics.ts.queries.SharedQueries;
import org.semantics.ts.utils.FormatterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

@Repository("hierarchy")
public class HierarchyDataAccessService implements HierarchyDao {

  @Qualifier("virtuosoGraph")
  @Autowired
  VirtGraph set;

  @Qualifier("virtuosoInstance")
  @Autowired
  Virtuoso virtuoso;

  @Autowired
  private CoreSharedServices sharedServices;

  @Override
  public List<TermDataFull> getChildren(String acronym, String uri, Optional<String> lang,
      Optional<Integer> page, Optional<Integer> limit) {

    int offset = FormatterUtils.getQueryOffset(page, limit);
    int limit_ = FormatterUtils.getValueFromOptionalOrDefault(limit,
        StandardVariables.getLimitAllTermsService());

    List<TermDataFull> narrower = new ArrayList<>();
    if (sharedServices.isAvailable(acronym)) {
      try {
        ArrayList<RDFNode> graphAndLabel = getGraphAndLabelMetadata(acronym);
        RDFNode graph = graphAndLabel.get(0);
        RDFNode labelRDF = graphAndLabel.get(1);
        if (graph != null) {
          String sparql =
              HierarchyQueries.getSPARQLNarrower(graph, labelRDF, uri, lang, limit_, offset, false);

          // execute COUNT query
          sharedServices.retrieveCachedCount(
              HierarchyQueries.getSPARQLNarrower(graph, labelRDF, uri, lang, limit_, offset, true),
              "getChildren()");

          VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
          ResultSet rs = virtuoso.executeSPARQL(vqe, sparql);
          narrower = sharedServices.resolveTermHierarchyData(acronym, rs, narrower);

          vqe.close();
        }
      } catch (InternalServerErrorException e) {
        set.close();
      }
    } else
      throw new EntityNotFoundException("Terminology", "acronym", acronym);
    return narrower;
  }

  @Override
  public List<TermDataFull> getDescendants(String acronym, String uri, Optional<String> lang,
      Optional<Integer> page, Optional<Integer> limit) {

    List<TermDataFull> allNarrower = new ArrayList<>();

    int offset = FormatterUtils.getQueryOffset(page, limit);
    int limit_ = FormatterUtils.getValueFromOptionalOrDefault(limit,
        StandardVariables.getLimitAllTermsService());

    if (sharedServices.isAvailable(acronym)) {
      try {
        ArrayList<RDFNode> graphAndLabel = getGraphAndLabelMetadata(acronym);
        RDFNode graph = graphAndLabel.get(0);
        RDFNode labelRDF = graphAndLabel.get(1);
        if (graph != null) {
          String hierarchyQuerySubClassOf = HierarchyQueries.getHierarchyQuerySubClassOf(graph);
          VirtuosoQueryExecution vqe =
              VirtuosoQueryExecutionFactory.create(hierarchyQuerySubClassOf, set);
          ResultSet res = virtuoso.executeSPARQL(vqe, hierarchyQuerySubClassOf);
          RDFNode subClassOfProperty = null;
          if (res.hasNext()) {
            QuerySolution result = res.nextSolution();
            subClassOfProperty = result.get("subClassOfProperty");

          }
          String sparql = HierarchyQueries.getSPARQLAllNarrower(graph, labelRDF, subClassOfProperty,
              uri, lang, limit_, offset, false);

          // execute COUNT query
          sharedServices.retrieveCachedCount(HierarchyQueries.getSPARQLAllNarrower(graph, labelRDF,
              subClassOfProperty, uri, lang, limit_, offset, true), "getDescendants()");

          vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
          ResultSet rs = virtuoso.executeSPARQL(vqe, sparql);
          allNarrower = sharedServices.resolveTermHierarchyData(acronym, rs, allNarrower);

          vqe.close();
        }
      } catch (InternalServerErrorException e) {
        set.close();
      }
    } else
      throw new EntityNotFoundException("Terminology", "acronym", acronym);
    return allNarrower;
  }

  @Override
  public List<TermDataFull> getParents(String acronym, String uri, Optional<String> lang,
      Optional<Integer> page, Optional<Integer> limit) {

    int offset = FormatterUtils.getQueryOffset(page, limit);
    int limit_ = FormatterUtils.getValueFromOptionalOrDefault(limit,
        StandardVariables.getLimitAllTermsService());

    List<TermDataFull> broader = new ArrayList<>();
    if (sharedServices.isAvailable(acronym)) {
      try {
        ArrayList<RDFNode> graphAndLabel = getGraphAndLabelMetadata(acronym);
        RDFNode graph = graphAndLabel.get(0);
        RDFNode labelRDF = graphAndLabel.get(1);
        if (graph != null) {
          String sparql =
              HierarchyQueries.getSPARQLBroader(graph, labelRDF, uri, lang, limit_, offset, false);

          // execute COUNT query
          sharedServices.retrieveCachedCount(
              HierarchyQueries.getSPARQLBroader(graph, labelRDF, uri, lang, limit_, offset, true),
              "getParents()");

          VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
          ResultSet rs = virtuoso.executeSPARQL(vqe, sparql);
          broader = sharedServices.resolveTermHierarchyData(acronym, rs, broader);

          vqe.close();
        }
      } catch (InternalServerErrorException e) {
        set.close();
      }
    } else
      throw new EntityNotFoundException("Terminology", "acronym", acronym);
    return broader;
  }

  @Override
  public List<TermDataFull> getAncestors(String acronym, String uri, Optional<String> lang,
      Optional<Integer> page, Optional<Integer> limit) {

    int offset = FormatterUtils.getQueryOffset(page, limit);
    int limit_ = FormatterUtils.getValueFromOptionalOrDefault(limit,
        StandardVariables.getLimitAllTermsService());

    List<TermDataFull> allbroader = new ArrayList<>();
    if (sharedServices.isAvailable(acronym)) {
      try {
        ArrayList<RDFNode> graphAndLabel = getGraphAndLabelMetadata(acronym);
        RDFNode graph = graphAndLabel.get(0);
        RDFNode labelRDF = graphAndLabel.get(1);
        if (graph != null) {
          String hierarchyQuerySubClassOf = HierarchyQueries.getHierarchyQuerySubClassOf(graph);
          VirtuosoQueryExecution vqe =
              VirtuosoQueryExecutionFactory.create(hierarchyQuerySubClassOf, set);
          ResultSet res = vqe.execSelect();
          RDFNode subClassOfProperty = null;
          if (res.hasNext()) {
            QuerySolution result = res.nextSolution();
            subClassOfProperty = result.get("subClassOfProperty");
          }
          String sparql = HierarchyQueries.getSPARQLAllBroader(graph, labelRDF, subClassOfProperty,
              uri, lang, limit_, offset, false);

          // execute COUNT query
          sharedServices.retrieveCachedCount(HierarchyQueries.getSPARQLAllBroader(graph, labelRDF,
              subClassOfProperty, uri, lang, limit_, offset, true), "getAncestors()");

          vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
          ResultSet rs = virtuoso.executeSPARQL(vqe, sparql);
          allbroader = sharedServices.resolveTermHierarchyData(acronym, rs, allbroader);

          vqe.close();
        }
      } catch (InternalServerErrorException e) {
        set.close();
      }
    } else
      throw new EntityNotFoundException("Terminology", "acronym", acronym);
    return allbroader;
  }

  private ArrayList<RDFNode> getGraphAndLabelMetadata(String acronym) {
    ArrayList<RDFNode> graphAndLabel = new ArrayList<>();
    String metadataQuery = SharedQueries.getMetadataQuery(acronym);
    VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(metadataQuery, set);
    ResultSet res = virtuoso.executeSPARQL(vqe, metadataQuery);
    while (res.hasNext()) {
      QuerySolution result = res.nextSolution();
      graphAndLabel.add(0, result.get("graph"));
      graphAndLabel.add(1, result.get("label"));
    }
    vqe.close();
    return graphAndLabel;
  }

}
