package org.semantics.ts.dao;

import java.util.List;
import java.util.Optional;
import org.semantics.ts.model.core.term.TermDataFull;

public interface HierarchyDao {

  List<TermDataFull> getChildren(String acronym, String uri, Optional<String> lang,
      Optional<Integer> page, Optional<Integer> limit);

  List<TermDataFull> getDescendants(String acronym, String uri, Optional<String> lang,
      Optional<Integer> page, Optional<Integer> limit);

  List<TermDataFull> getParents(String acronym, String uri, Optional<String> lang,
      Optional<Integer> page, Optional<Integer> limit);

  List<TermDataFull> getAncestors(String acronym, String uri, Optional<String> lang,
      Optional<Integer> page, Optional<Integer> limit);

}
