package org.semantics.ts.dao;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.ws.rs.InternalServerErrorException;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.RDFNode;
import org.semantics.ts.configs.StandardVariables;
import org.semantics.ts.errorhandling.ArchiveNotFoundException;
import org.semantics.ts.errorhandling.EntityNotFoundException;
import org.semantics.ts.model.core.terminology.TerminologyArchive;
import org.semantics.ts.model.core.terminology.TerminologyData;
import org.semantics.ts.model.core.terminology.TerminologyDataExtended;
import org.semantics.ts.model.core.terminology.TerminologyMetadata;
import org.semantics.ts.model.core.terminology.TerminologyMetrics;
import org.semantics.ts.model.database.Virtuoso;
import org.semantics.ts.queries.CoreQueries;
import org.semantics.ts.queries.SharedQueries;
import org.semantics.ts.utils.FormatterUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

@Repository("terminologies")
public class TerminologyDataAccessService implements TerminologyDao {

  private static final Logger LOGGER = LoggerFactory.getLogger(TerminologyDataAccessService.class);

  @Qualifier("virtuosoGraph")
  @Autowired
  private VirtGraph set;

  @Qualifier("virtuosoInstance")
  @Autowired
  private Virtuoso virtuoso;

  @Autowired
  private CoreSharedServices sharedServices;

  /**
   * Get the list of terminologies along with their metadata as specified by the TerminologyData
   * model
   *
   * @return info about available terminologies
   */
  @Override
  public List<TerminologyData> getTerminologies(Optional<Integer> page, Optional<Integer> limit) {

    List<TerminologyData> termInfos = new ArrayList<>();
    Map<String, String> context = new HashMap<String, String>();

    int offset = FormatterUtils.getQueryOffset(page, limit);
    int limit_ = FormatterUtils.getValueFromOptionalOrDefault(limit,
        StandardVariables.getLimitAllTermsService());

    String sparql = CoreQueries.getSPARQLTerminologies(null, limit_, offset, false);
    sharedServices.checkIfVirtuosoAvailable();
    try {
      // (0) execute COUNT query
      sharedServices.retrieveCachedCount(CoreQueries.getSPARQLTerminologies(null,
          StandardVariables.getLimitAllTermsService(), 0, true), "getTerminologies()");

      // (1) query to find all terminologies
      VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
      ResultSet rs = virtuoso.executeSPARQL(vqe, sparql);

      // iterate found terminologies
      while (rs.hasNext()) {
        QuerySolution solution = rs.next();

        JsonObject builder = new JsonObject();
        builder.addProperty("name", solution.get("name").toString());
        builder.addProperty("acronym", solution.get("acronym").toString());
        builder.addProperty("definition", solution.get("description").toString());
        builder.addProperty("uri", solution.get("uri").toString());
        builder.addProperty("type", solution.get("type").asResource().getLocalName());
        builder.addProperty("lastArchiveID", solution.get("lastArchiveID").toString());

        // build @context
        context.put("name", sharedServices.getAttribute("name"));
        context.put("acronym", sharedServices.getAttribute("acronym"));
        context.put("definition", sharedServices.getAttribute("definition"));
        context.put("uri", sharedServices.getAttribute("uri"));
        context.put("type", solution.get("type").toString());
        context.put("lastArchiveID", sharedServices.getAttribute("lastArchiveID"));

        builder.add("context", new Gson().toJsonTree(context));

        // build POJO from JSON
        TerminologyData resultMap = new Gson().fromJson(builder, TerminologyData.class);
        resultMap
            .setLinks(FormatterUtils.buildTerminologyLinks(builder.get("acronym").getAsString()));

        termInfos.add(resultMap);
      }
      vqe.close();
    } catch (InternalServerErrorException e) {
      set.close();
    }
    return termInfos;
  }

  /**
   * Get data about a specific ontology (by acronym).
   *
   * @param acronym - ontology acronym, e.g. PLANQK
   * @return data about the ontology specified by the TerminologyDataExtended model
   */
  @Override
  public List<TerminologyData> getTerminologyByAcronym(String acronym) {

    List<TerminologyData> resultList = new ArrayList<>();
    Map<String, String> context = new HashMap<String, String>();

    if (sharedServices.isAvailable(acronym)) {
      // try {
      // String sparql = CoreQueries.getSPARQLTerminologyInfo(acronym);
      // VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
      // ResultSet rs = virtuoso.executeSPARQL(vqe, sparql);
      // JsonObject builder = buildSimpleJSONObject(rs);
      // vqe.close();
      // TerminologyData resultMap = gson.fromJson(builder, TerminologyData.class);
      // resultMap.setLinks(FormatterUtils.buildTerminologyLinks(acronym));
      // resultList.add(resultMap);
      // } catch (InternalServerErrorException e) {
      // set.close();
      // }
      try {
        String sparql = CoreQueries.getSPARQLTerminologies(Optional.of(acronym), 1, 0, false);
        VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
        ResultSet rs = virtuoso.executeSPARQL(vqe, sparql);

        while (rs.hasNext()) {
          QuerySolution solution = rs.next();

          JsonObject builder = new JsonObject();
          builder.addProperty("name", solution.get("name").toString());
          builder.addProperty("acronym", solution.get("acronym").toString());
          builder.addProperty("definition", solution.get("description").toString());
          builder.addProperty("uri", solution.get("uri").toString());
          builder.addProperty("type", solution.get("type").asResource().getLocalName());
          builder.addProperty("lastArchiveID", solution.get("lastArchiveID").toString());

          // build @context
          context.put("name", sharedServices.getAttribute("name"));
          context.put("acronym", sharedServices.getAttribute("acronym"));
          context.put("definition", sharedServices.getAttribute("definition"));
          context.put("uri", sharedServices.getAttribute("uri"));
          context.put("type", solution.get("type").toString());
          context.put("lastArchiveID", sharedServices.getAttribute("lastArchiveID"));

          builder.add("context", new Gson().toJsonTree(context));

          // build POJO from JSON
          TerminologyData resultMap = new Gson().fromJson(builder, TerminologyData.class);
          resultMap
              .setLinks(FormatterUtils.buildTerminologyLinks(builder.get("acronym").getAsString()));

          resultList.add(resultMap);
        }
        vqe.close();
      } catch (InternalServerErrorException e) {
        set.close();
      }

    } else
      throw new EntityNotFoundException("Terminology", "acronym", acronym);
    return resultList;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<TerminologyMetrics> getTerminologyMetrics(String acronym) {

    Map<String, String> context = new HashMap<String, String>();
    List<TerminologyMetrics> resultList = new ArrayList<>();

    if (sharedServices.isAvailable(acronym)) {
      try {
        String sparql = CoreQueries.getSPARQLMetrics(acronym);
        VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
        ResultSet rs = virtuoso.executeSPARQL(vqe, sparql);
        JsonObject builder = buildSimpleJSONObject(rs);
        TerminologyMetrics resultMap = new Gson().fromJson(builder, TerminologyMetrics.class);
        resultMap.setLinks(FormatterUtils.buildTerminologyLinks(acronym));

        List<String> jsonKeys = new ArrayList<String>();
        builder.entrySet().iterator().forEachRemaining(entry -> jsonKeys.add(entry.getKey()));
        jsonKeys.forEach(k -> context.put(k, sharedServices.getAttribute(k)));

        resultMap.setContext(context);
        resultList.add(resultMap);
        vqe.close();
      } catch (InternalServerErrorException e) {
        set.close();
      }
    } else
      throw new EntityNotFoundException("Terminology", "acronym", acronym);
    return resultList;
  }

  /**
   * Implements the terminology metadata endpoint.
   *
   * @param acronym terminology acronym
   * @return metadata of the terminology as specified by TerminologyMetadata model
   */
  @Override
  public List<TerminologyMetadata> getTerminologyMetadata(String acronym) {
    Map<String, String> context = new HashMap<String, String>();
    List<TerminologyMetadata> resultList = new ArrayList<>();
    if (sharedServices.isAvailable(acronym)) {
      JsonObject builder = new JsonObject();
      JsonArray contributors = new JsonArray();
      JsonArray creators = new JsonArray();
      String metadataQuery = SharedQueries.getMetadataQuery(acronym);
      try {
        VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(metadataQuery, set);
        ResultSet res = virtuoso.executeSPARQL(vqe, metadataQuery);
        RDFNode graph = null;
        while (res.hasNext()) {
          QuerySolution result = res.nextSolution();
          graph = result.get("graph");
        }
        if (graph != null) {
          String sparql = CoreQueries.getSPARQLMetadata(graph);
          vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
          ResultSet rs = virtuoso.executeSPARQL(vqe, sparql);
          while (rs.hasNext()) {
            QuerySolution currentResult = rs.next();
            String label = FormatterUtils.formatField(currentResult, "label");
            String value = FormatterUtils.formatField(currentResult, "value");
            String attribute = FormatterUtils.formatField(currentResult, "attribute");

            // if (attrUriToLabel.get(attribute) == null) {
            // resolveContext(acronym, attribute);
            // }
            //
            // label = attrUriToLabel.get(attribute);
            if (label == null) {

              if (sharedServices.getAttribute(
                  currentResult.get("attribute").asResource().getLocalName()) == null) {
                sharedServices.resolveContext(acronym, attribute);
              }

              // try to resolve label from cached attribute labels
              label = currentResult.get("attribute").asResource().getLocalName();

            }


            if (label != null) {
              if (label.equals("contributor")) {
                contributors = FormatterUtils.processArrayFields(contributors, value);
              } else if (label.equals("creator")) {
                creators = FormatterUtils.processArrayFields(creators, value);
              } else {
                builder.addProperty(label, value);
              }
              context.put(label, attribute);
            }
          }
          builder.add("contributor", contributors);
          builder.add("creator", creators);
          builder.add("context", new Gson().toJsonTree(context));
        }
        vqe.close();
      } catch (InternalServerErrorException e) {
        set.close();
      }
      TerminologyMetadata resultMap = new Gson().fromJson(builder, TerminologyMetadata.class);
      resultMap.setLinks(FormatterUtils.buildTerminologyLinks(acronym));
      resultList.add(resultMap);
    } else
      throw new EntityNotFoundException("Terminology", "acronym", acronym);
    return resultList;
  }

  @Override
  public File getDownload(String acronym) {

    // get list of all directories in this folder
    String[] files = new File(StandardVariables.getOntologiesDir() + acronym).list();
    Arrays.sort(files);
    // select the last one, i.e., the most current one
    String date = files[files.length - 1];

    if (date == null)
      throw new EntityNotFoundException("Terminology", "acronym", acronym);

    File file = new File(StandardVariables.getOntologiesDir() + acronym + File.separator + date
        + File.separator + acronym.toLowerCase() + ".owl");

    LOGGER.info("requested to download " + file.getAbsolutePath());

    return file;
  }

  @Override
  public List<TerminologyArchive> getTerminologyArchivedVersion(String acronym,
      Optional<Integer> archiveId) {

    // Map<String, String> context = new HashMap<String, String>();
    List<TerminologyArchive> resultList = new ArrayList<>();

    if (sharedServices.isAvailable(acronym)) {

      try {

        String sparql = CoreQueries.getSPARQLTerminologyArchive(acronym);
        VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
        ResultSet rs = virtuoso.executeSPARQL(vqe, sparql);

        int lastArchiveID = 0;

        while (rs.hasNext()) {
          QuerySolution result = rs.nextSolution();
          lastArchiveID = Integer.valueOf(FormatterUtils.formatField(result, "value"));
        }
        vqe.close();

        // (0) execute COUNT query
        sharedServices.cacheCount("getTerminologyArchivedVersion()", lastArchiveID);

        if (lastArchiveID > 0) {
          // there are archived versions available

          // StringBuilder baseQuery = new StringBuilder().append("SELECT ?attribute ?value ?label")
          // .append(" FROM <" + StandardVariables.getUriprefix() + "Metadata>")
          // .append(" FROM <" + StandardVariables.getMetadaschema()
          // + "> FROM <http://omv.ontoware.org/2005/05/ontology> ")
          // .append(" WHERE {");

          if (archiveId.isPresent() == true && lastArchiveID >= archiveId.get()) {
            // we look for a specific archived version

            // StringBuilder subquery = new StringBuilder().append(baseQuery)
            // .append("<" + StandardVariables.getUriprefix() + acronym.hashCode() + "/"
            // + archiveId + "> ?attribute ?value.")
            // .append(" FILTER(").append(CoreQueries.ATTRIBUTE_QUERY)
            // .append(" || ?attribute = omv:resourceLocator")
            // .append(" || ?attribute = " + StandardVariables.getShortPrefix() + ":releaseDate")
            // .append(" || ?attribute = " + StandardVariables.getShortPrefix() + ":archivingDate")
            // .append(" || ?attribute = " + StandardVariables.getShortPrefix() + ":archiveID")
            // .append(").").append(" OPTIONAL{?attribute rdfs:label ?label}.}");

            String subQuery = CoreQueries.getSPARQLArchivedVersion(acronym, archiveId.get());

            vqe = VirtuosoQueryExecutionFactory.create(subQuery, set);
            ResultSet sub = virtuoso.executeSPARQL(vqe, subQuery);

            JsonObject builder = buildSimpleJSONObject(sub);
            TerminologyArchive resultMap = new Gson().fromJson(builder, TerminologyArchive.class);
            resultMap.setLinks(FormatterUtils.buildTerminologyLinks(acronym));

            Map<String, String> context = new HashMap<String, String>();
            /*
             * for (Field f : resultMap.getClass().getDeclaredFields()) { if
             * (f.getName().equalsIgnoreCase("released")) context.put("released",
             * sharedServices.getAttribute("releaseDate")); else context.put(f.getName(),
             * sharedServices.getAttribute(f.getName())); }
             * 
             * for (Field f : resultMap.getClass().getSuperclass().getDeclaredFields()) { if
             * (f.getName().equalsIgnoreCase("released")) context.put("released",
             * sharedServices.getAttribute("releaseDate")); else context.put(f.getName(),
             * sharedServices.getAttribute(f.getName())); }
             */
            List<String> jsonKeys = new ArrayList<String>();
            builder.entrySet().iterator().forEachRemaining(entry -> jsonKeys.add(entry.getKey()));
            jsonKeys.forEach(k -> context.put(k, sharedServices.getAttribute(k)));
            resultMap.setContext(context);
            resultList.add(resultMap);

          } else {

            for (int i = lastArchiveID; i > 0; i--) {
              // extract every archived version

              // StringBuilder subquery = new StringBuilder().append(baseQuery)
              // .append("<" + StandardVariables.getUriprefix() + acronym.hashCode() + "/" + i
              // + "> ?attribute ?value.")
              // .append(" FILTER(").append(CoreQueries.ATTRIBUTE_QUERY)
              // .append(" || ?attribute = omv:resourceLocator")
              // .append(" || ?attribute = " + StandardVariables.getShortPrefix() + ":releaseDate")
              // .append(
              // " || ?attribute = " + StandardVariables.getShortPrefix() + ":archivingDate")
              // .append(" || ?attribute = " + StandardVariables.getShortPrefix() + ":archiveID")
              // .append(").").append(" OPTIONAL{?attribute rdfs:label ?label}.}");

              String subQuery = CoreQueries.getSPARQLArchivedVersion(acronym, i);

              vqe = VirtuosoQueryExecutionFactory.create(subQuery, set);
              ResultSet sub = virtuoso.executeSPARQL(vqe, subQuery);

              JsonObject builder = buildSimpleJSONObject(sub);
              TerminologyArchive resultMap = new Gson().fromJson(builder, TerminologyArchive.class);
              resultMap.setLinks(FormatterUtils.buildTerminologyLinks(acronym));

              Map<String, String> context = new HashMap<String, String>();

              List<String> jsonKeys = new ArrayList<String>();

              builder.entrySet().iterator().forEachRemaining(entry -> jsonKeys.add(entry.getKey()));
              jsonKeys.forEach(k -> context.put(k, sharedServices.getAttribute(k)));
              /*
               * for (Field f : resultMap.getClass().getDeclaredFields()) {
               * 
               * if (f.getName().equalsIgnoreCase("released")) context.put("released",
               * sharedServices.getAttribute("releaseDate")); else context.put(f.getName(),
               * sharedServices.getAttribute(f.getName())); }
               * 
               * for (Field f : resultMap.getClass().getSuperclass().getDeclaredFields()) { if
               * (f.getName().equalsIgnoreCase("released")) context.put("released",
               * sharedServices.getAttribute("releaseDate")); else context.put(f.getName(),
               * sharedServices.getAttribute(f.getName())); }
               */

              resultMap.setContext(context);
              resultList.add(resultMap);

            }
          }
          vqe.close();

        } else
          throw new ArchiveNotFoundException(acronym);
      } catch (InternalServerErrorException e) {
        set.close();
      }
    }

    return resultList;
  }

  @Override
  public List<TerminologyDataExtended> getTerminologyLatestVersion(String acronym) {

    Map<String, String> context = new HashMap<String, String>();
    List<TerminologyDataExtended> resultList = new ArrayList<>();

    if (sharedServices.isAvailable(acronym)) {

      try {

        JsonObject builder = new JsonObject();

        String sparql = CoreQueries.getSPARQLTerminologyInfo(acronym);
        // String sparql = CoreQueries.getSPARQLTerminologies(Optional.of(acronym), 1, 0);
        VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
        ResultSet rs = virtuoso.executeSPARQL(vqe, sparql);

        while (rs.hasNext()) {
          QuerySolution currentResult = rs.next();
          String label = FormatterUtils.formatField(currentResult, "label");
          String value = FormatterUtils.formatField(currentResult, "value");
          String attribute = FormatterUtils.formatField(currentResult, "attribute");

          if (label == null) {

            if (sharedServices
                .getAttribute(currentResult.get("attribute").asResource().getLocalName()) == null) {
              sharedServices.resolveContext(acronym, attribute);
            }

            // try to resolve label from cached attribute labels
            label = currentResult.get("attribute").asResource().getLocalName();

          }

          switch (label) {
            case "description":
              builder.addProperty("definition", value);
              break;

            case "releaseDate":
              builder.addProperty("released", value);
              context.put("released", sharedServices.getAttribute("releaseDate"));
              break;

            default:
              // add everything else
              builder.addProperty(label, value);
              break;
          }

          // build @context
          if (builder.keySet().contains(label) == true)
            context.put(label, sharedServices.getAttribute(label));

        }
        builder.add("context", new Gson().toJsonTree(context));
        // JsonObject builder = buildSimpleJSONObject(rs);
        vqe.close();
        TerminologyDataExtended resultMap =
            new Gson().fromJson(builder, TerminologyDataExtended.class);
        resultMap.setLinks(FormatterUtils.buildTerminologyLinks(acronym));
        resultList.add(resultMap);
      } catch (InternalServerErrorException e) {
        set.close();
      }

    } else
      throw new EntityNotFoundException("Terminology", "acronym", acronym);

    return resultList;
  }

  private JsonObject buildSimpleJSONObject(ResultSet rs) {
    JsonObject builder = new JsonObject();
    while (rs.hasNext()) {
      QuerySolution currentResult = rs.next();
      String label = FormatterUtils.formatField(currentResult, "label");
      String value = FormatterUtils.formatField(currentResult, "value");
      if (label != null && !label.isEmpty()) {
        builder.addProperty(label, value);
      }
    }
    return builder;
  }
}
