package org.semantics.ts.dao;

import org.semantics.ts.model.core.term.TermDataBasic;

import java.util.List;
import java.util.Optional;

public interface InstancesDao {
    List<TermDataBasic> getDirectInstances(String acronym, String uri, Optional<String> lang);

}
