package org.semantics.ts.dao;


import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Repository;

@Repository("mapping")
public class MappingDataAccessService implements MappingDao {
  private static final Logger LOGGER = LoggerFactory.getLogger(MappingDataAccessService.class);

  @Autowired
  ResourceLoader resourceLoader;

  // @Override
  // public String getMapping(String baseInstanceUri, String modelName, String modelFile, String
  // inputJson) throws IOException {
  // GenericRDFGenerator rdfGenerator = new GenericRDFGenerator();
  // R2RMLMappingIdentifier modelIdentifier = new R2RMLMappingIdentifier(
  // modelName, loadResource(modelFile).getURL());
  // rdfGenerator.addModel(modelIdentifier);
  // StringWriter sw = new StringWriter();
  // PrintWriter pw = new PrintWriter(sw);
  // N3KR2RMLRDFWriter writer = new N3KR2RMLRDFWriter(new URIFormatter(), pw);
  // writer.setBaseURI(baseInstanceUri);
  // RDFGeneratorRequest request = new RDFGeneratorRequest(modelName, inputJson);
  //
  //
  // request.setInputFile(loadResource(inputJson).getFile());
  // request.setAddProvenance(false);
  // request.setDataType(GenericRDFGenerator.InputType.JSON);
  // request.addWriter(writer);
  // try {
  // rdfGenerator.generateRDF(request);
  // } catch (KarmaException e) {
  // e.printStackTrace();
  // }
  // String rdf = sw.toString();
  // System.out.println("Generated RDF: " + rdf);
  // return rdf;
  // }

  public Resource loadResource(String name) throws IOException {
    LOGGER.info("Fetching " + resourceLoader.getResource(name));
    return resourceLoader.getResource(name);
  }


}
