package org.semantics.ts.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.ws.rs.InternalServerErrorException;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.RDFNode;
import org.semantics.ts.configs.StandardVariables;
import org.semantics.ts.errorhandling.EntityNotFoundException;
import org.semantics.ts.model.core.term.TermDataFull;
import org.semantics.ts.model.core.term.TermSynonyms;
import org.semantics.ts.model.database.Virtuoso;
import org.semantics.ts.queries.CoreQueries;
import org.semantics.ts.queries.SharedQueries;
import org.semantics.ts.utils.FormatterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

@Repository("terms")
public class TermDataAccessService implements TermDao {

  @Qualifier("virtuosoGraph")
  @Autowired
  private VirtGraph set;

  @Qualifier("virtuosoInstance")
  @Autowired
  private Virtuoso virtuoso;

  @Autowired
  private CoreSharedServices sharedServices;


  /**
   * Returns all terms of an ontology or an exception if the ontology has more than
   * limitAlltermsService terms.
   *
   * @param acronym terminology acronym
   * @return all terms of the ontology identified by acronym
   */
  @Override
  public List<TermDataFull> getAllTerms(String acronym, Optional<String> language,
      Optional<Integer> page, Optional<Integer> limit) {
    List<TermDataFull> resultList = new ArrayList<>();

    int offset = FormatterUtils.getQueryOffset(page, limit);
    int limit_ = FormatterUtils.getValueFromOptionalOrDefault(limit,
        StandardVariables.getLimitAllTermsService());

    if (sharedServices.isAvailable(acronym)) {
      if (sharedServices.isSmallerThanLimit(acronym)) {
        try {
          String metadataQuery = SharedQueries.getMetadataQuery(acronym);
          VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(metadataQuery, set);
          ResultSet res = virtuoso.executeSPARQL(vqe, metadataQuery);
          RDFNode graph = null;
          String label = "";
          if (res.hasNext()) {
            QuerySolution result = res.nextSolution();
            graph = result.get("graph");
            label = "<" + result.get("label").toString() + ">";
          }
          String sparql =
              CoreQueries.getSPARQLAllTerms(graph, label, language, limit_, offset, false);

          // execute COUNT query and store value
          sharedServices.retrieveCachedCount(
              CoreQueries.getSPARQLAllTerms(graph, label, language, limit_, offset, true),
              "getAllTerms()");

          vqe.close();

          vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
          ResultSet rs = virtuoso.executeSPARQL(vqe, sparql);
          // while (rs.hasNext()) {
          // QuerySolution current_result = rs.next();
          // JsonObject builder = FormatterUtils.formatTermInfo(current_result);
          // TermDataBasic resultMap = gson.fromJson(builder, TermDataBasic.class);
          // resultMap
          // .setLinks(FormatterUtils.buildTermLinks(builder.get("uri").getAsString(), acronym));
          // termDatumBasics.add(resultMap);
          resultList = sharedServices.resolveTermHierarchyData(acronym, rs, resultList);
          // }
          vqe.close();
        } catch (InternalServerErrorException e) {
          e.printStackTrace();
          set.close();
        }
      } else
        throw new InternalServerErrorException(
            "The requested terminology is too big to list all terms.");
    } else
      throw new EntityNotFoundException("Terminology", "acronym", acronym);
    return resultList;
  }

  @Override
  public List<TermDataFull> getTermByUri(String acronym, String uri) {
    // Map<String, String> context = new HashMap<String, String>();
    List<TermDataFull> resultList = new ArrayList<>();
    // JsonArray synonym = new JsonArray();
    // JsonArray subClassOf = new JsonArray();
    // JsonArray acronyms = new JsonArray();
    // JsonArray types = new JsonArray();
    if (sharedServices.isAvailable(acronym)) {
      String sparql = CoreQueries.getSPARQLTerm(acronym, uri);
      // JsonObject builder = new JsonObject();
      // try {
      VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
      // ResultSet rs = virtuoso.executeSPARQL(vqe, sparql);
      ResultSet rs = vqe.execSelect();
      resultList.add(sharedServices.resolveTermData(acronym, uri, rs));
      // if (rs.hasNext()) {
      // while (rs.hasNext()) {
      // QuerySolution currentResult = rs.next();
      // String label = FormatterUtils.formatField(currentResult, "label");
      // String value = FormatterUtils.formatField(currentResult, "value");
      // String attribute = FormatterUtils.formatField(currentResult, "attribute");
      //
      // if (label == null) {
      //
      // if (sharedServices
      // .getAttribute(currentResult.get("attribute").asResource().getLocalName()) == null)
      // sharedServices.resolveContext(acronym, attribute);
      //
      // // try to resolve label from cached attribute labels
      // label = currentResult.get("attribute").asResource().getLocalName();
      //
      // }
      //
      // if (label != null) {
      // if (label.contains("synonym")) {
      // synonym = FormatterUtils.processArrayFields(synonym, value);
      // } else if (label.equals("subClassOf")) {
      // subClassOf = FormatterUtils.processArrayFields(subClassOf, value);
      // } else if (label.equals("acronym")) {
      // acronyms = FormatterUtils.processArrayFields(acronyms, value);
      // } else if (label.equals("type")) {
      // types = FormatterUtils.processArrayFields(types, value);
      // } else if (label.contains("definition") || label.contains("description")) {
      // builder.addProperty("definition", value);
      // } else {
      // builder.addProperty(label, value);
      // }
      // // build @context
      // context.put(label, attribute);
      // }
      //
      // }
      // builder.addProperty("uri", uri);
      // builder.addProperty("sourceTerminology", acronym);
      // builder.add("synonym", synonym);
      // builder.add("subClassOf", subClassOf);
      // builder.add("type", types);
      // builder.add("acronym", acronyms);
      // builder.add("context", gson.toJsonTree(context));
      // // addTermLinks(builder, uri, acronym);
      // vqe.close();
      // } else
      // throw new EntityNotFoundException("Term", "term", uri);
      // } catch (InternalServerErrorException e) {
      // set.close();
      // }
      // TermDataFull resultMap = gson.fromJson(builder, TermDataFull.class);
      // resultMap.setLinks(FormatterUtils.buildTermLinks(uri, acronym));
      // resultList.add(resultMap);
      // } else
      // throw new EntityNotFoundException("Terminology", "acronym", acronym);
    }
    return resultList;
  }

  @Override
  public List<TermSynonyms> getTermSynonymsByUri(String acronym, String uri,
      Optional<String> language) {

    Map<String, String> context = new HashMap<String, String>();
    List<TermSynonyms> resultList = new ArrayList<>();

    if (sharedServices.isAvailable(acronym)) {
      JsonObject builder = new JsonObject();
      try {
        JsonArray synonym = new JsonArray();
        String metadataSynonym = CoreQueries.getSPARQLMetadataSynonymSpecified(acronym);
        VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(metadataSynonym, set);
        ResultSet res = vqe.execSelect();
        RDFNode graph = null;
        RDFNode synonymRDF = null;
        if (res.hasNext()) {
          QuerySolution result = res.nextSolution();
          graph = result.get("graph");
          synonymRDF = result.get("synonym");
        }
        if (synonym != null) {
          String sparql =
              CoreQueries.getSPARQLSynonym(uri, graph.toString(), synonymRDF.toString(), language);
          vqe = VirtuosoQueryExecutionFactory.create(sparql, set);
          ResultSet rs = virtuoso.executeSPARQL(vqe, sparql);
          while (rs.hasNext()) {
            QuerySolution currentResult = rs.next();
            String synonym_ = FormatterUtils.formatField(currentResult, "synonym");
            synonym = FormatterUtils.processArrayFields(synonym, synonym_);
          }
          builder.add("synonym", synonym);

          context.put("synonym", sharedServices.getAttribute("synonym"));
          builder.add("context", new Gson().toJsonTree(context));
        }
        vqe.close();
      } catch (InternalServerErrorException e) {
        set.close();
      }

      TermSynonyms resultMap = new Gson().fromJson(builder, TermSynonyms.class);
      resultMap.setLinks(FormatterUtils.buildTermLinks(uri, acronym));
      resultList.add(resultMap);
    } else
      throw new EntityNotFoundException("Terminology", "acronym", acronym);
    return resultList;
  }

  @Override
  public List<TermDataFull> getTerminologyChangelog(String acronym, Optional<Integer> page,
      Optional<Integer> limit) {

    List<TermDataFull> resultList = new ArrayList<>();

    int offset = 0, pagesize = StandardVariables.getLimitAllTermsService();

    if (limit.isPresent() == true)
      pagesize = limit.get();

    if (page.isPresent() == true) {
      if (page.get() > 1)
        offset = pagesize * (page.get() - 1);
    }

    if (sharedServices.isAvailable(acronym)) {
      try {
        // execute COUNT query
        sharedServices.retrieveCachedCount(CoreQueries.getSPARQLChangedTerms(acronym,
            StandardVariables.getLimitAllTermsService(), 0, true), "getTerminologyChangelog()");

        String changedTerms = CoreQueries.getSPARQLChangedTerms(acronym, pagesize, offset, false);
        VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(changedTerms, set);
        ResultSet rs = virtuoso.executeSPARQL(vqe, changedTerms);

        while (rs.hasNext()) {
          String URI = "";

          QuerySolution qs = rs.nextSolution();

          // extract URI and status of term
          URI = qs.get("o").asResource().toString();

          if (qs.get("change").asLiteral().toString().equalsIgnoreCase("modified")
              || qs.get("change").asLiteral().toString().equalsIgnoreCase("added")) {

            // get complete term info for URI
            List<TermDataFull> term_ = getTermByUri(acronym, URI);

            // TODO better check
            if (term_.size() == 1) {
              // JsonObjectBuilder termBuilder = Json.createObjectBuilder();
              // termBuilder.add("uri", URI);
              // for (String key : res.get(0).keySet())
              // termBuilder.add(key, res.get(0).get(key));
              // builder.add(termBuilder.build());
              TermDataFull tempTerm = term_.get(0);
              tempTerm.setStatus(qs.get("change").asLiteral().toString());
              Map<String, String> context = (Map<String, String>) tempTerm.getContext();
              context.put("status", "http://terminologies.gfbio.org/terms/ts-schema/status");

              tempTerm.setContext(context);

              tempTerm.setLinks(FormatterUtils.buildTermLinks(URI, acronym));
              resultList.add(tempTerm);
            }
          } else {
            // for removed terms we only display the URI
            JsonObject builder = new JsonObject();
            builder.addProperty("uri", URI);
            TermDataFull resultMap = new Gson().fromJson(builder, TermDataFull.class);
            resultMap.setStatus(qs.get("change").asLiteral().toString());

            Map<String, String> context = new HashMap<String, String>();
            context.put("status", "http://terminologies.gfbio.org/terms/ts-schema/status");

            resultMap.setContext(context);

            resultMap.setLinks(FormatterUtils.buildTermLinks(URI, acronym));
            resultList.add(resultMap);
          }
        }

      } catch (InternalServerErrorException e) {
        set.close();
      }
    }

    return resultList;
  }

}
