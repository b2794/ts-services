package org.semantics.ts.configs;

import org.semantics.ts.model.database.Virtuoso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import virtuoso.jena.driver.VirtGraph;

@Configuration
@PropertySource("classpath:application.properties")
@ComponentScan(basePackages = {"org.semantics.ts.*"})
@ConfigurationProperties(prefix = "virtuoso")
public class VirtuosoConfig {

    @Autowired
    private Environment env;

    @Bean(name = "virtuosoInstance")
    public Virtuoso virtuosoInstance() throws Exception {
        String host = env.getProperty("virtuoso.host");
        String username = env.getProperty("virtuoso.username");
        String password = env.getProperty("virtuoso.password");
        return new Virtuoso(host, username, password);
    }

    @Bean(name = "virtuosoGraph")
    public VirtGraph virtuosoGraph() throws Exception {
        String host = env.getProperty("virtuoso.host");
        String username = env.getProperty("virtuoso.username");
        String password = env.getProperty("virtuoso.password");
        Virtuoso virtuoso = new Virtuoso(host, username, password);
        return virtuoso.getVirtGraph();
    }


}
