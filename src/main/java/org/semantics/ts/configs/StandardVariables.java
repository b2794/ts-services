package org.semantics.ts.configs;

import java.util.ArrayList;
import java.util.Arrays;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("standardVariables")
public class StandardVariables {

  private static final Logger LOGGER = LoggerFactory.getLogger(StandardVariables.class);

  /**
   * Variables declared here can be set at runtime, i.e., when starting Spring Boot
   */

  @Value("${ts.api.url}")
  private String apiUrl;

  @Value("${ts.api.url}")
  public void setApiUrlStatic(String apiUrl) {
    StandardVariables.apiUrlStatic = apiUrl;
  }

  @Value("${ts.metadata.schema}")
  private String metadataSchema;

  @Value("${ts.metadata.schema}")
  public void setMetadataSchemaStatic(String metadataSchema) {
    StandardVariables.metadataSchemaStatic = metadataSchema;
  }

  @Value("${ts.uri.prefix}")
  private String uriPrefix;

  @Value("${ts.uri.prefix}")
  public void setUriPrefixStatic(String uriPrefix) {
    StandardVariables.uriPrefixStatic = uriPrefix;
  }

  @Value("${ts.short.prefix}")
  private String shortPrefix;

  @Value("${ts.short.prefix}")
  public void setShortPrefixStatic(String shortPrefix) {
    StandardVariables.shortPrefixStatic = shortPrefix;
  }

  @Value("${ts.metadata.graph}")
  private String metadataGraph;

  @Value("${ts.metadata.graph}")
  public void setMetadataGraphStatic(String metadataGraph) {
    StandardVariables.metadataGraphStatic = metadataGraph;
  }

  @Value("${ts.ontologies.dir}")
  private String ontologiesDir;

  @Value("${ts.ontologies.dir}")
  public void setOntologiesDirStatic(String ontologiesDir) {
    StandardVariables.ontologiesDirStatic = ontologiesDir;
  }

  private static String uriPrefixStatic, metadataSchemaStatic, shortPrefixStatic,
      metadataGraphStatic, ontologiesDirStatic, apiUrlStatic;

  // Graphs
  private static final String plaqkPrefix = "http://planqk.de/ontologies/qco/";
  private static final String plaqkInstancesPrefix = "http://planqk.de/ontologies/qco/data/";
  private static final String aBoxPrefix = "data/";
  private static final String oboInOwlURI = "http://www.ontobee.org/ontologies/oboInOwl.owl";
  private static final String tsSchema = "http://terminologies.gfbio.org/terms/ts-schema";

  // Metrics
  private static final String individualNumber = "numberOfIndividuals";
  private static final String maximumNumberOfChildren = "maximumNumberOfChildren";
  private static final String maximumDepth = "maximumDepth";
  private static final String twentyfiveChildren = "classesWithMoreThan25Children";
  private static final String averageNumberOfChildren = "averageNumberOfChildren";
  private static final String propertyNumber = "numberOfProperties";
  private static final String singleChild = "classesWithASingleChild";
  private static final String classesWithoutDefinition = "classesWithoutDefinition";
  private static final String numberOfleaves = "numberOfLeaves";
  private static final String classesWithoutLabel = "classesWithoutLabel";
  private static final String classesWithMoreThan1Parent = "classesWithMoreThan1Parent";

  // Other variables
  // FIXME
  private static final String wsPrefix = "http://terminologies.gfbio.org/api";
  private static final int limitAllTermsService = 10000;
  private static final int tsCacheDuration = 180;

  private static final String classesNumber = "numberOfClasses";
  private static final ArrayList<String> standardDescriptionAttributes =
      new ArrayList<>(Arrays.asList("dc:description"));

  @PostConstruct
  public void printVariables() {
    LOGGER.info("metadataSchema=" + metadataSchema + "; uriPrefix=" + uriPrefix + "; shortPrefix="
        + shortPrefix + "; metadataGraph=" + metadataGraph + "; ontologiesDir" + ontologiesDir);
  }

  // Getters
  public static String getMetadaschema() {
    return metadataSchemaStatic;
  }

  public static String getOntologiesDir() {
    return ontologiesDirStatic;
  }

  public static String getUriprefix() {
    return uriPrefixStatic;
  }

  public static String getMetadataGraph() {
    return metadataGraphStatic;
  }

  public static String getApiUrl() {
    return apiUrlStatic;
  }

  public static String getShortPrefix() {
    return shortPrefixStatic;
  }

  public static String getIndividualnumber() {
    return individualNumber;
  }

  public static String getMaximumnumberofchildren() {
    return maximumNumberOfChildren;
  }

  public static String getMaximumdepth() {
    return maximumDepth;
  }

  public static String getTwentyfivechildren() {
    return twentyfiveChildren;
  }

  public static String getAveragenumberofchildren() {
    return averageNumberOfChildren;
  }

  public static String getPropertynumber() {
    return propertyNumber;
  }

  public static String getSinglechild() {
    return singleChild;
  }

  public static String getClasseswithoutdefinition() {
    return classesWithoutDefinition;
  }

  public static String getNumberofleaves() {
    return numberOfleaves;
  }

  public static String getClasseswithoutlabel() {
    return classesWithoutLabel;
  }

  public static String getClasseswithmorethan1parent() {
    return classesWithMoreThan1Parent;
  }

  public static String getWsprefix() {
    return wsPrefix;
  }

  public static ArrayList<String> getStandarddescriptionattributes() {
    return standardDescriptionAttributes;
  }

  public static String getOboInOwlURI() {
    return oboInOwlURI;
  }

  public static String getClassesNumber() {
    return classesNumber;
  }

  public static String getABoxPrefix() {
    return aBoxPrefix;
  }

  public static String getPlaqkInstancesPrefix() {
    return plaqkInstancesPrefix;
  }

  public static String getPlaqkPrefix() {
    return plaqkPrefix;
  }

  public static int getLimitAllTermsService() {
    return limitAllTermsService;
  }

  public static String getTsschema() {
    return tsSchema;
  }

  public static int getTscacheduration() {
    return tsCacheDuration;
  }

}
