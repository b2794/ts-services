package org.semantics.ts.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.semantics.ts.model.TSResponse;
import org.semantics.ts.service.TerminologiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/core/terminologies")
public class TerminologiesController {

  private final TerminologiesService terminologiesService;

  @Autowired
  public TerminologiesController(TerminologiesService terminologiesService) {
    this.terminologiesService = terminologiesService;
  }

  @GetMapping(value = "")
  public ResponseEntity<TSResponse> getTerminologies(HttpServletRequest uriInfo,
      @RequestParam("page") Optional<Integer> page,
      @RequestParam(name = "limit") Optional<Integer> limit) {

    HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.set(HttpHeaders.CONTENT_TYPE, "application/json");
    return ResponseEntity.ok().headers(responseHeaders)
        .body(this.terminologiesService.getTerminologies(uriInfo, page, limit));
  }

  @GetMapping(value = "/{acronym}", produces = "application/json")
  public ResponseEntity<TSResponse> getTerminologyByUri(HttpServletRequest uriInfo,
      @PathVariable("acronym") String acronym) {

    HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.set(HttpHeaders.CONTENT_TYPE, "application/json");
    return ResponseEntity.ok().headers(responseHeaders)
        .body(this.terminologiesService.getTerminologyByAcronym(uriInfo, acronym));
  }

  @GetMapping(value = "/{acronym}/allterms", produces = "application/json")
  public ResponseEntity<TSResponse> getAllTerms(HttpServletRequest uriInfo,
      @PathVariable("acronym") String acronym, @RequestParam("language") Optional<String> language,
      @RequestParam("page") Optional<Integer> page,
      @RequestParam(name = "limit") Optional<Integer> limit) {

    HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.set(HttpHeaders.CONTENT_TYPE, "application/json");
    return ResponseEntity.ok().headers(responseHeaders)
        .body(this.terminologiesService.getAllTerms(uriInfo, acronym, language, page, limit));
  }

  @GetMapping(value = "/{acronym}/term", produces = "application/json")
  public ResponseEntity<TSResponse> getTermByUri(HttpServletRequest uriInfo,
      @PathVariable("acronym") String acronym, @RequestParam("uri") String uri) {

    HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.set(HttpHeaders.CONTENT_TYPE, "application/json");
    return ResponseEntity.ok().headers(responseHeaders)
        .body(this.terminologiesService.getTermByUri(uriInfo, acronym, uri));
  }

  @GetMapping(value = "/{acronym}/synonyms", produces = "application/json")
  public ResponseEntity<TSResponse> getTermSynonymsByUri(HttpServletRequest uriInfo,
      @PathVariable("acronym") String acronym, @RequestParam("uri") String uri,
      @RequestParam("language") Optional<String> language) {

    HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.set(HttpHeaders.CONTENT_TYPE, "application/json");
    return ResponseEntity.ok().headers(responseHeaders)
        .body(this.terminologiesService.getTermSynonymsByUri(uriInfo, acronym, uri, language));
  }

  @GetMapping(value = "/{acronym}/metrics", produces = "application/json")
  public ResponseEntity<TSResponse> getTerminologyMetrics(HttpServletRequest uriInfo,
      @PathVariable("acronym") String acronym) {

    HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.set(HttpHeaders.CONTENT_TYPE, "application/json");
    return ResponseEntity.ok().headers(responseHeaders)
        .body(this.terminologiesService.getTerminologyMetrics(uriInfo, acronym));
  }

  @GetMapping(value = "/{acronym}/metadata", produces = "application/json")
  public ResponseEntity<TSResponse> getTerminologyMetadata(HttpServletRequest uriInfo,
      @PathVariable("acronym") String acronym) {

    HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.set(HttpHeaders.CONTENT_TYPE, "application/json");
    return ResponseEntity.ok().headers(responseHeaders)
        .body(this.terminologiesService.getTerminologyMetadata(uriInfo, acronym));
  }

  @GetMapping(value = "/{acronym}/archive/{archiveId}", produces = "application/json")
  public ResponseEntity<TSResponse> getTerminologyArchive(HttpServletRequest uriInfo,
      @PathVariable("acronym") String acronym, @RequestParam("page") Optional<Integer> page,
      @RequestParam(name = "limit") Optional<Integer> limit, Optional<Integer> archiveId) {

    HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.set(HttpHeaders.CONTENT_TYPE, "application/json");
    return ResponseEntity.ok().headers(responseHeaders).body(this.terminologiesService
        .getTerminologyArchivedVersion(uriInfo, acronym, archiveId, page, limit));
  }

  @GetMapping(value = "/{acronym}/latest", produces = "application/json")
  public ResponseEntity<TSResponse> getTerminologyLatestVersion(HttpServletRequest uriInfo,
      @PathVariable("acronym") String acronym) {

    HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.set(HttpHeaders.CONTENT_TYPE, "application/json");
    return ResponseEntity.ok().headers(responseHeaders)
        .body(this.terminologiesService.getTerminologyLatestVersion(uriInfo, acronym));
  }

  @GetMapping(value = "/{acronym}/download", produces = "application/octet-stream")
  public ResponseEntity<Resource> downloadTerminology(HttpServletRequest uriInfo,
      @PathVariable("acronym") String acronym) throws FileNotFoundException {

    HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.set(HttpHeaders.CONTENT_TYPE, "application/octet-stream");

    File file = this.terminologiesService.getDownload(acronym);

    InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

    return ResponseEntity.ok().headers(responseHeaders).contentLength(file.length())
        .contentType(MediaType.APPLICATION_OCTET_STREAM).body(resource);
  }

  @GetMapping(value = "/{acronym}/changelog", produces = "application/json")
  public ResponseEntity<TSResponse> getTerminologyChangelog(HttpServletRequest uriInfo,
      @PathVariable("acronym") String acronym, @RequestParam("page") Optional<Integer> page,
      @RequestParam(name = "limit") Optional<Integer> limit) {

    HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.set(HttpHeaders.CONTENT_TYPE, "application/json");
    return ResponseEntity.ok().headers(responseHeaders)
        .body(this.terminologiesService.getTerminologyChangelog(uriInfo, acronym, page, limit));
  }
}
