package org.semantics.ts.controller;

import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.semantics.ts.model.TSResponse;
import org.semantics.ts.service.HierarchyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hierarchy/terminologies")
public class HierarchyController {
  private final HierarchyService hierarchyService;

  @Autowired
  public HierarchyController(HierarchyService hierarchyService) {
    this.hierarchyService = hierarchyService;
  }

  @GetMapping(value = "/{acronym}/children", produces = "application/json")
  public ResponseEntity<TSResponse> getChildren(HttpServletRequest uriInfo,
      @PathVariable("acronym") String acronym, @RequestParam("uri") String uri,
      @RequestParam("lang") Optional<String> lang, @RequestParam("page") Optional<Integer> page,
      @RequestParam("limit") Optional<Integer> limit) {
    HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.set(HttpHeaders.CONTENT_TYPE, "application/json");
    return ResponseEntity.ok().headers(responseHeaders)
        .body(this.hierarchyService.getChildren(uriInfo, acronym, uri, lang, page, limit));
  }

  @GetMapping(value = "/{acronym}/descendants", produces = "application/json")
  public ResponseEntity<TSResponse> getDescendants(HttpServletRequest uriInfo,
      @PathVariable("acronym") String acronym, @RequestParam("uri") String uri,
      @RequestParam("lang") Optional<String> lang, @RequestParam("page") Optional<Integer> page,
      @RequestParam("limit") Optional<Integer> limit) {
    HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.set(HttpHeaders.CONTENT_TYPE, "application/json");
    return ResponseEntity.ok().headers(responseHeaders)
        .body(this.hierarchyService.getDescendants(uriInfo, acronym, uri, lang, page, limit));
  }

  @GetMapping(value = "/{acronym}/parents", produces = "application/json")
  public ResponseEntity<TSResponse> getParents(HttpServletRequest uriInfo,
      @PathVariable("acronym") String acronym, @RequestParam("uri") String uri,
      @RequestParam("lang") Optional<String> lang, @RequestParam("page") Optional<Integer> page,
      @RequestParam("limit") Optional<Integer> limit) {
    HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.set(HttpHeaders.CONTENT_TYPE, "application/json");
    return ResponseEntity.ok().headers(responseHeaders)
        .body(this.hierarchyService.getParents(uriInfo, acronym, uri, lang, page, limit));
  }

  @GetMapping(value = "/{acronym}/ancestors", produces = "application/json")
  public ResponseEntity<TSResponse> getAncestors(HttpServletRequest uriInfo,
      @PathVariable("acronym") String acronym, @RequestParam("uri") String uri,
      @RequestParam("lang") Optional<String> lang, @RequestParam("page") Optional<Integer> page,
      @RequestParam("limit") Optional<Integer> limit) {
    HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.set(HttpHeaders.CONTENT_TYPE, "application/json");
    return ResponseEntity.ok().headers(responseHeaders)
        .body(this.hierarchyService.getAncestors(uriInfo, acronym, uri, lang, page, limit));
  }

}
