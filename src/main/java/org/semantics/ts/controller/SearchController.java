package org.semantics.ts.controller;

import javax.servlet.http.HttpServletRequest;
import org.semantics.ts.model.TSResponse;
import org.semantics.ts.model.core.services.Search;
import org.semantics.ts.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/search-services")
public class SearchController {
  private final SearchService searchService;

  @Autowired
  public SearchController(SearchService searchService) {
    this.searchService = searchService;
  }

  @PostMapping(value = "/search", consumes = "application/json")
  public ResponseEntity<TSResponse> postSearch(HttpServletRequest uriInfo,
      @RequestBody Search searchParams) {
    HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.set(HttpHeaders.CONTENT_TYPE, "application/json");
    return ResponseEntity.ok().headers(responseHeaders)
        .body(this.searchService.getSearch(uriInfo, searchParams));
  }

  // 24.03.22 FB service endpoint deactivated
  // @PostMapping(value = "/expansion", consumes = "application/json")
  // public ResponseEntity<TSResponse> postSearchQueryExtension(HttpServletRequest uriInfo,
  // @RequestBody SearchBasic searchParams) {
  // HttpHeaders responseHeaders = new HttpHeaders();
  // responseHeaders.set(HttpHeaders.CONTENT_TYPE, "application/json");
  // return ResponseEntity.ok()
  // .headers(responseHeaders)
  // .body(this.searchService.getSearchQueryExpansion(uriInfo, searchParams));
  // }
  //
  // 24.03.22 FB service endpoint deactivated
  // @PostMapping(value = "/suggest", produces = "application/json")
  // public ResponseEntity<TSResponse> postSuggest(HttpServletRequest uriInfo,
  // @RequestBody SearchBasic suggestParams) {
  // HttpHeaders responseHeaders = new HttpHeaders();
  // responseHeaders.set(HttpHeaders.CONTENT_TYPE, "application/json");
  // return ResponseEntity.ok()
  // .headers(responseHeaders)
  // .body(this.searchService.getSuggest(uriInfo, suggestParams));
  // }

}
