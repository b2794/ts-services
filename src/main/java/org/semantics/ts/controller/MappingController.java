package org.semantics.ts.controller;


import org.semantics.ts.service.MappingService;

// @RestController
// @RequestMapping("/mapping/terminologies")
// 24.03.22 FB service endpoint deactivated
public class MappingController {
  private final MappingService mappingService;

  public MappingController(MappingService mappingService) {
    this.mappingService = mappingService;
  }

  // @GetMapping(value = "{artifact}", produces = "application/json")
  // public ResponseEntity<TSResponse> getMapping(HttpServletRequest uriInfo,
  // @PathVariable("artifact") String artifact) throws Exception {
  // HttpHeaders responseHeaders = new HttpHeaders();
  // responseHeaders.set(HttpHeaders.CONTENT_TYPE, "application/json");
  // return ResponseEntity.ok().headers(responseHeaders)
  // .body(this.mappingService.getMapping(uriInfo, artifact));
  // }

}
