package org.semantics.ts.controller;

import org.semantics.ts.service.InstancesService;

// @RestController
// @RequestMapping("/instances/terminologies")
// 24.03.22 FB service endpoint deactivated
public class InstancesController {
  private final InstancesService instancesService;

  public InstancesController(InstancesService instancesService) {
    this.instancesService = instancesService;
  }

  // @GetMapping(value = "/{acronym}", produces = "application/json")
  // public ResponseEntity<TSResponse> getDirectInstances(HttpServletRequest uriInfo,
  // @PathVariable("acronym") String acronym, @RequestParam("uri") String uri,
  // @RequestParam("lang") Optional<String> lang) {
  // HttpHeaders responseHeaders = new HttpHeaders();
  // responseHeaders.set(HttpHeaders.CONTENT_TYPE, "application/json");
  // return ResponseEntity.ok().headers(responseHeaders)
  // .body(this.instancesService.getDirectInstances(uriInfo, acronym, uri, lang));
  // }
}
