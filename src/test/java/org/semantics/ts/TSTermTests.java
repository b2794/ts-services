package org.semantics.ts;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@SpringBootTest
@AutoConfigureMockMvc
public class TSTermTests {

  private static final String ACRONYM = "ENVO";

  private static final String TERMURI = "http://purl.obolibrary.org/obo/ENVO_00002011";

  @Autowired
  private MockMvc mockMvc;

  @Test
  public void testGetTermByUri() throws Exception {
    MvcResult res = this.mockMvc
        .perform(get("/core/terminologies/{acronym}/term", ACRONYM).param("uri", TERMURI))
        .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

    JsonObject response =
        new Gson().fromJson(res.getResponse().getContentAsString(), JsonObject.class);

    assertEquals(1, response.get("totalCount").getAsInt());

    JsonObject term = response.getAsJsonArray("collection").get(0).getAsJsonObject();
    JsonArray synonym = term.getAsJsonArray("synonym");
    JsonArray subClassOf = term.getAsJsonArray("subClassOf");

    assertEquals(TERMURI, term.get("uri").getAsString());
    assertEquals("fresh water", term.get("label").getAsString());
    assertEquals("freshwater", synonym.get(0).getAsString());
    assertEquals("sweet water", synonym.get(1).getAsString());
    assertEquals("http://purl.obolibrary.org/obo/ENVO_00002006", subClassOf.get(0).getAsString());
    assertNotNull(term.get("definition").getAsString());
  }

  @Test
  public void testGetSynonyms() throws Exception {
    MvcResult res = this.mockMvc
        .perform(get("/core/terminologies/{acronym}/synonyms", "CHEBI").param("uri",
            "http://purl.obolibrary.org/obo/CHEBI_36341"))
        .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

    JsonObject response =
        new Gson().fromJson(res.getResponse().getContentAsString(), JsonObject.class);

    assertEquals(1, response.get("totalCount").getAsInt());

    JsonObject term = response.getAsJsonArray("collection").get(0).getAsJsonObject();
    JsonArray synonym = term.getAsJsonArray("synonym");

    assertEquals("boson", synonym.get(0).getAsString());
    assertEquals(1, synonym.size());
  }
}
