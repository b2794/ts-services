package org.semantics.ts;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@SpringBootTest
@AutoConfigureMockMvc
public class TSHierarchyTests {


  private static final String ACRONYM = "ENVO";

  private static final String TERMURI = "http://purl.obolibrary.org/obo/FOODON_03411283";

  private static final String TYPE = "http://www.w3.org/2002/07/owl#Class";

  @Autowired
  private MockMvc mockMvc;

  @Test
  public void testGetChildren() throws Exception {
    MvcResult res = this.mockMvc
        .perform(get("/hierarchy/terminologies/{acronym}/children", ACRONYM).param("uri", TERMURI))
        .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

    JsonObject response =
        new Gson().fromJson(res.getResponse().getContentAsString(), JsonObject.class);

    assertEquals(1, response.get("totalCount").getAsInt());

    JsonObject term = response.getAsJsonArray("collection").get(0).getAsJsonObject();
    JsonArray subClassOf = term.getAsJsonArray("subClassOf");
    JsonArray type = term.getAsJsonArray("type");

    assertEquals("http://purl.obolibrary.org/obo/FOODON_00001155", term.get("uri").getAsString());
    assertEquals("watermelon plant@en", term.get("label").getAsString());
    assertEquals(TERMURI, subClassOf.get(0).getAsString());
    assertEquals(TYPE, type.get(0).getAsString());
  }

  @Test
  public void testGetParents() throws Exception {
    MvcResult res = this.mockMvc
        .perform(get("/hierarchy/terminologies/{acronym}/parents", ACRONYM).param("uri", TERMURI))
        .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

    JsonObject response =
        new Gson().fromJson(res.getResponse().getContentAsString(), JsonObject.class);

    assertEquals(2, response.get("totalCount").getAsInt());
  }

  @Test
  public void testGetAncestors() throws Exception {
    MvcResult res = this.mockMvc
        .perform(get("/hierarchy/terminologies/{acronym}/ancestors", ACRONYM).param("uri", TERMURI))
        .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

    JsonObject response =
        new Gson().fromJson(res.getResponse().getContentAsString(), JsonObject.class);

    assertEquals(4, response.get("totalCount").getAsInt());
  }

  @Test
  public void testGetDescendants() throws Exception {
    MvcResult res = this.mockMvc
        .perform(
            get("/hierarchy/terminologies/{acronym}/descendants", ACRONYM).param("uri", TERMURI))
        .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

    JsonObject response =
        new Gson().fromJson(res.getResponse().getContentAsString(), JsonObject.class);

    assertEquals(1, response.get("totalCount").getAsInt());
  }

}
