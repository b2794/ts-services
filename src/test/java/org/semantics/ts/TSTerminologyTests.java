package org.semantics.ts;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

@SpringBootTest
@AutoConfigureMockMvc
public class TSTerminologyTests {

  @Autowired
  private MockMvc mockMvc;

  @Test
  public void testGetTerminologies() throws Exception {
    MvcResult res = this.mockMvc.perform(get("/core/terminologies"))
        .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

    JsonObject response =
        new Gson().fromJson(res.getResponse().getContentAsString(), JsonObject.class);

    // TODO set size accordingly
    assertEquals(24, response.get("totalCount").getAsInt());
  }

  @Test
  public void testGetTerminology() throws Exception {
    MvcResult res = this.mockMvc.perform(get("/core/terminologies/{acronym}", "CHEBI"))
        .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

    JsonObject response =
        new Gson().fromJson(res.getResponse().getContentAsString(), JsonObject.class);

    assertEquals(1, response.get("totalCount").getAsInt());

    JsonObject chebi = response.getAsJsonArray("collection").get(0).getAsJsonObject();
    assertEquals("CHEBI", chebi.get("acronym").getAsString());
    assertEquals("Ontology", chebi.get("type").getAsString());
    assertEquals("Chemical Entities of Biological Interest Ontology",
        chebi.get("name").getAsString());

    JsonObject context = chebi.getAsJsonObject("@context");
    assertEquals(6, context.size());
  }



  // @Test
  // public void testQueryStringComparison() {
  // String s1 =
  // "SELECT ?attribute ?value ?label FROM <http://terminologies.gfbio.org/terms/ENVO> FROM
  // <http://terminologies.gfbio.org/terms/> FROM <http://terminologies.gfbio.org/terms/Metadata>
  // FROM <http://terminologies.gfbio.org/terms/ontology> FROM
  // <http://terminologies.gfbio.org/terms/ts-schema> FROM
  // <http://omv.ontoware.org/2005/05/ontology> WHERE
  // {{<http://purl.obolibrary.org/obo/ENVO_00002011> ?attribute ?value}. FILTER ( !
  // isBLANK(?value)) . OPTIONAL {?attribute rdfs:label ?label}}";
  //
  // String s2 =
  // "SELECT ?attribute ?value ?label FROM <http://terminologies.gfbio.org/terms/ENVO> FROM
  // <http://terminologies.gfbio.org/terms/> FROM <http://terminologies.gfbio.org/terms/Metadata>
  // FROM <http://terminologies.gfbio.org/terms/ontology> FROM
  // <http://terminologies.gfbio.org/terms/ts-schema> FROM
  // <http://omv.ontoware.org/2005/05/ontology> WHERE
  // {{<http://purl.obolibrary.org/obo/ENVO_06105012> ?attribute ?value}. FILTER ( !
  // isBLANK(?value)) . OPTIONAL {?attribute rdfs:label ?label}}";
  //
  // assertNotEquals(s1, s2);
  //
  // s1 = "SELECT ?acronym ?uri ?name ?description ?type ?lastArchiveID FROM
  // <http://terminologies.gfbio.org/terms/Metadata> WHERE {?s a omv:Ontology . ?s omv:acronym
  // ?acronym . ?s gfbio:graph ?graph . ?s omv:URI ?uri . ?s omv:name ?name . ?s omv:description
  // ?description . ?s rdf:type ?type . ?s gfbio:lastArchiveID ?lastArchiveID . } LIMIT 10000 OFFSET
  // 0";
  // s2 = "SELECT ?acronym ?uri ?name ?description ?type ?lastArchiveID FROM
  // <http://terminologies.gfbio.org/terms/Metadata> WHERE {?s a omv:Ontology . ?s omv:acronym
  // ?acronym . ?s gfbio:graph ?graph . ?s omv:URI ?uri . ?s omv:name ?name . ?s omv:description
  // ?description . ?s rdf:type ?type . ?s gfbio:lastArchiveID ?lastArchiveID . } LIMIT 10000 OFFSET
  // 0";
  //
  // assertEquals(s1, s2);
  // }
}
