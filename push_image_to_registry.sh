#!/bin/bash
# pushes a docker image to this project's image registry on GitLab
# set a valid tag before pushing

if [ -z $1 ]; 
    then echo "tag is not set"; 
    else echo "tag is set to '$1'"; 
    docker login registry.gitlab.com;
    docker build -t registry.gitlab.com/b2794/ts-services:$1 . ;
    docker push registry.gitlab.com/b2794/ts-services:$1;
fi
